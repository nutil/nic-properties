package com.nic.properties.repositoryImpl;

import com.nic.properties.QEntities.QTBLNearestCitiesEntity;
import com.nic.properties.helper.QJPAHelper;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class NearestCityRepository extends QJPAHelper {
    private final QTBLNearestCitiesEntity qtblNearestCitiesEntity;
    public NearestCityRepository() {
        qtblNearestCitiesEntity = QTBLNearestCitiesEntity.tBLNearestCitiesEntity;
    }

    public List<String> getNearestCitiesByStateCodeAndCityName(String stateCode, String cityName) {
        JPAQuery<String> query = getHibernateHintedJPAQuery();
        query.select(qtblNearestCitiesEntity.city)
                .from(qtblNearestCitiesEntity)
                .where(qtblNearestCitiesEntity.searchState.equalsIgnoreCase(stateCode), qtblNearestCitiesEntity.searchCity.equalsIgnoreCase(cityName));
        List<String> nearestCities = query.fetch();
        if (nearestCities.isEmpty()) {
            return Collections.emptyList();
        } else {
            return nearestCities;
        }
    }
}
