package com.nic.properties.repositoryImpl;


import com.nic.properties.QEntities.QProperty;
import com.nic.properties.helper.QJPAHelper;
import com.nic.properties.model.PropertyDTO;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

/**
 *
 * Repository class for Property.java.
 *
 */

@Repository
public class PropertyRepositoryImpl extends QJPAHelper {

    // Type safe class instantiation for Property.java class.
    private static final QProperty qProperty = QProperty.property;

    /*
    *
    * Fetch property payload(List<PropertyDTO>) for Property.java
    *   with the given stateCode, cityName and list of sale types.
    *
    * */
    public List<PropertyDTO> findByStateCodeCityAndSaleTypeForMiniSRP(String stateCode, String cityName, List<String> saleTypes) {
        JPAQuery<PropertyDTO> propertyDTOJPAQuery = getHibernateHintedJPAQuery()
                .select(getPropertyDTO())
                .from(qProperty)
                .where(qProperty.stateOrProvince.equalsIgnoreCase(stateCode).and(qProperty.city.equalsIgnoreCase(cityName).and(qProperty.saleType.in(saleTypes))))
                .orderBy(qProperty.qualityScore2.desc())
                .limit(4L);
        List<PropertyDTO> propertyDTOS = propertyDTOJPAQuery.fetch();
        if (propertyDTOS == null || propertyDTOS.isEmpty()) {
            return Collections.emptyList();
        } else {
            return propertyDTOS;
        }
    }

    /*
     *
     * Fetch property payload(List<PropertyDTO>) for Property.java
     *   with the given stateCode, list of cityNames and list of sale types.
     *  Resulting in making a property search within 20 miles of radius.
     *
     * */
    public List<PropertyDTO> findByStateCodeAndCityInAndSaleTypesInWithinTwentyMilesOfRadius(String stateCode, List<String> cities, List<String> saleTypes) {
        JPAQuery<PropertyDTO> propertyDTOJPAQuery = getHibernateHintedJPAQuery()
                .select(getPropertyDTO())
                .from(qProperty);
        BooleanBuilder searchCriterion = new BooleanBuilder();
        searchCriterion.and(qProperty.stateOrProvince.equalsIgnoreCase(stateCode));
        searchCriterion.and(qProperty.city.in(cities));
        searchCriterion.and(qProperty.saleType.in(saleTypes));
        propertyDTOJPAQuery
                .where(searchCriterion)
                .orderBy(qProperty.qualityScore2.desc())
                .limit(4L);
        List<PropertyDTO> propertyDTOS = propertyDTOJPAQuery.fetch();
        if (propertyDTOS == null || propertyDTOS.isEmpty()) {
            return Collections.emptyList();
        } else {
            return propertyDTOS;
        }
    }

    /*
     *
     *  Create and fetch the count for a particular search criteria(criterion) of the Property.java class.
     *
     * */
    public Long createCountQueryForProperties(BooleanBuilder booleanBuilder) {

        JPAQuery<Long> countQueryForProperties = getHibernateHintedJPAQuery()
                .select(qProperty.propertyId.count())
                .from(qProperty)
                .where(booleanBuilder)
                .orderBy(qProperty.qualityScore2.desc());

        Long count = countQueryForProperties.fetchOne();
        return count;
    }

}
