package com.nic.properties.repositoryImpl;

import com.nic.properties.QEntities.QTBLPropertyTaxonomy;
import com.nic.properties.helper.QJPAHelper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PropertyTaxonomyDAO extends QJPAHelper {

    private final QTBLPropertyTaxonomy qPropertyTaxonomy;

    public PropertyTaxonomyDAO() {
        this.qPropertyTaxonomy = QTBLPropertyTaxonomy.tBLPropertyTaxonomy;
    }

    public List<String> getSaleGroups() {
        List<String> saleGroups = getHibernateHintedJPAQuery()
                .select(qPropertyTaxonomy.saleGroup)
                .from(qPropertyTaxonomy)
                .distinct()
                .fetch();
        return saleGroups.stream().map(String::toLowerCase).collect(Collectors.toList());
    }

    public List<String> getAllSaleTypes() {
        List<String> saleTypes = getHibernateHintedJPAQuery()
                .select(qPropertyTaxonomy.saleType)
                .from(qPropertyTaxonomy)
                .distinct()
                .orderBy(qPropertyTaxonomy.saleType.asc())
                .fetch();
        return saleTypes.stream().map(String::toLowerCase).collect(Collectors.toList());
    }

    public List<String> getBySaleGroupType(String saleGroup) {
        List<String> saleTypes = getHibernateHintedJPAQuery()
                .select(qPropertyTaxonomy.saleType)
                .from(qPropertyTaxonomy)
                .where(qPropertyTaxonomy.saleGroup.equalsIgnoreCase(saleGroup))
                .fetch();
        return saleTypes.stream().map(String::toLowerCase).collect(Collectors.toList());
    }

}
