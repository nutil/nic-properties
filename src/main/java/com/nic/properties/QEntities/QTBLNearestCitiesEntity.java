package com.nic.properties.QEntities;

import com.nic.properties.entity.TBLNearestCitiesEntity;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.processing.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QTBLNearestCitiesEntity is a Querydsl query type for TBLNearestCitiesEntity
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTBLNearestCitiesEntity extends EntityPathBase<TBLNearestCitiesEntity> {

    public static final QTBLNearestCitiesEntity tBLNearestCitiesEntity = new QTBLNearestCitiesEntity("tBLNearestCitiesEntity");
    private static final long serialVersionUID = -1567362321L;
    public final StringPath city = createString("city");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final NumberPath<Double> proximity = createNumber("proximity", Double.class);

    public final StringPath searchCity = createString("searchCity");

    public final StringPath searchState = createString("searchState");

    public final StringPath state = createString("state");

    public QTBLNearestCitiesEntity(String variable) {
        super(TBLNearestCitiesEntity.class, forVariable(variable));
    }

    public QTBLNearestCitiesEntity(Path<? extends TBLNearestCitiesEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTBLNearestCitiesEntity(PathMetadata metadata) {
        super(TBLNearestCitiesEntity.class, metadata);
    }

}

