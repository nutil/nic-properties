package com.nic.properties.QEntities;

import com.nic.properties.entity.Property;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.*;

import javax.annotation.processing.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QProperty is a Querydsl query type for Property
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QProperty extends EntityPathBase<Property> {

    public static final QProperty property = new QProperty("property");
    private static final long serialVersionUID = 1796571800L;
    public final NumberPath<Long> addressId = createNumber("addressId", Long.class);

    public final BooleanPath altFinance = createBoolean("altFinance");

    public final NumberPath<Integer> amount = createNumber("amount", Integer.class);

    public final StringPath amountDefinition = createString("amountDefinition");

    public final StringPath appliances = createString("appliances");

    public final NumberPath<Integer> assessedTax = createNumber("assessedTax", Integer.class);

    public final NumberPath<Integer> assessedTaxYear = createNumber("assessedTaxYear", Integer.class);

    public final NumberPath<Integer> assessedValuation = createNumber("assessedValuation", Integer.class);

    public final NumberPath<Integer> assessedValuationImprovement = createNumber("assessedValuationImprovement", Integer.class);

    public final NumberPath<Integer> assessedValuationLand = createNumber("assessedValuationLand", Integer.class);

    public final NumberPath<Double> assessedValuationRate = createNumber("assessedValuationRate", Double.class);

    public final NumberPath<Integer> assessedValuationYear = createNumber("assessedValuationYear", Integer.class);

    public final NumberPath<Integer> assessedYearDelinquent = createNumber("assessedYearDelinquent", Integer.class);

    public final DatePath<java.sql.Date> auctionDate = createDate("auctionDate", java.sql.Date.class);

    public final TimePath<java.sql.Time> auctionTime = createTime("auctionTime", java.sql.Time.class);

    public final BooleanPath bargainPrice = createBoolean("bargainPrice");

    public final StringPath basement = createString("basement");

    public final NumberPath<java.math.BigDecimal> bathsTotal = createNumber("bathsTotal", java.math.BigDecimal.class);

    public final NumberPath<Integer> bedrooms = createNumber("bedrooms", Integer.class);

    public final StringPath city = createString("city");

    public final DatePath<java.sql.Date> closeDate = createDate("closeDate", java.sql.Date.class);

    public final NumberPath<Integer> closePrice = createNumber("closePrice", Integer.class);

    public final StringPath condition = createString("condition");

    public final StringPath constructionType = createString("constructionType");

    public final StringPath cooling = createString("cooling");

    public final StringPath county = createString("county");

    public final StringPath dataSources = createString("dataSources");

    public final StringPath den = createString("den");

    public final StringPath description = createString("description");

    public final StringPath elementaryschool = createString("elementaryschool");

    public final NumberPath<Double> estimatedConfidence = createNumber("estimatedConfidence", Double.class);

    public final NumberPath<Integer> estimatedHigh = createNumber("estimatedHigh", Integer.class);

    public final NumberPath<Integer> estimatedLow = createNumber("estimatedLow", Integer.class);

    public final NumberPath<Integer> estimatedMonthlyPayment = createNumber("estimatedMonthlyPayment", Integer.class);

    public final NumberPath<Integer> estimatedRent = createNumber("estimatedRent", Integer.class);

    public final NumberPath<Integer> estimatedValue = createNumber("estimatedValue", Integer.class);

    public final DateTimePath<java.sql.Timestamp> estimatedValueDate = createDateTime("estimatedValueDate", java.sql.Timestamp.class);

    public final BooleanPath exclusive = createBoolean("exclusive");

    public final StringPath exteriorWalls = createString("exteriorWalls");

    public final StringPath familyRoom = createString("familyRoom");

    public final StringPath features = createString("features");

    public final StringPath fips = createString("fips");

    public final DatePath<java.sql.Date> firstLook = createDate("firstLook", java.sql.Date.class);

    public final DateTimePath<java.sql.Timestamp> firstMergeDate = createDateTime("firstMergeDate", java.sql.Timestamp.class);

    public final BooleanPath fixerUpper = createBoolean("fixerUpper");

    public final StringPath floorCovering = createString("floorCovering");

    public final StringPath foundation = createString("foundation");

    public final StringPath fullStreetName = createString("fullStreetName");

    public final DatePath<java.sql.Date> gnndExpDate = createDate("gnndExpDate", java.sql.Date.class);

    public final StringPath heating = createString("heating");

    public final StringPath highSchool = createString("highSchool");

    public final BooleanPath hmpthMortgage = createBoolean("hmpthMortgage");

    public final BooleanPath hmpthRenovationMortgage = createBoolean("hmpthRenovationMortgage");

    public final StringPath hoaFee = createString("hoaFee");

    public final NumberPath<Integer> homeScore = createNumber("homeScore", Integer.class);

    public final StringPath hud203kEligible = createString("hud203kEligible");

    public final StringPath hudBidDeadline = createString("hudBidDeadline");

    public final NumberPath<Integer> hudEscrowAmount = createNumber("hudEscrowAmount", Integer.class);

    public final StringPath hudPriority = createString("hudPriority");

    public final StringPath hudSaleStatus = createString("hudSaleStatus");

    public final BooleanPath hundredDownPayment = createBoolean("hundredDownPayment");

    public final NumberPath<Integer> investorScore = createNumber("investorScore", Integer.class);

    public final NumberPath<Integer> judgmentAmount = createNumber("judgmentAmount", Integer.class);

    public final StringPath kitchen = createString("kitchen");

    public final NumberPath<Double> latitude = createNumber("latitude", Double.class);

    public final StringPath legalDescription = createString("legalDescription");

    public final StringPath lispendCaseNo = createString("lispendCaseNo");

    public final StringPath lispendDocketNo = createString("lispendDocketNo");

    public final StringPath lispendDocNo = createString("lispendDocNo");

    public final StringPath lispendIndexNo = createString("lispendIndexNo");

    public final DatePath<java.sql.Date> lispendLoanDate = createDate("lispendLoanDate", java.sql.Date.class);

    public final StringPath lispendLoanNo = createString("lispendLoanNo");

    public final DatePath<java.sql.Date> lispendRecordingDate = createDate("lispendRecordingDate", java.sql.Date.class);

    public final DatePath<java.sql.Date> listDate = createDate("listDate", java.sql.Date.class);

    public final StringPath listingStatus = createString("listingStatus");

    public final StringPath listingUrl = createString("listingUrl");

    public final NumberPath<Integer> listPrice = createNumber("listPrice", Integer.class);

    public final StringPath livingArea = createString("livingArea");

    public final NumberPath<Integer> livingAreaSquareFeet = createNumber("livingAreaSquareFeet", Integer.class);

    public final StringPath livingRoom = createString("livingRoom");

    public final NumberPath<Double> longitude = createNumber("longitude", Double.class);

    public final StringPath lotSize = createString("lotSize");

    public final StringPath middleSchool = createString("middleSchool");

    public final StringPath mlsDisclaimer = createString("mlsDisclaimer");

    public final StringPath mlsNumber = createString("mlsNumber");

    public final NumberPath<Integer> monthlyAmount30yearFixedMortgage = createNumber("monthlyAmount30yearFixedMortgage", Integer.class);

    public final NumberPath<Integer> monthlyAmountRTOHenrysAlgo = createNumber("monthlyAmountRTOHenrysAlgo", Integer.class);

    public final NumberPath<Double> nodAmountDefault = createNumber("nodAmountDefault", Double.class);

    public final DatePath<java.sql.Date> nodDateDefault = createDate("nodDateDefault", java.sql.Date.class);

    public final DatePath<java.sql.Date> nodDateDefaultedLien = createDate("nodDateDefaultedLien", java.sql.Date.class);

    public final NumberPath<Integer> nodDocNumberDefaultedLien = createNumber("nodDocNumberDefaultedLien", Integer.class);

    public final StringPath nodDocumentNumber = createString("nodDocumentNumber");

    public final DatePath<java.sql.Date> nodRecordingDate = createDate("nodRecordingDate", java.sql.Date.class);

    public final NumberPath<Integer> nodRecordingYear = createNumber("nodRecordingYear", Integer.class);

    public final StringPath notsAuctionAddress = createString("notsAuctionAddress");

    public final StringPath notsAuctionCity = createString("notsAuctionCity");

    public final StringPath notsAuctionDescription = createString("notsAuctionDescription");

    public final StringPath notsAuctionDocumentNumber = createString("notsAuctionDocumentNumber");

    public final StringPath notsAuctionHouseName = createString("notsAuctionHouseName");

    public final DatePath<java.sql.Date> notsAuctionRecordingDate = createDate("notsAuctionRecordingDate", java.sql.Date.class);

    public final StringPath notsAuctionState = createString("notsAuctionState");

    public final StringPath notsAuctionTerms = createString("notsAuctionTerms");

    public final StringPath notsAuctionTitle = createString("notsAuctionTitle");

    public final NumberPath<Integer> notsAuctionZIP = createNumber("notsAuctionZIP", Integer.class);

    public final StringPath notsIndexNo = createString("notsIndexNo");

    public final DatePath<java.sql.Date> notsLoanDate = createDate("notsLoanDate", java.sql.Date.class);

    public final NumberPath<Double> NotsLoanDefaultAmount = createNumber("NotsLoanDefaultAmount", Double.class);

    public final StringPath notsLoanNo = createString("notsLoanNo");

    public final NumberPath<Double> notsOpeningBid = createNumber("notsOpeningBid", Double.class);

    public final StringPath notsTrusteeSaleNumber = createString("notsTrusteeSaleNumber");

    public final BooleanPath nspHome = createBoolean("nspHome");

    public final NumberPath<Integer> originalLoanAmount = createNumber("originalLoanAmount", Integer.class);

    public final BooleanPath paidListing = createBoolean("paidListing");

    public final StringPath parcelNumber = createString("parcelNumber");

    public final StringPath parking = createString("parking");

    public final StringPath parkingSpaces = createString("parkingSpaces");

    public final NumberPath<Integer> pictureCount = createNumber("pictureCount", Integer.class);

    public final StringPath pictureDataSourceUrl = createString("pictureDataSourceUrl");

    public final BooleanPath pictureRemote = createBoolean("pictureRemote");

    public final StringPath pool = createString("pool");

    public final StringPath postal_Code = createString("postal_Code");

    public final BooleanPath priceReduction = createBoolean("priceReduction");

    public final NumberPath<Long> propertyId = createNumber("propertyId", Long.class);

    public final StringPath propertyRecordType = createString("propertyRecordType");

    public final NumberPath<Integer> qualityScore1 = createNumber("qualityScore1", Integer.class);

    public final NumberPath<Integer> qualityScore10 = createNumber("qualityScore10", Integer.class);

    public final NumberPath<Integer> qualityScore2 = createNumber("qualityScore2", Integer.class);

    public final NumberPath<Integer> qualityScore3 = createNumber("qualityScore3", Integer.class);

    public final NumberPath<Integer> qualityScore4 = createNumber("qualityScore4", Integer.class);

    public final NumberPath<Integer> qualityScore5 = createNumber("qualityScore5", Integer.class);

    public final NumberPath<Integer> qualityScore6 = createNumber("qualityScore6", Integer.class);

    public final NumberPath<Integer> qualityScore7 = createNumber("qualityScore7", Integer.class);

    public final NumberPath<Integer> qualityScore8 = createNumber("qualityScore8", Integer.class);

    public final NumberPath<Integer> qualityScore9 = createNumber("qualityScore9", Integer.class);

    public final NumberPath<Integer> qualityScoreMember = createNumber("qualityScoreMember", Integer.class);

    public final NumberPath<Integer> qualityScoreSales = createNumber("qualityScoreSales", Integer.class);

    public final NumberPath<Integer> rent = createNumber("rent", Integer.class);

    public final StringPath reoDocumentNumber = createString("reoDocumentNumber");

    public final DatePath<java.sql.Date> reoRecordingDate = createDate("reoRecordingDate", java.sql.Date.class);

    public final StringPath roofType = createString("roofType");

    public final StringPath roomList = createString("roomList");

    public final BooleanPath rtoFinancing = createBoolean("rtoFinancing");

    public final BooleanPath rtoPotential = createBoolean("rtoPotential");

    public final StringPath saleType = createString("saleType");

    public final StringPath schoolDistrict = createString("schoolDistrict");

    public final NumberPath<Integer> sortAmount = createNumber("sortAmount", Integer.class);

    public final BooleanPath specialFinancing = createBoolean("specialFinancing");

    public final BooleanPath specialtyHome = createBoolean("specialtyHome");

    public final StringPath stateOrProvince = createString("stateOrProvince");

    public final StringPath stories = createString("stories");

    public final StringPath streetAddress = createString("streetAddress");

    public final StringPath style = createString("style");

    public final StringPath taxSaleBidMethod = createString("taxSaleBidMethod");

    public final BooleanPath taxSaleGetPremiumBack = createBoolean("taxSaleGetPremiumBack");

    public final StringPath taxSaleHowOften = createString("taxSaleHowOften");

    public final StringPath taxSaleJurisdictionName = createString("taxSaleJurisdictionName");

    public final StringPath taxSaleRate = createString("taxSaleRate");

    public final StringPath taxSaleRedemtionPeriod = createString("taxSaleRedemtionPeriod");

    public final NumberPath<Integer> totalRooms = createNumber("totalRooms", Integer.class);

    public final NumberPath<Integer> totalUnits = createNumber("totalUnits", Integer.class);

    public final StringPath tracking = createString("tracking");

    public final NumberPath<Integer> transferValue = createNumber("transferValue", Integer.class);

    public final StringPath unitNumber = createString("unitNumber");

    public final StringPath urlHex = createString("urlHex");

    public final StringPath viewType = createString("viewType");

    public final DatePath<java.sql.Date> YearBuilt = createDate("YearBuilt", java.sql.Date.class);

    public final StringPath zoning = createString("zoning");

    public QProperty(String variable) {
        super(Property.class, forVariable(variable));
    }

    public QProperty(Path<? extends Property> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProperty(PathMetadata metadata) {
        super(Property.class, metadata);
    }

}

