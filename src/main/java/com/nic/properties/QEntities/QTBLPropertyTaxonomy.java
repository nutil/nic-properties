package com.nic.properties.QEntities;

import com.nic.properties.entity.TBLPropertyTaxonomy;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import javax.annotation.processing.Generated;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;


/**
 * QTBLPropertyTaxonomy is a Querydsl query type for TBLPropertyTaxonomy
 */
@Generated("com.querydsl.codegen.DefaultEntitySerializer")
public class QTBLPropertyTaxonomy extends EntityPathBase<TBLPropertyTaxonomy> {

    public static final QTBLPropertyTaxonomy tBLPropertyTaxonomy = new QTBLPropertyTaxonomy("tBLPropertyTaxonomy");
    private static final long serialVersionUID = -849123455L;
    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath mainCategory = createString("mainCategory");

    public final StringPath saleGroup = createString("saleGroup");

    public final StringPath saleType = createString("saleType");

    public QTBLPropertyTaxonomy(String variable) {
        super(TBLPropertyTaxonomy.class, forVariable(variable));
    }

    public QTBLPropertyTaxonomy(Path<? extends TBLPropertyTaxonomy> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTBLPropertyTaxonomy(PathMetadata metadata) {
        super(TBLPropertyTaxonomy.class, metadata);
    }

}

