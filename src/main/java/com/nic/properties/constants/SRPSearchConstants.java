package com.nic.properties.constants;

import java.util.ArrayList;
import java.util.List;

public class SRPSearchConstants {
    public static final List<String> sale = new ArrayList<>();
    public static final List<String> foreClosures = new ArrayList<>();
    public static final List<String> preForeClosures = new ArrayList<>();
    public static final List<String> bankOwnedREO = new ArrayList<>();
    public static final List<String> hudHomes = new ArrayList<>();
    public static final List<String> shortSales = new ArrayList<>();
    public static final List<String> houseAuctions = new ArrayList<>();
    public static final List<String> rentToOwn = new ArrayList<>();
    public static final List<String> houseForRent = new ArrayList<>();
    public static final List<String> FannieMaeOrFreddieMac = new ArrayList<>();
    public static final List<String> FSBO = new ArrayList<>();
    public static final List<String> TaxLien = new ArrayList<>();
    public static final List<String> VAForeclosure = new ArrayList<>();
    public static final List<String> PreForeclosureLisPendens = new ArrayList<>();
    public static final List<String> PreForeclosureNOD = new ArrayList<>();

    static {
        sale.add("FSBO");
        sale.add("ForeclosuresRS2");
        sale.add("Pre-Foreclosures");
        sale.add("Banked Owned");

        foreClosures.add("Pre-Foreclosures");
        foreClosures.add("Banked Owned & REO");
        foreClosures.add("Home Auctions");
        foreClosures.add("For Sale");

        preForeClosures.add("ForeclosuresRS2");
        preForeClosures.add("Banked Owned & REO");
        preForeClosures.add("Home Auctions");
        preForeClosures.add("For Sale");

        bankOwnedREO.add("ForeclosuresRS2");
        bankOwnedREO.add("Pre-Foreclosures");
        bankOwnedREO.add("Home Auctions");
        bankOwnedREO.add("For Sale");

        hudHomes.add("ForeclosuresRS2");
        hudHomes.add("Pre-Foreclosures");
        hudHomes.add("Home Auctions");
        hudHomes.add("For Sale");

        shortSales.add("ForeclosuresRS2");
        shortSales.add("Pre-Foreclosures");
        shortSales.add("Home Auctions");
        shortSales.add("For Sale");

        houseAuctions.add("ForeclosuresRS2");
        houseAuctions.add("Pre-Foreclosures");
        houseAuctions.add("Bank Owned & REO");
        houseAuctions.add("For Sale");

        rentToOwn.add("Homes for Rent");
        rentToOwn.add("Foreclosures");
        rentToOwn.add("For Sale");

        houseForRent.add("Rent to Own");
        houseForRent.add("Foreclosures");
        houseForRent.add("For Sale");

        FSBO.add("FSBO");
        PreForeclosureLisPendens.add("PreForeclosureLisPendens");
        PreForeclosureNOD.add("PreForeclosureNOD");
        VAForeclosure.add("VAForeclosure");
        FannieMaeOrFreddieMac.add("FannieMaeOrFreddieMac");
        TaxLien.add("TaxLien");
    }
}
