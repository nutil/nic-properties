package com.nic.properties.configuration;

import com.nic.properties.entity.StateDetail;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;

@Component
@Primary
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.nic.properties.repository",
        entityManagerFactoryRef = "pdbEntityManagerFactory",
        transactionManagerRef = "pdbTransactionManager"
)
public class PdbConfiguration {
    @Bean
    @ConfigurationProperties("app.datasource.pdb")
    public DataSourceProperties pdbDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties("app.datasource.pdb.configuration")
    public DataSource pdbDataSource() {
        return pdbDataSourceProperties().initializeDataSourceBuilder()
                .type(HikariDataSource.class).build();
    }

    @Bean
    public EntityManagerFactoryBuilder entityManagerFactoryBuilder() {
        return new EntityManagerFactoryBuilder(new HibernateJpaVendorAdapter(), new HashMap<>(), null);
    }

    @Primary
    @Bean(name = "pdbEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean pdbEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(pdbDataSource())
                .packages(StateDetail.class)
                .build();
    }

    @Bean
    public PlatformTransactionManager pdbTransactionManager(
            final @Qualifier("pdbEntityManagerFactory") LocalContainerEntityManagerFactoryBean pdbEntityManagerFactory) {
        return new JpaTransactionManager(pdbEntityManagerFactory.getObject());
    }

    @Bean
    @Qualifier(value = "entityManager")
    public EntityManager entityManager(EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}


