package com.nic.properties.configuration;

import com.nic.properties.cache.Memcached;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.*;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collection;

@Configuration
@EnableCaching
public class MemcachedConfiguration implements CachingConfigurer {


    @Value("${memcached.addresses}")
    private String memcachedAddresses;

    @Value("${memcached.expiration.sec}")
    private int expirationSec;

    /*@Autowired
    CacheManager cacheManager;*/


    @Override
    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        try {
            cacheManager.setCaches(internalCaches());
            return cacheManager;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Collection<Memcached> internalCaches() throws IOException {
        final Collection<Memcached> caches = new ArrayList<>();
        caches.add(new Memcached("performDBAction", memcachedAddresses, expirationSec));
        caches.add(new Memcached("NearByCounty", memcachedAddresses, expirationSec));
        caches.add(new Memcached("NearByCity", memcachedAddresses, expirationSec));
        caches.add(new Memcached("NearByZips", memcachedAddresses, expirationSec));
        caches.add(new Memcached("pdpNearBy", memcachedAddresses, expirationSec));
        caches.add(new Memcached("searchAddressSubmit", memcachedAddresses, expirationSec));
        caches.add(new Memcached("fetchSearchResultsForMiniSRP", memcachedAddresses, expirationSec));
        caches.add(new Memcached("fetchSearchResultsForPDPByZip", memcachedAddresses, expirationSec));
        caches.add(new Memcached("findEstimatedValueHistory", memcachedAddresses, expirationSec));
        caches.add(new Memcached("findSmartzipComparables", memcachedAddresses, expirationSec));
        caches.add(new Memcached("getPdp", memcachedAddresses, expirationSec));
        caches.add(new Memcached("fetchSearchResultsForPDPByZipAndCost", memcachedAddresses, expirationSec));
        caches.add(new Memcached("fetchSearchResultsForPDPByCounty", memcachedAddresses, expirationSec));
        caches.add(new Memcached("fetchSearchResultsForPDPByCost", memcachedAddresses, expirationSec));
        caches.add(new Memcached("fetchSearchResultsForPDPByPropertyId", memcachedAddresses, expirationSec));
        caches.add(new Memcached("findAssociatesForPDP", memcachedAddresses, expirationSec));
        caches.add(new Memcached("fetchCountyResultCounts", memcachedAddresses, expirationSec));
        caches.add(new Memcached("fetchPostalDetails", memcachedAddresses, expirationSec));
        caches.add(new Memcached("fetchDataByFilteringWoImages", memcachedAddresses, expirationSec));
        caches.add(new Memcached("getImagesByPid", memcachedAddresses, expirationSec));
        caches.add(new Memcached("findByStateAndCityAndMultipleSaleTypesForMiniSRP", memcachedAddresses, expirationSec));
        return caches;
    }

    @Override
    public KeyGenerator keyGenerator() {
        return new SimpleKeyGenerator() {

            @SneakyThrows
            @Override
            public Object generate(Object target, Method method, Object... params) {

                String str = "";
                for (Object p : params) {
                    if (p != null)
                        str = str + "," + p;
                }
                MessageDigest digest = MessageDigest.getInstance("SHA-1");
                digest.reset();
                digest.update(str.getBytes(StandardCharsets.UTF_8));
                str = String.format("%040x", new BigInteger(1, digest.digest()));

                return target.getClass().getName() + "/" + method.getName() + str;
            }

        };
    }

    @Override
    public CacheErrorHandler errorHandler() {
        return new SimpleCacheErrorHandler();
    }

    @Override
    public CacheResolver cacheResolver() {
        return null;
    }


}

