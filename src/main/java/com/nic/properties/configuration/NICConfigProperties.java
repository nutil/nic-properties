package com.nic.properties.configuration;

public final class NICConfigProperties {
    public static final String HIBERNATE_HINT = "org.hibernate.readOnly";
    public static final boolean HIBERNATE_HINT_VALUE = true;
    public static final long RESULT_LIMIT = 10L;
}
