package com.nic.properties.configuration;

import com.nic.properties.entity.MedianHomePriceByGeo;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Component
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.nic.properties.repository.smartZip",
        entityManagerFactoryRef = "smartZipEntityManagerFactory",
        transactionManagerRef = "smartZipTransactionManager"
)
public class SmartZipConfiguration {

    @Bean
    @ConfigurationProperties("app.datasource.smartzip")
    public DataSourceProperties smartZipDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties("app.datasource.smartzip.configuration")
    public DataSource smartZipDataSource() {
        return smartZipDataSourceProperties().initializeDataSourceBuilder()
                .type(HikariDataSource.class).build();
    }

    @Bean(name = "smartZipEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean smartZipEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(smartZipDataSource())
                .packages(MedianHomePriceByGeo.class)
                .build();
    }

    @Bean
    public PlatformTransactionManager smartZipTransactionManager(
            final @Qualifier("smartZipEntityManagerFactory") LocalContainerEntityManagerFactoryBean smartZipEntityManagerFactory) {
        return new JpaTransactionManager(smartZipEntityManagerFactory.getObject());
    }
}




