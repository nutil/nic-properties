package com.nic.properties.sevice;

import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class StreetName implements Serializable {

    Pattern streetPattern = Pattern.compile("^(\\b\\D+\\b)?\\s*(\\b.*?\\d.*?\\b)\\s*(\\b\\D+\\b)?$");

    public String getStreet(String streetAddress) {
        String streetName = null;
        String streetNumber = null;
        if (streetAddress != null) {
            Matcher m = streetPattern.matcher(streetAddress);
            if (m.find()) {
                String group1 = m.group(1);
                String group3 = m.group(3);
                if (group1 == null && group3 == null) {
                    return streetAddress.replaceAll("^[0-9]+", "");
                } else if (group1 != null && group3 != null) {
                    return streetAddress.replaceAll("^[0-9]+", "");
                } else {
                    streetName = (group1 != null) ? group1 : group3;
                    streetNumber = m.group(2);
                }
            } else {
                return streetAddress;
            }
        }
        return streetName;
    }

    public String getStreetNumber(String streetAddress) {
        String streetName = null;
        String streetNumber = null;
        if (streetAddress != null) {
            Matcher m = streetPattern.matcher(streetAddress);
            if (m.find()) {
                String group1 = m.group(1);
                String group3 = m.group(3);
                if (group1 == null && group3 == null) {
                    return streetAddress.replaceAll("\\s.*", "");
                } else if (group1 != null && group3 != null) {
                    return streetAddress.replaceAll("\\s.*", "");
                } else {
                    streetName = (group1 != null) ? group1 : group3;
                    streetNumber = m.group(2);
                }
            }
        }
        return streetNumber;
    }
}
