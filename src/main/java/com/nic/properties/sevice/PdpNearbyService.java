package com.nic.properties.sevice;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nic.properties.entity.MedianHomePriceByGeo;
import com.nic.properties.model.NearByMediamHomePriceByGeoModel;
import com.nic.properties.repository.CountyRepository;
import com.nic.properties.repository.SRPRepository;
import com.nic.properties.repository.ZipRepository;
import com.nic.properties.repository.smartZip.MedianHomePriceByGeoRepositoryPdp;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PdpNearbyService {

    Logger log = LogManager.getLogger(PdpNearbyService.class);
    @Autowired
    private CountyRepository countyRepository;
    @Autowired
    private SRPRepository srpRepository;
    @Autowired
    private ZipRepository zipRepository;
    @Autowired
    private MedianHomePriceByGeoRepositoryPdp medianHomePriceByGeoRepository;

    /**
     * This service is intended to fetch the nearby state, county, cities, zip for a particular saleType
     *
     * @param state
     * @param county
     * @param city
     * @param zip
     * @param saleType
     * @return
     */
    @Cacheable("pdpNearBy")
    public Map<String, Object> getPdpNearby(String state, String county, String city, Integer zip, String saleType) {
        log.info("Getting near by Counties, Cities, zip");
        Map<String, Object> zipMap = new HashMap<>();
        List<String> saleTypesList = getSaleTypesList(saleType);
        List<NearByMediamHomePriceByGeoModel> nearByMediamHomePriceByGeoModelList = new ArrayList<>();

        //Integer fips = countyRepository.getfipByZip(zip, saleTypesList);
       // Integer fips = countyRepository.getfips(state, county, saleTypesList);

        zipMap.put("NearBy Counties", medianHomePriceByGeoRepository.getMedianHomePriceListByFips(state, county, saleTypesList));

        nearByMediamHomePriceByGeoModelList = convertEntityList(medianHomePriceByGeoRepository.getMedianHomePriceByCity(state, city, saleTypesList));
        nearByMediamHomePriceByGeoModelList.parallelStream().forEach(x -> {
            x.setCounty(countyRepository.getCountyByCity(new StringTokenizer(x.getGeo(), ",").nextToken()));
        });
        zipMap.put("NearBy Cities", nearByMediamHomePriceByGeoModelList);
        nearByMediamHomePriceByGeoModelList = convertEntityList(medianHomePriceByGeoRepository.getMedianHomePriceByZip(zip, saleTypesList));

        nearByMediamHomePriceByGeoModelList.parallelStream().forEach(x -> {
            x.setCounty(countyRepository.getCountyByZip(Integer.valueOf(x.getGeo())));
            x.setCity(zipRepository.getcity(Integer.valueOf(x.getGeo())));
            x.setState(zipRepository.getStateByZip(Integer.valueOf(x.getGeo())));
            if(x.getGeo().length()==4)
                x.setGeo("0"+x.getGeo());
        });
        zipMap.put("nearby Zip", nearByMediamHomePriceByGeoModelList);

        return zipMap;
    }

    /**
     * @param saleType
     * @return
     * @code this method is intended to fetch the list of saleTypes in the saleGroup types.
     */
    public List<String> getSaleTypesList(String saleType) {
        List<String> saleTypeList = new ArrayList<String>();
        if (saleType.equalsIgnoreCase("ForeclosuresRS2") || saleType.equalsIgnoreCase("PreForeclosures") || saleType.equalsIgnoreCase("Auctions")) {
            saleTypeList = srpRepository.fetchForeclosureTypes(saleType);
        } else {
            saleTypeList.add(saleType);
        }
        return saleTypeList;
    }

    /**
     * @param entityList
     * @return
     * @code this method is intended to convert entityList
     */
    public List<NearByMediamHomePriceByGeoModel> convertEntityList(List<MedianHomePriceByGeo> entityList) {
        if (CollectionUtils.isEmpty(entityList)) {
            return new ArrayList<>();
        }
        ObjectMapper objMap = new ObjectMapper();
        return objMap.convertValue(entityList, new TypeReference<List<NearByMediamHomePriceByGeoModel>>() {
        });
    }
}
