package com.nic.properties.sevice;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.nic.properties.entity.SmartzipComparable;
import com.nic.properties.entity.SmartzipCompsResult;
import com.nic.properties.repository.PDPRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.util.List;
import java.util.Locale;

@Service
public class SmartZipService {

    private static final String RESPONSE_JSON = " &responseJson: ";
    Logger log = LogManager.getLogger(SmartZipService.class);
    ResourceBundleMessageSource messageSource;
    Integer MONTHS_BACK = 12;
    Integer MAX_SALES = 20;
    @Autowired
    PdpService pdpService;
    @Autowired
    PDPRepository pDPRepository;
    @Autowired
    StreetName streetName;

    /**
     * @param szId
     * @return
     * @code this method is intended to frame url and hit smart zip api
     */
    @Cacheable("findSmartzipComparables")
    public SmartzipCompsResult findSmartzipComparables(String szId) {
        log.info(" Getting data from smart Zip " + szId);
        SmartzipCompsResult result = new SmartzipCompsResult();

        messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages/smartzip");

        String key = messageSource.getMessage("smartzip.brand.default", null, Locale.US);
        StringBuilder url = new StringBuilder();
        url.append(messageSource.getMessage("smartzip.api.url.comps", null, Locale.US));
        url.append("?api_key=").append(key);
        url.append("&property_id=" + szId);
        url.append("&months_back=").append(MONTHS_BACK);
        url.append("&max_results=").append(MAX_SALES);
        URL smartzipUrl = null;
        try {
            smartzipUrl = new URL(url.toString());
        } catch (Exception e) {
            log.error("Error while creating URL : " + url + " " + e.getLocalizedMessage());
        }
        String smartzipCallUrl = null;
        if (null != smartzipUrl) {
            smartzipCallUrl = smartzipUrl.toString();
        } else {
            smartzipCallUrl = url.toString();
        }
        try {
            result = smartZipRestCall(smartzipCallUrl, szId);
        } catch (Exception e) {
            log.error("Error occurred: " + e.getLocalizedMessage());
        }
        List<SmartzipComparable> compList = result.getComparables();
        compList.forEach(temp ->
        {
            temp.setPropertyId(pDPRepository.getPropertyIdComp(temp.id));
            String address = temp.getAddress();
            if (address != null)
                temp.setAddress(streetName.getStreet(address));
        });
        log.info("Completed SmartZip api call and returned the result.");
        return result;
    }

    /**
     * @param smartzipCallUrl
     * @param szId
     * @return
     * @code this method is intended to call smart zip api
     */
    private SmartzipCompsResult smartZipRestCall(String smartzipCallUrl, String szId) {
        log.info(" Calling Smart Zip api");
        SmartzipCompsResult result = new SmartzipCompsResult();
        RestTemplate restTemplate = new RestTemplate();
        String responseJson = null;
        try {
            responseJson = restTemplate.getForObject(smartzipCallUrl, String.class);
            if (responseJson != null) {
                result = new Gson().fromJson(responseJson, SmartzipCompsResult.class);
                if (CollectionUtils.isEmpty(result.getComparables())) {
                    log.info("SmartzipCompsResult :: comparable_properties are empty for this property :: SmartZip ID: " + szId + RESPONSE_JSON + responseJson);
                }
            }
        } catch (JsonParseException e) {
            log.error("SmartzipCompsResult :: SmartZip ID: " + szId + RESPONSE_JSON + responseJson + " & JsonParseException: " + e.getLocalizedMessage());
        } catch (Exception e) {
            log.error("SmartzipCompsResult :: Comparable sales data not found for this property (or) Error in calling Smartzip API:: SmartZip ID: " + szId + RESPONSE_JSON + responseJson + " & Smartzip API Exception: " + e.getLocalizedMessage());
        }
        return result;
    }
}
