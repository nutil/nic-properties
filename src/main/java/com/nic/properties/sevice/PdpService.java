package com.nic.properties.sevice;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nic.properties.entity.*;
import com.nic.properties.model.MiniSRPPropertyModel;
import com.nic.properties.model.TBLImageModel;
import com.nic.properties.model.TBLPropertyModel;
import com.nic.properties.model.TblePropertyModelSRPFilter;
import com.nic.properties.repository.*;
import com.nic.properties.srpenum.ImageSizeEnum;
import com.nic.properties.util.ImageUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class PdpService {
    private static final String Foreclosure = "Foreclosures";
    private static final String RENTTOOWN = "RentToOwn";
    Logger log = LogManager.getLogger(PdpService.class);

    ResourceBundleMessageSource messageSource;
    @Autowired
    private PdpDetailRepository pdpDetailRepository;
    @Autowired
    private PropertyTaxonomyRepository propertyTaxonomyRepository;
    @Autowired
    private ImageRepository imageRepository;
    @Autowired
    private FsboOrMlsPropertyRepository fsboOrMlsPropertyRepository;
    @Autowired
    private PDPRepository repository;


    @Autowired
    private TblePropertyEntitySRPFilterRepository tblePropertyEntitySRPFilterRepository;
    @Autowired
    private PDPValueHistoryRepository historyRepository;
    @Autowired
    private PDPAssociateRepository pdpAssociateRepository;
    @Autowired
    private ImageUtils imageUtils;
    @Autowired
    private SRPRepository srpRepository;
    @Autowired
    private StreetName streetName;

    /**
     * @param cityName
     * @param state
     * @param zip
     * @param saletype
     * @return
     * @code this method is intended to fetch search results for PDP by zip
     */
    @Cacheable("fetchSearchResultsForPDPByZip")
    public List<TBLPropertyModel> fetchSearchResultsForPDPByZip(String cityName, String state, String zip, String saletype) {
        log.info("Fetching search results for PDP by zip ");
        List<String> saleTypesList = getSaleTypesList(saletype);
        List<TBLPropertyEntity> fetchSearchResultsForPDPVAFC = repository
                .fetchSearchResultsForPDPByZip(saleTypesList, cityName, state, zip);
        ObjectMapper objMap = new ObjectMapper();
        List<TBLPropertyModel> convertValue = objMap.convertValue(fetchSearchResultsForPDPVAFC, new TypeReference<List<TBLPropertyModel>>() {
        });
        convertValue.forEach(x -> {
            if ((x.getStreetName() == null)) {
                x.setStreetName(streetName.getStreet(x.getStreetAddress()));
            }
            if (!(x.getSaleType().equalsIgnoreCase("FSBO")) && !(x.getSaleType().equalsIgnoreCase("ResaleMLS"))) {
                x.setStreetAddress(x.getStreetName());
            } else
                x.setStreetNumber(streetName.getStreetNumber(x.getStreetAddress()));
            if ((x.getLivingAreaSquareFeet() != null) && (x.getAmount() != null)) {
                x.setPricePerSqft(x.getAmount() / x.getLivingAreaSquareFeet());
            }
        });
        convertValue = updateImageHexaValue(convertValue);
        return convertValue;
    }

    /**
     * @param propertyId
     * @param cityName
     * @param state
     * @param zip
     * @param saletype
     * @param cost
     * @return list of TBLPropertyModel
     * @code this method is intended to fetch search results for PDP by Zip and Cost
     */
    @Cacheable("fetchSearchResultsForPDPByZipAndCost")
    public List<TblePropertyModelSRPFilter> fetchSearchResultsForPDPByZipAndCost(Integer propertyId, String cityName, String state, String zip, String saletype, Integer cost) {
        log.info("Fetching search results for PDP by zip and cost");
        List<String> saleTypesList = getSaleTypesList(saletype);
        List<TblePropertyEntitySRPFilter> fetchSearchResultsForPDPVAFC;
        Integer upperLimitCost = cost + 100000;
        Integer lowerLimitCost = cost - 100000;

        fetchSearchResultsForPDPVAFC = tblePropertyEntitySRPFilterRepository
                .fetchSearchResultsForPDPByZipAndCost(propertyId, saleTypesList, cityName, state, zip, upperLimitCost, lowerLimitCost);
        ObjectMapper objMap = new ObjectMapper();
        List<TblePropertyModelSRPFilter> convertValue = objMap.convertValue(fetchSearchResultsForPDPVAFC, new TypeReference<List<TblePropertyModelSRPFilter>>() {
        });
        convertValue.forEach(x -> {
            if ((x.getStreetName() == null)) {
                x.setStreetName(streetName.getStreet(x.getStreetAddress()));
            }
            if (!(x.getSaleType().equalsIgnoreCase("FSBO")) && !(x.getSaleType().equalsIgnoreCase("ResaleMLS"))) {
                x.setStreetAddress(x.getStreetName());
            } else
                x.setStreetNumber(streetName.getStreetNumber(x.getStreetAddress()));
            if ((x.getLivingAreaSquareFeet() != null) && (x.getAmount() != null)) {
                x.setPricePerSqft(x.getAmount() / x.getLivingAreaSquareFeet());
            }


            if(x.getPostal_code().length()==4)
                x.setPostal_code("0"+x.getPostal_code());
        });
        convertValue = imageUtils.updateImageHexaValue(convertValue);
        return convertValue;
    }

    /**
     * @param propertyId
     * @param cityName
     * @param state
     * @param county
     * @param saletype
     * @return
     * @code this method is intended to fetch search results for PDP county
     */
    @Cacheable("fetchSearchResultsForPDPByCounty")
    public List<TblePropertyModelSRPFilter> fetchSearchResultsForPDPByCounty(String propertyId, String cityName, String state, String county, String saletype) {
        log.info("Fetching search results for PDP by county ");
        List<String> saleTypesList = getSaleTypesList(saletype);
        List<TblePropertyEntitySRPFilter> fetchSearchResultsForPDPVAFC;

        fetchSearchResultsForPDPVAFC = tblePropertyEntitySRPFilterRepository.fetchSearchResultsForPDPByCounty(propertyId, saleTypesList, cityName, state, county);
        ObjectMapper objMap = new ObjectMapper();
        List<TblePropertyModelSRPFilter> convertValue = objMap.convertValue(fetchSearchResultsForPDPVAFC, new TypeReference<List<TblePropertyModelSRPFilter>>() {
        });

        convertValue.forEach(x -> {
            if ((x.getStreetName() == null)) {
                x.setStreetName(streetName.getStreet(x.getStreetAddress()));
            }
            if (!(x.getSaleType().equalsIgnoreCase("FSBO")) && !(x.getSaleType().equalsIgnoreCase("ResaleMLS"))) {
                x.setStreetAddress(x.getStreetName());
            } else
                x.setStreetNumber(streetName.getStreetNumber(x.getStreetAddress()));
            if ((x.getLivingAreaSquareFeet() != null) && (x.getAmount() != null)) {
                x.setPricePerSqft(x.getAmount() / x.getLivingAreaSquareFeet());
            }

            if(x.getPostal_code().length()==4)
                x.setPostal_code("0"+x.getPostal_code());
        });
        convertValue = imageUtils.updateImageHexaValue(convertValue);
        return convertValue;
    }

    /**
     * @param cityName
     * @param state
     * @param county
     * @param saletype
     * @param cost
     * @return
     * @code this code is intended to fetch search results for PDP by cost
     */
    @Cacheable("fetchSearchResultsForPDPByCost")
    public List<TBLPropertyModel> fetchSearchResultsForPDPByCost(String cityName, String state, String county, String saletype, Integer cost) {
        log.info("Fetching search results for PDP by cost ");
        List<String> saleTypesList = getSaleTypesList(saletype);
        List<TBLPropertyEntity> fetchSearchResultsForPDPVAFC;

        fetchSearchResultsForPDPVAFC = repository
                .fetchSearchResultsForPDPByCost(saleTypesList, cityName, state, county, cost);
        ObjectMapper objMap = new ObjectMapper();
        List<TBLPropertyModel> convertValue = objMap.convertValue(fetchSearchResultsForPDPVAFC, new TypeReference<List<TBLPropertyModel>>() {
        });

        convertValue.forEach(x -> {
            if ((x.getStreetName() == null)) {
                x.setStreetName(streetName.getStreet(x.getStreetAddress()));
            }
            if (!(x.getSaleType().equalsIgnoreCase("FSBO")) && !(x.getSaleType().equalsIgnoreCase("ResaleMLS"))) {
                x.setStreetAddress(x.getStreetName());
            } else
                x.setStreetNumber(streetName.getStreetNumber(x.getStreetAddress()));
            if ((x.getLivingAreaSquareFeet() != null) && (x.getAmount() != null)) {
                x.setPricePerSqft(x.getAmount() / x.getLivingAreaSquareFeet());
            }
        });
        convertValue = updateImageHexaValue(convertValue);
        return convertValue;
    }

    /**
     * @param propId
     * @return
     * @code this method is intended to fetch search results for PDP by propertyId
     */
    @Cacheable("fetchSearchResultsForPDPByPropertyId")
    public List<MiniSRPPropertyModel> fetchSearchResultsForPDPByPropertyId(String propId) {
        log.info("Fetching search results for PDP by PropertyId ");
        propId=propId.trim();
        List<MiniSRPPropertyModel> convertValue = new ArrayList<>();


        if((!pidIsInteger(propId))||propId.length()>10) {
            MiniSRPPropertyModel miniSRPPropertyModel = new MiniSRPPropertyModel();
            miniSRPPropertyModel.setPropertyIdValid(false);
            convertValue.add(miniSRPPropertyModel);
            return convertValue;
        }
        Long pid= Long.valueOf(propId);
        if (!tblePropertyEntitySRPFilterRepository.existsById(pid)) {
            MiniSRPPropertyModel miniSRPPropertyModel = new MiniSRPPropertyModel();
            miniSRPPropertyModel.setPropertyIdValid(false);
            convertValue.add(miniSRPPropertyModel);
            return convertValue;
        }
        Integer propertyId=Integer.valueOf(propId);
        Date date;
        SimpleDateFormat dataFormatter = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat yearFormatter = new SimpleDateFormat("yyyy");
        List<TblePropertyEntitySRPFilter> fetchSearchResults = new ArrayList<>();

        List<TblePropertyEntitySRPFilter> fetchSearchResultsForPDP = tblePropertyEntitySRPFilterRepository
                .fetchSearchResultsForPDPPropertyId(propertyId);
        String saleType = pdpDetailRepository.getSaleTypebyId(propertyId);
        String saleGroupType = propertyTaxonomyRepository.getSaleGroupTypeById(saleType);

        fetchSearchResults.addAll(fetchSearchResultsForPDP);
        ObjectMapper objMap = new ObjectMapper();
        convertValue = objMap.convertValue(fetchSearchResults, new TypeReference<List<MiniSRPPropertyModel>>() {
        });
        convertValue = imageUtils.updateImageHexaValuePDP(convertValue);
        convertValue.forEach(x -> {
            x.setAmountDefinition(repository.getAmountDefinition(propertyId));
            if ((x.getStreetName() == null)) {
                x.setStreetName(streetName.getStreet(x.getStreetAddress()));
            }
            if (!(x.getSaleType().equalsIgnoreCase("FSBO")) && !(x.getSaleType().equalsIgnoreCase("ResaleMLS"))) {
                x.setStreetAddress(x.getStreetName());
            } else
                x.setStreetNumber(streetName.getStreetNumber(x.getStreetAddress()));
            if ((x.getLivingAreaSquareFeet() != null) && (x.getAmount() != null)) {
                x.setPricePerSqft(x.getAmount() / x.getLivingAreaSquareFeet());
            }

            if(x.getPostal_code().length()==4)
                x.setPostal_code("0"+x.getPostal_code());
            x.setPropertyIdValid(true);
        });

        for (MiniSRPPropertyModel entity : convertValue) {
            try {
                if (entity.getYearBuilt() != null) {
                    date = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy").parse(entity.getYearBuilt().toString());
                    String formatedDate = dataFormatter.format(date);
                    String formatedYear = yearFormatter.format(date);
                    entity.setYearBuiltDate(formatedDate);
                    entity.setYear(formatedYear);
                }
            } catch (ParseException e) {
                log.error("Exception occurred: " + e.getLocalizedMessage());
            }
        }
        return convertValue;
    }

    private boolean pidIsInteger(String str) {


        String regex = "[0-9]+";

        Pattern p = Pattern.compile(regex);
        if (str == null) {
            return false;
        }
        Matcher m = p.matcher(str);
        return m.matches();
    }
    /**
     * @param entityList
     * @return list of TBLPropertyModel
     * @code this code is intended to update Image Hexa value
     */
    public List<TBLPropertyModel> updateImageHexaValue(List<TBLPropertyModel> entityList) {
        List<TBLPropertyModel> newList = new ArrayList<>();
        for (TBLPropertyModel entity : entityList) {
            List<TBLImageModel> imageEntity = entity.getImageEntity();
            List<TBLImageModel> newImageList = new ArrayList<>();

            for (TBLImageModel image : imageEntity) {
                if(entity.getPrimaryImage().equals(image.getUrl()))
                    image.setPrimary(true);
                String imageUrl = imageUtils.getImageUrl(image, ImageSizeEnum.medium2, false, "");
                String imageUrlLarge = imageUtils.getImageUrl(image, ImageSizeEnum.original, false, "");
                String imageUrlHex = imageUtils.getImageUrlHex(image);
                image.setUrl(imageUrl);
                image.setUrlLarge(imageUrlLarge);
                image.setUrlHex(imageUrlHex);

                newImageList.add(image);
            }
            entity.setImageEntity(newImageList);
            newList.add(entity);
        }
        return newList;
    }

    /**
     * @param smartZipId
     * @return
     */
    @Cacheable("findEstimatedValueHistory")
    public List<ValueHistoryEntity> findEstimatedValueHistory(String smartZipId) {
        List<ValueHistoryEntity> fetchValueHistory = new ArrayList<>();
        fetchValueHistory = historyRepository.findEstimatedValueHistory(smartZipId);
        return fetchValueHistory;
    }

    /**
     * @param propertyId
     * @return
     */
    @Cacheable("findAssociatesForPDP")
    public List<TBLPropertyAssociatesEntity> findAssociatesForPDP(Integer propertyId) {
        log.info("Finding associates for PDP ");
        List<TBLPropertyAssociatesEntity> fetchAssociates = new ArrayList<>();
        String saleType = pdpDetailRepository.getSaleTypebyId(propertyId);
        String saleGroupType = propertyTaxonomyRepository.getSaleGroupTypeById(saleType);
        if (saleGroupType != null) {
            if (saleGroupType.equals("FSBO") || saleGroupType.equals("ResaleMLS")) {
                fetchAssociates = pdpAssociateRepository.findAssociatesByPropertyId(propertyId);
            }
        }
        return fetchAssociates;
    }

    /**
     * @param propertyId
     * @return
     * @throws Exception
     */
    @Cacheable("getPdp")
    public Object getPdp(Integer propertyId) {
        messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages/propertyText");

        String saleType = pdpDetailRepository.getSaleTypebyId(propertyId);
        String saleGroupType = propertyTaxonomyRepository.getSaleGroupTypeById(saleType);
        String description;
        if (saleGroupType != null) {
            if (saleGroupType.equals("FSBO") || saleGroupType.equals("ResaleMLS") || saleGroupType.equals("Rental")) {
                FsboOrMlsProperty fsboOrMlsProperty = fsboOrMlsPropertyRepository.getPd(propertyId);
                if (fsboOrMlsProperty.getDescription() == null) {
                    description = messageSource.getMessage("property.saletype." + fsboOrMlsProperty.getSaleType(), null, Locale.US);
                    fsboOrMlsProperty.setDescription(description);
                }
                if (fsboOrMlsProperty.getStreetName() == null) {
                    fsboOrMlsProperty.setStreetName(streetName.getStreet(fsboOrMlsProperty.getStreetAddress()));
                }
                fsboOrMlsProperty.setStreetNumber(streetName.getStreetNumber(fsboOrMlsProperty.getStreetAddress()));
                return fsboOrMlsProperty;
            } else {
                PropertyDetails pd = pdpDetailRepository.getPdById(propertyId);
                if (pd.getDescription() == null) {
                    description = messageSource.getMessage("property.saletype." + pd.getSaleType(), null, Locale.US);
                    pd.setDescription(description);
                }
                if (pd.getStreetName() == null) {
                    pd.setStreetName(streetName.getStreet(pdpDetailRepository.getStreetAddressbyPid(propertyId)));
                }
                pd = pd.setValuesToNothing();
                return pd;
            }
        } else {
            return null;
        }
    }

    /**
     * @param id
     * @return
     * @code this code is intended to update image Hexa value.
     */
    public List<TBLImageModel> updateImageHexaValue(Integer id) {
        List<TBLImageModel> newImageList = new ArrayList<>();
        List<TBLImageEntity> imageEntity = imageRepository.getImages(id);
        ObjectMapper objMap = new ObjectMapper();
        List<TBLImageModel> convertValue = objMap.convertValue(imageEntity, new TypeReference<List<TBLImageModel>>() {
        });
        for (TBLImageModel image : convertValue) {
            String imageUrl = imageUtils.getImageUrl(image, ImageSizeEnum.medium2, false, "");
            //adding image hex as per UI js image encryption
            String imageUrlHex = imageUtils.getImageUrlHex(image);
            image.setUrl(imageUrl);
            image.setUrlHex(imageUrlHex);
            newImageList.add(image);
        }
        return newImageList;
    }

    /**
     * @param saleType
     * @return
     */
    public List<String> getSaleTypesList(String saleType) {
        List<String> saleTyepList = new ArrayList<String>();
        if (saleType.equalsIgnoreCase("ForeclosuresRS2") || saleType.equalsIgnoreCase("PreForeclosures") || saleType.equalsIgnoreCase("Auctions")) {
            saleTyepList = srpRepository.fetchForeclosureTypes(saleType);
        } else {
            saleTyepList.add(saleType);
        }
        return saleTyepList;
    }
}
