package com.nic.properties.sevice;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nic.properties.constants.SRPSearchConstants;
import com.nic.properties.entity.TblePropertyEntitySRPFilter;
import com.nic.properties.model.*;
import com.nic.properties.repository.CountyRepository;
import com.nic.properties.repository.SRPRepository;
import com.nic.properties.repository.TBLPropertyTaxonomyRepository;
import com.nic.properties.repository.ZipRepository;
import com.nic.properties.srpenum.SRPEnum;
import com.nic.properties.srpenum.SortEnum;
import com.nic.properties.util.CountyCountAsyncSort;
import com.nic.properties.util.ImageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;


@Service
public class SRPFilterServiceWoImages {

	private static final String SELECT_QUERY = "SELECT  DISTINCT P1 FROM TblePropertyEntitySRPFilter P1 where ";
	private static List<String> saleTypeList;
	private static List<String> saleTypeListToUpdate;
	private final int PAGE_SIZE = 28;
	private final Logger log = LoggerFactory.getLogger(SRPFilterService.class);
	@PersistenceContext
	private EntityManager entityManager;
	@Autowired
	private SRPRepository repository;
	@Autowired
	private ImageUtils imageUtils;
	@Autowired
	private TBLPropertyTaxonomyRepository propertyTaxonomyRepository;
	@Autowired
	private StreetName streetName;
	@Value("${realtyStore.product.qualityScore}")
	private String qualityScore;
	@Autowired
	private CountyRepository countyRepository;
	@Autowired
	private ZipRepository zipRepository;

	/**
	 * @code to generate the Site-map URLs for geographic locations(states,
	 *       counties, cities, ZipCodes) from sale-type perspective. @param saleType
	 *       specifies the saleType
	 * @return map that holds key as a String and List<String> as a values
	 */
	/**
	 * @param filterRequest
	 * @param city
	 * @param state
	 * @param county
	 * @param postal_code
	 * @param streetAddress
	 * @return
	 * @code to filter the request based on search parameters
	 */
	@Cacheable("fetchDataByFilteringWoImages")
	public SrpFilterResponseWoImages fetchDataByFilteringWoImages(SrpFilterRequest filterRequest ,
																  String city,
																  String state,
																  String county,
																  String postal_code,
																  String streetAddress) {
		double startTime = System.currentTimeMillis();
		log.info("Started Search Filter.");
		Integer fips = null;
		SrpFilterResponseWoImages srpFilterResponse = new SrpFilterResponseWoImages();
		Integer zipCode = null;
		if(postal_code!=null)
			zipCode = Integer.parseInt(postal_code);
		if(county!=null && county.equalsIgnoreCase("miami dade")){
			county = "miami-dade";
		}
		//String validCounty = repository.getProperCountyName(county);

		if(county!=null)
		{
			fips = repository.getFipsbyStateAndCounty(county, state);
			if(fips==null)
			{srpFilterResponse.setLocationValid(false);
				return srpFilterResponse;}
		}

		//Spelling validation for URL
		if ((postal_code != null && zipRepository.containsZip(zipCode) == 0)
				|| (city != null && zipRepository.containsCity(city) == 0)) {

			srpFilterResponse.setLocationValid(false);
			return srpFilterResponse;
		}
		//city county zipcode mismatch check for URL
		if(postal_code!=null)
		{
			if( city != null && !((zipRepository.getcityList(zipCode)).stream().anyMatch(city::equalsIgnoreCase))
					||(county != null && !(countyRepository.getFipsByCity(city).contains(fips)))
					|| (state != null && !(countyRepository.getStateByFips(fips).stream().anyMatch(state::equalsIgnoreCase))) )
			{
				srpFilterResponse.setLocationValid(false);
				return srpFilterResponse;
			}
		}
		else if(city!=null)
		{
			if((county != null &&( (!(countyRepository.getCountiesByCity(city).stream().anyMatch(county::equalsIgnoreCase)))
					&& (!(zipRepository.getCityByFips(fips).stream().anyMatch(city::equalsIgnoreCase)))))
					/*|| (state != null && !(countyRepository.getStateByCounty(county).stream().anyMatch(state::equalsIgnoreCase))) */)
			{
				srpFilterResponse.setLocationValid(false);
				return srpFilterResponse;
			}
		}/*else if( county!=null)
		{
			if(state!=null&& !(countyRepository.getStateByCounty(county).stream().anyMatch(state::equalsIgnoreCase)))
			{
				srpFilterResponse.setLocationValid(false);
				return srpFilterResponse;
			}
		}*/

		List<NextProperties> nextProperties = new ArrayList<>();
		saleTypeList = new ArrayList<>();

		saleTypeListToUpdate = new ArrayList<>();

		try {

			if (filterRequest.getImage() == null) {
				filterRequest.setImage(false);
			}


			StringBuilder conditionalQuery = new StringBuilder();
			if (!CollectionUtils.isEmpty(filterRequest.getSaleTypes())) {
				List<String> saleTypes = new ArrayList<>();
				saleTypeList.addAll(filterRequest.getSaleTypes());

				saleTypeListToUpdate.addAll(filterRequest.getSaleTypes());

				for (String saleType : filterRequest.getSaleTypes()) {
					if (saleType.equalsIgnoreCase("ForeclosuresRS2") || saleType.equalsIgnoreCase("PreForeclosures")
							|| saleType.equalsIgnoreCase("Auctions") || saleType.equalsIgnoreCase("HUDHomesForeclosureRS2")
							|| saleType.equalsIgnoreCase("TaxSales") || saleType.equalsIgnoreCase("HUDOrGovtForeclosures")
							|| saleType.equalsIgnoreCase("BankForeclosures") || saleType.equalsIgnoreCase("ResaleMLS")
							|| saleType.equalsIgnoreCase("RentToOwn") || saleType.equalsIgnoreCase("FSBO")
							|| saleType.equalsIgnoreCase("Rental") || saleType.equalsIgnoreCase("OwnerFinancing")
							|| saleType.equalsIgnoreCase("ShortSale")) {
						saleTypes.addAll(repository.fetchForeclosureTypes(saleType));
					} else {
						saleTypes.add(saleType);
					}
				}
				filterRequest.setSaleTypes(saleTypes);
			}
			Long count;
			conditionalQuery = buildSearchCriteriaForFilter(filterRequest, conditionalQuery);
			conditionalQuery = buildSearchCriteriaForMandatoryParams(city, state, county, streetAddress, postal_code,
					conditionalQuery);
			String finalQuery = SELECT_QUERY + conditionalQuery.toString();
			if (finalQuery.endsWith("where")) {
				finalQuery = finalQuery.replace("where", "");
			}
			//Sorting
			if(filterRequest.getSortBy()!=null&&(!filterRequest.getSortBy().equalsIgnoreCase("BESTMATCH")))
			{
				finalQuery=finalQuery.concat("ORDER BY P1.").concat((SortEnum.valueOf(filterRequest.getSortBy().toUpperCase()).value));/*.concat(" desc")*/
			}
			else
				finalQuery = finalQuery.concat(" ORDER BY P1.").concat(qualityScore).concat(" desc");

			count = performDBActionForCount(filterRequest, city, state, county, streetAddress, postal_code, finalQuery);
			log.info("Fetching property list.");
			List<TblePropertyEntitySRPFilter> resultList = performDBAction(filterRequest, city, state, county, streetAddress,
					postal_code, finalQuery);

			srpFilterResponse.setCount(count);
			srpFilterResponse.setPropertyList(convertEntityList(resultList));
			if ((filterRequest.getHomeTypes() == null && filterRequest.getSpecialOption() == null
					&& filterRequest.getSpecialOption() == null && filterRequest.getAmount() == null
					&& filterRequest.getMaxAmount() == null && filterRequest.getBedRooms() == null
					&& filterRequest.getBathsTotal() == null && filterRequest.getLivingSquareFeet() == null
					&& filterRequest.getMaxLivingSquareFeet() == null && filterRequest.getLotSize() == null
					&& filterRequest.getMaxLotSize() == null && filterRequest.getQualityScore() == null
					&& filterRequest.getYear() == null && filterRequest.getMaxYear() == null
					&& filterRequest.getImage() == false && ((filterRequest.getPageIndex() == null) || (filterRequest.getPageIndex() == 1)))
					&& (CollectionUtils.isEmpty(resultList) || resultList.size() <= 4)) {
				{
					// for sub sale types

					finalQuery = finalQuery.replaceAll(
							"(P1.city) = :city",
							" (P1.city) IN ( select LOWER(t3.city) from TBLNearestCitiesEntity t3 where (t3.searchCity) = :city AND (t3.searchState)=:state and proximity<=20 order by P1.propertyId) "
					);
					finalQuery.concat(" ORDER BY P1.").concat(qualityScore).concat(" desc");
					log.info("Fetching property list within 20 miles of radius.");
					count = performDBActionForCount(filterRequest, city, state, county, streetAddress, postal_code,
							finalQuery);
					resultList = performDBAction(filterRequest, city, state, county, streetAddress, postal_code,
							finalQuery);
					List<TblePropertyModelSRPFilter> convertEntityList = convertEntityList(resultList);
					if (!CollectionUtils.isEmpty(convertEntityList)
							&& !CollectionUtils.isEmpty(srpFilterResponse.getPropertyList()))
						convertEntityList.removeAll(srpFilterResponse.getPropertyList());
					if(convertEntityList!=null)
					srpFilterResponse.setMilesPropertyList(convertEntityList);
					else
						srpFilterResponse.setMilesPropertyList(new ArrayList<>());
					if (count < 28) {
						srpFilterResponse.setMilesCount(Long
								.valueOf(CollectionUtils.isEmpty(convertEntityList) ? 0 : convertEntityList.size()));
					} else {
						srpFilterResponse.setMilesCount(count);
					}
					if (CollectionUtils.isEmpty(resultList) || resultList.size() <= 4) {

						finalQuery = finalQuery.replaceAll(
								"(P1.city) IN ( select (t3.city) from TBLNearestCitiesEntity t3 where LOWER(t3.searchCity) = :city AND (t3.searchState)=:state and proximity<=20)",
								" (P1.city) = :city ");
						filterRequest.setSaleTypes(getappropriateSaleTypes(filterRequest));
						if (!CollectionUtils.isEmpty(filterRequest.getSaleTypes())) {
							List<String> saleTypes = new ArrayList<>();
							saleTypeList.addAll(filterRequest.getSaleTypes());
							for (String types : filterRequest.getSaleTypes()) {
								if (types.equalsIgnoreCase("ForeclosuresRS2")
										|| types.equalsIgnoreCase("PreForeclosures")
										|| types.equalsIgnoreCase("Auctions")
										|| types.equalsIgnoreCase("HUDHomesForeclosureRS2")
										|| types.equalsIgnoreCase("TaxSales")
										|| types.equalsIgnoreCase("HUDOrGovtForeclosures")
										|| types.equalsIgnoreCase("BankForeclosures")
										|| types.equalsIgnoreCase("ResaleMLS") || types.equalsIgnoreCase("RentToOwn")
										|| types.equalsIgnoreCase("FSBO") || types.equalsIgnoreCase("Rental")
										|| types.equalsIgnoreCase("OwnerFinancing")
										|| types.equalsIgnoreCase("ShortSale")) {
									saleTypes.addAll(repository.fetchSaleGroupTypes(types));
									// saleTypes.addAll(repository.fetchForeclosureTypes(types));
								} else {
									saleTypes.add(types);
								}
							}
							filterRequest.setSaleTypes(saleTypes);
						}
						finalQuery = finalQuery.replace(
								"(P1.city) IN ( select LOWER(t3.city) from TBLNearestCitiesEntity t3 where (t3.searchCity) = :city AND (t3.searchState)=:state and proximity<=20 order by P1.propertyId)",
								"(P1.city) = :city"
						);
						finalQuery.concat(" ORDER BY P1.").concat(qualityScore).concat(" desc");
						log.info("Fetching property list for sub sale types.");
						count = performDBActionForCount(filterRequest, city, state, county, streetAddress, postal_code,
								finalQuery);
						List<TblePropertyEntitySRPFilter> performDBAction = performDBAction(filterRequest, city, state, county,
								streetAddress, postal_code, finalQuery);
						if (performDBAction.size() + resultList.size() > 28) {
							resultList.addAll(performDBAction.subList(0, performDBAction.size() - resultList.size()));
						} else {
							resultList.addAll(performDBAction);
						}

						List<TblePropertyModelSRPFilter> convertEntityListForSub = convertEntityList(resultList);
						if (!CollectionUtils.isEmpty(convertEntityListForSub)) {
							if (!CollectionUtils.isEmpty(srpFilterResponse.getMilesPropertyList()))
								convertEntityListForSub.removeAll(srpFilterResponse.getMilesPropertyList());
							if (!CollectionUtils.isEmpty(srpFilterResponse.getPropertyList()))
								convertEntityListForSub.removeAll(srpFilterResponse.getPropertyList());
						}

						srpFilterResponse.setSubtypesPropertyList(convertEntityListForSub);

						if (count < 28) {
							srpFilterResponse
									.setSubTypesCount(Long.valueOf(CollectionUtils.isEmpty(convertEntityListForSub) ? 0
											: convertEntityListForSub.size()));
						} else {
							srpFilterResponse.setSubTypesCount(count);
						}

					}
				}
				if (srpFilterResponse.getPropertyList() != null && srpFilterResponse.getPropertyList().size() > 4) {
					srpFilterResponse.setMilesPropertyList(null);
					srpFilterResponse.setMilesCount(null);
				}
				if (srpFilterResponse.getMilesPropertyList() != null
						&& srpFilterResponse.getMilesPropertyList().size() >= 1) {
					srpFilterResponse.setSubtypesPropertyList(null);
					srpFilterResponse.setSubTypesCount(null);
				}

			}
		} catch (Exception e) {
			log.error("An exception has occurred in SRPFilterService.fetchDataByFiltering. " + e.getLocalizedMessage());
			e.printStackTrace();
			//srpFilterResponse.setErrorMessage(e.getMessage());
			srpFilterResponse.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			srpFilterResponse.setCount(null);
			srpFilterResponse.setPropertyList(new ArrayList<>());
			srpFilterResponse.setSubtypesPropertyList(new ArrayList<>());
			srpFilterResponse.setMilesPropertyList(new ArrayList<>());

		}
		if (srpFilterResponse.getMilesPropertyList() != null) {
			srpFilterResponse.getMilesPropertyList().forEach(x -> {
				if ((x.getStreetName() == null)) {
					x.setStreetName(streetName.getStreet(x.getStreetAddress()));
				}
				if (!(x.getSaleType().equalsIgnoreCase("FSBO")) && !(x.getSaleType().equalsIgnoreCase("ResaleMLS"))) {
					x.setStreetAddress(x.getStreetName());
				} else
					x.setStreetNumber(streetName.getStreetNumber(x.getStreetAddress()));
				if ((x.getLivingAreaSquareFeet() != null) && (x.getAmount() != null)) {
					x.setPricePerSqft(x.getAmount() / x.getLivingAreaSquareFeet());
				}
				if (x.getSaleType().equals("ownerfinancing")) {
					x = null;
				}

				if(x.getPostal_code().length()==4)
					x.setPostal_code("0"+x.getPostal_code());
				nextProperties.add(new NextProperties(x.getPropertyId(), x.getStreetAddress(), x.getCity(), x.getCounty(), x.getStateOrProvince(), x.getPostal_code()));


			});
			srpFilterResponse.setNextPropertyIDs(nextProperties);
		}
		if (srpFilterResponse.getPropertyList() != null) {
			srpFilterResponse.getPropertyList().forEach(x -> {
				if ((x.getStreetName() == null)) {
					x.setStreetName(streetName.getStreet(x.getStreetAddress()));
				}
				if (!(x.getSaleType().equalsIgnoreCase("FSBO")) && !(x.getSaleType().equalsIgnoreCase("ResaleMLS"))) {
					x.setStreetAddress(x.getStreetName());
				} else
					x.setStreetNumber(streetName.getStreetNumber(x.getStreetAddress()));
				if ((x.getLivingAreaSquareFeet() != null) && (x.getAmount() != null)) {
					x.setPricePerSqft(x.getAmount() / x.getLivingAreaSquareFeet());
				}

				if(x.getPostal_code().length()==4)
					x.setPostal_code("0"+x.getPostal_code());
				nextProperties.add(new NextProperties(x.getPropertyId(), x.getStreetAddress(), x.getCity(), x.getCounty(), x.getStateOrProvince(), x.getPostal_code()));


			});
			srpFilterResponse.setNextPropertyIDs(nextProperties);
		}
		if (srpFilterResponse.getSubtypesPropertyList() != null) {
			srpFilterResponse.getSubtypesPropertyList().forEach(x -> {
				if ((x.getStreetName() == null)) {
					x.setStreetName(streetName.getStreet(x.getStreetAddress()));
				}
				if (!(x.getSaleType().equalsIgnoreCase("FSBO")) && !(x.getSaleType().equalsIgnoreCase("ResaleMLS"))) {
					x.setStreetAddress(x.getStreetName());
				} else
					x.setStreetNumber(streetName.getStreetNumber(x.getStreetAddress()));
				if ((x.getLivingAreaSquareFeet() != null) && (x.getAmount() != null)) {
					x.setPricePerSqft(x.getAmount() / x.getLivingAreaSquareFeet());
				}

				if(x.getPostal_code().length()==4)
					x.setPostal_code("0"+x.getPostal_code());
				nextProperties.add(new NextProperties(x.getPropertyId(), x.getStreetAddress(), x.getCity(), x.getCounty(), x.getStateOrProvince(), x.getPostal_code()));


			});
			srpFilterResponse.setNextPropertyIDs(nextProperties);
		}

		addHexaAndForeClosureTypes(srpFilterResponse);
		//srpFilterResponse.setHttpStatus(HttpStatus.OK);
		srpFilterResponse.setLocationValid(true);

		List<TblePropertyModelSRPFilter> propertyList = srpFilterResponse.getPropertyList();
		List<TblePropertyModelSRPFilter> milesPropertyList = srpFilterResponse.getMilesPropertyList();

		if (milesPropertyList != null) {
			if (propertyList != null) {
				int listSize = propertyList.size() + milesPropertyList.size();
				if (listSize > 28) {
					int index = 28 - propertyList.size();
					srpFilterResponse.setMilesPropertyList(milesPropertyList.subList(0, index));
					srpFilterResponse.setMilesCount(Long.valueOf(srpFilterResponse.getMilesPropertyList().size()));
				}
			} else if (milesPropertyList.size() > 29) {
				srpFilterResponse.setMilesPropertyList(milesPropertyList.subList(0, 28));
				srpFilterResponse.setMilesCount(Long.valueOf(srpFilterResponse.getMilesPropertyList().size()));
			}
		}
		double endTime = System.currentTimeMillis();
		log.info("Time Taken By SRP Filter Service For Fetching The Data Is : " + (((endTime - startTime) / 1000.0) % 60.0));
		return srpFilterResponse;
	}

	private void addHexaAndForeClosureTypes(SrpFilterResponseWoImages response) {
		if (!CollectionUtils.isEmpty(response.getPropertyList())) {
			//  if (saleTyepList.contains("ForeclosuresRS2"))
			if (saleTypeListToUpdate.stream().anyMatch("ForeclosuresRS2"::equalsIgnoreCase)) {
				response.setPropertyList(updateSaleType(response.getPropertyList(), SRPEnum.ForeclosuresRS2.name()));
			} else if (saleTypeListToUpdate.stream().anyMatch("PreForeclosures"::equalsIgnoreCase)) {
				response.setPropertyList(updateSaleType(response.getPropertyList(), SRPEnum.PreForeclosures.name()));
			} else if (saleTypeListToUpdate.stream().anyMatch("BankForeclosures"::equalsIgnoreCase)) {
				response.setPropertyList(updateSaleType(response.getPropertyList(), "BankForeclosures"));
			} else if (saleTypeListToUpdate.stream().anyMatch("Auctions"::equalsIgnoreCase)) {
				response.setPropertyList(updateSaleType(response.getPropertyList(), "Auctions"));
			} else if (saleTypeListToUpdate.stream().anyMatch("ShortSale"::equalsIgnoreCase)) {
				response.setPropertyList(updateSaleType(response.getPropertyList(), "ShortSale"));
			} else if (saleTypeListToUpdate.stream().anyMatch("HUDHomesForeclosureRS2"::equalsIgnoreCase)) {
				response.setPropertyList(updateSaleType(response.getPropertyList(), "HUDHomesForeclosureRS2"));
			} else if (saleTypeListToUpdate.stream().anyMatch("ResaleMLS"::equalsIgnoreCase)) {
				response.setPropertyList(updateSaleType(response.getPropertyList(), "ResaleMLS"));
			} else if (saleTypeListToUpdate.stream().anyMatch("FSBO"::equalsIgnoreCase)) {
				response.setPropertyList(updateSaleType(response.getPropertyList(), "FSBO"));
			} else if (saleTypeListToUpdate.stream().anyMatch("RentToOwn"::equalsIgnoreCase)) {
				response.setPropertyList(updateSaleType(response.getPropertyList(), "RentToOwn"));
			} else if (saleTypeListToUpdate.stream().anyMatch("Rental"::equalsIgnoreCase)) {

				response.setPropertyList(updateSaleType(response.getPropertyList(), "Rental"));
			}
			response.setPropertyList(updateImageHexaValue(response.getPropertyList()));
		}
		if (!CollectionUtils.isEmpty(response.getMilesPropertyList())) {
			if (saleTypeListToUpdate.stream().anyMatch("ForeclosuresRS2"::equalsIgnoreCase)) {
				response.setMilesPropertyList(
						updateSaleType(response.getMilesPropertyList(), SRPEnum.ForeclosuresRS2.name()));
			} else if (saleTypeListToUpdate.stream().anyMatch("PreForeclosures"::equalsIgnoreCase)) {
				response.setMilesPropertyList(
						updateSaleType(response.getMilesPropertyList(), SRPEnum.PreForeclosures.name()));
			} else if (saleTypeListToUpdate.stream().anyMatch("BankForeclosures"::equalsIgnoreCase)) {
				response.setMilesPropertyList(updateSaleType(response.getMilesPropertyList(), "BankForeclosures"));
			} else if (saleTypeListToUpdate.stream().anyMatch("Auctions"::equalsIgnoreCase)) {
				response.setMilesPropertyList(updateSaleType(response.getMilesPropertyList(), "Auctions"));
			} else if (saleTypeListToUpdate.stream().anyMatch("ShortSale"::equalsIgnoreCase)) {
				response.setMilesPropertyList(updateSaleType(response.getMilesPropertyList(), "ShortSale"));
			} else if (saleTypeListToUpdate.stream().anyMatch("HUDHomesForeclosureRS2"::equalsIgnoreCase)) {
				response.setMilesPropertyList(updateSaleType(response.getMilesPropertyList(), "HUDHomesForeclosureRS2"));
			} else if (saleTypeListToUpdate.stream().anyMatch("ResaleMLS"::equalsIgnoreCase)) {
				response.setMilesPropertyList(updateSaleType(response.getMilesPropertyList(), "ResaleMLS"));
			} else if (saleTypeListToUpdate.stream().anyMatch("FSBO"::equalsIgnoreCase)) {
				response.setMilesPropertyList(updateSaleType(response.getMilesPropertyList(), "FSBO"));
			} else if (saleTypeListToUpdate.stream().anyMatch("RentToOwn"::equalsIgnoreCase)) {
				response.setMilesPropertyList(updateSaleType(response.getMilesPropertyList(), "RentToOwn"));
			} else if (saleTypeListToUpdate.stream().anyMatch("Rental"::equalsIgnoreCase)) {
				response.setMilesPropertyList(updateSaleType(response.getMilesPropertyList(), "Rental"));
			}
			response.setMilesPropertyList(updateImageHexaValue(response.getMilesPropertyList()));
		}
		if (!CollectionUtils.isEmpty(response.getSubtypesPropertyList())) {
			Map<String, Integer> map = new HashMap<String, Integer>();
			Integer i = 0;
			List<TblePropertyModelSRPFilter> saleTypeMainList = new ArrayList<>();
			List<TblePropertyModelSRPFilter> saleTypeList = response.getSubtypesPropertyList();
			for (TblePropertyModelSRPFilter item : saleTypeList) {
				if (!map.containsKey(item.getSaleType())) {
					map.put(item.getSaleType(), 1);
				} else {
					map.put(item.getSaleType(), map.get(item.getSaleType()) + 1);
				}
				if (map.get(item.getSaleType()) <= 4) {
					saleTypeMainList.add(item);
				}
			}
			int count = (saleTypeMainList.size());
			response.setSubTypesCount((long) count);
			response.setSubtypesPropertyList(updateImageHexaValue(saleTypeMainList));
		}
	}

	/**
	 * @param entityList
	 * @return
	 */
	public List<TblePropertyModelSRPFilter> convertEntityList(List<TblePropertyEntitySRPFilter> entityList) {
		if (CollectionUtils.isEmpty(entityList)) {
			return null;
		}
		ObjectMapper objMap = new ObjectMapper();
		return objMap.convertValue(entityList, new TypeReference<List<TblePropertyModelSRPFilter>>() {
		});
	}

	/**
	 * @param entityList
	 * @return
	 */
	public List<TblePropertyModelSRPFilter> updateImageHexaValue(List<TblePropertyModelSRPFilter> entityList) {

		entityList.parallelStream().forEach(x -> {

			x.setUrlHex(imageUtils.getImageUrlHexForImage(x.getUrlHex()));
		});

		return entityList;
	}

	/**
	 * @param entityList
	 * @param saleType
	 * @return
	 */
	public List<TblePropertyModelSRPFilter> updateSaleType(List<TblePropertyModelSRPFilter> entityList, String saleType) {


		List<TblePropertyModelSRPFilter> newList = new ArrayList<>();
		if (saleTypeListToUpdate.size() > 1) {
			return entityList;
		} else {
			for (TblePropertyModelSRPFilter entity : entityList) {
				entity.setSaleType(saleType);
				newList.add(entity);
			}
		}
		return newList;


	}

	/**
	 * @param filterRequest
	 * @return
	 */
	public List<String> getappropriateSaleTypes(SrpFilterRequest filterRequest) {
		log.info("Fetching appropriate Sale types.");
		String saleType="";
		if(!filterRequest.getSaleTypes().isEmpty())
		 saleType = filterRequest.getSaleTypes().get(0);

		SRPEnum valueOf = SRPEnum.valueOfLabel(saleType);
		if(valueOf!=null) {
			switch (valueOf) {
				case SALE:
					return SRPSearchConstants.sale;
				case BANK_OWNED_REO:
					return SRPSearchConstants.bankOwnedREO;
				case HUD_HOMES:
					return SRPSearchConstants.hudHomes;
				case SHORT_SALES:
					return repository.fetchSaleGroupTypes(saleType);
				case PreForeclosures:
					return repository.fetchSaleGroupTypes(saleType);
				case BANKFORECLOSURES:
					return repository.fetchSaleGroupTypes(saleType);
				case HUDORGOVTFORECLOSURES:
					return repository.fetchSaleGroupTypes(saleType);
				case TAXSALES:
					return repository.fetchSaleGroupTypes(saleType);
				case HOUSE_AUCTIONS:
					return repository.fetchSaleGroupTypes(saleType);
				case FOR_SALE:
					return repository.fetchSaleGroupTypes(saleType);
				case RENT_TO_OWN:
					return repository.fetchSaleGroupTypes(saleType);
				case FSBO:
					return repository.fetchSaleGroupTypes(saleType);
				case HOUSE_FOR_RENT:
					return repository.fetchSaleGroupTypes(saleType);
				case OWNERFINANCING:
					return repository.fetchSaleGroupTypes(saleType);
				case ForeclosuresRS2:
					return repository.fetchSaleGroupTypes(saleType);
				case HUDHOMESFORECLOSURERS2:
					return repository.fetchSaleGroupTypes(saleType);
				case FANNIEMAEORFREDDIEMAC:
					return SRPSearchConstants.FannieMaeOrFreddieMac;
				case VAFORECLOSURE:
					return SRPSearchConstants.VAForeclosure;
				case TAXLEIN:
					return SRPSearchConstants.TaxLien;
				default:
					return filterRequest.getSaleTypes();
			}
		}
		else return new ArrayList<>();
	}


	/**
	 * @param filterRequest
	 * @param city
	 * @param state
	 * @param county
	 * @param streetAddress
	 * @param postal_code
	 * @param finalQuery
	 * @return
	 */

	private List<TblePropertyEntitySRPFilter> performDBAction(SrpFilterRequest filterRequest, String city, String state,
															  String county, String streetAddress, String postal_code, String finalQuery) {
		log.info("Creating query and performing DB operation");
		Query filterQuery = entityManager.createQuery(finalQuery);
		setInputParams(filterRequest, filterQuery, city, state, county, streetAddress, postal_code);
		if (filterRequest.getPageIndex() != null && filterRequest.getPageIndex() >= 1) {
			filterQuery.setFirstResult(calculatePageFirstResult((filterRequest.getPageIndex() == null) ? 0 : filterRequest.getPageIndex()));
		}
		filterQuery.setMaxResults(PAGE_SIZE);
		List<TblePropertyEntitySRPFilter> resultList = filterQuery.getResultList();
		return resultList;
	}

	/**
	 * @param filterRequest
	 * @param city
	 * @param state
	 * @param county
	 * @param streetAddress
	 * @param postal_code
	 * @param finalQuery
	 * @return
	 */
	private Long performDBActionForCount(SrpFilterRequest filterRequest, String city, String state, String county,
										 String streetAddress, String postal_code, String finalQuery) {
		log.info("Fetching the count of results.");
		finalQuery = finalQuery.replaceFirst("DISTINCT P1", "count(DISTINCT P1.propertyId)");
		Query createCountQuery = entityManager.createQuery(finalQuery);
		setInputParams(filterRequest, createCountQuery, city, state, county, streetAddress, postal_code);
		Long count = (Long) createCountQuery.getSingleResult();
		return count;
	}


	/**
	 * @param city
	 * @param state
	 * @param county
	 * @param streetAddress
	 * @param postal_code
	 * @param conditionalQuery
	 * @return
	 */
	private StringBuilder buildSearchCriteriaForMandatoryParams(String city, String state, String county,
																String streetAddress, String postal_code, StringBuilder conditionalQuery) {
		log.info("building search criteria for mandatory params");
		if (!StringUtils.isEmpty(city)) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.city = :city ");
		}
		if (!StringUtils.isEmpty(state)) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.stateOrProvince = :state ");
		}
		if (!StringUtils.isEmpty(county)) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.county = :county ");
		}
		if (!StringUtils.isEmpty(streetAddress)) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.streetAddress = :streetAddress ");
		}
		if (!StringUtils.isEmpty(postal_code)) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.postal_code = :postal_code ");
		}
		return conditionalQuery;
	}

	/**
	 * @param filterRequest
	 * @param conditionalQuery
	 * @return
	 */
	private StringBuilder buildSearchCriteriaForFilter(SrpFilterRequest filterRequest, StringBuilder conditionalQuery) {
		log.info("building search criteria for filter params");

		if (filterRequest.getImage() == true) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.urlHex is not null ");
		}
		if (!CollectionUtils.isEmpty(filterRequest.getSaleTypes())) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" (P1.saleType) in (:saleType) ");
		}

		if (!CollectionUtils.isEmpty(filterRequest.getSpecialOption())) {
			int i = 0;
			for (String splColumn : filterRequest.getSpecialOption()) {
				// when user selects multiple special Options and
				//	In response properties be having either of those special options or all or some
				if (splColumn.equalsIgnoreCase("fixerupper")) {
					splColumn = "fixerUpper";
				} else if (splColumn.equalsIgnoreCase("rtoFinancing")) {
					splColumn = "rtoFinancing";
				} else if (splColumn.equalsIgnoreCase("specialFinancing")) {
					splColumn = "specialFinancing";
				} else if (splColumn.equalsIgnoreCase("rtoPotential")) {
					splColumn = "rtoPotential";
				} else if (splColumn.equalsIgnoreCase("exclusive")) {
					splColumn = "exclusive";
				} else if (splColumn.equalsIgnoreCase("bargainPrice")) {
					splColumn = "bargainPrice";
				}
				if (i == 0) {
					conditionalQuery = addAndCondition(conditionalQuery).append("(");
					//conditionalQuery.append("(");
					i++;
				} else {
					conditionalQuery = addOrCondition(conditionalQuery);
				}
				conditionalQuery = conditionalQuery.append(" P1." + splColumn + " is true ");
			}
			conditionalQuery= conditionalQuery.append(")");
		}
		if (filterRequest.getQualityScore() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.qualityScore2 >= :qualityScore ");
		}
		if (!CollectionUtils.isEmpty(filterRequest.getHomeTypes())) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" (P1.propertyRecordType) in (:recordType) ");
		}

		if (filterRequest.getAmount() != null && filterRequest.getMaxAmount() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.amount between :minAmt and :maxAmt");
		} else if (filterRequest.getAmount() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.amount >= :amount ");
		} else if (filterRequest.getMaxAmount() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.amount <= :maxAmount ");
		}
		if (filterRequest.getBedRooms() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.bedrooms >= :bedrooms ");
		}
		if (filterRequest.getBathsTotal() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.bathsTotal >= :bathstotal ");
		}
		if (filterRequest.getLivingSquareFeet() != null && filterRequest.getMaxLivingSquareFeet() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.livingAreaSquareFeet between :minLivSqrFt and :maxLivsqrFt");
		} else if (filterRequest.getLivingSquareFeet() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.livingAreaSquareFeet >= :livingSqrft ");
		} else if (filterRequest.getMaxLivingSquareFeet() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.livingAreaSquareFeet <= :maxLivingSqrft ");
		}
		if (filterRequest.getLotSize() != null && filterRequest.getMaxLotSize() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.amount between :minLotSize and :maxLotSize");
		} else if (filterRequest.getLotSize() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.lotSize >= :lotSize ");
		} else if (filterRequest.getMaxLotSize() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.lotSize <= :maxLotSize ");
		}
		if (filterRequest.getSqftPrice() != null && filterRequest.getMaxSqftPrice() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.amount/P1.livingAreaSquareFeet between :minSqftPrice and :maxSqftPrice");
		} else if (filterRequest.getSqftPrice() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.amount/P1.livingAreaSquareFeet >= :minSqftPrice ");

		} else if (filterRequest.getMaxSqftPrice() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" P1.amount/P1.livingAreaSquareFeet <= :maxSqftPrice ");
		}

		if (filterRequest.getYear() != null && filterRequest.getMaxYear() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" SUBSTRING(P1.YearBuilt, 1, 4) between :minYear and :maxYear");
		} else if (filterRequest.getYear() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" SUBSTRING(P1.YearBuilt, 1, 4) >= :year ");
		} else if (filterRequest.getMaxYear() != null) {
			conditionalQuery = addAndCondition(conditionalQuery);
			conditionalQuery = conditionalQuery.append(" SUBSTRING(P1.YearBuilt, 1, 4) <= :maxYear ");
		}

		return conditionalQuery;
	}

	//Add or condition to conditional Query
	private StringBuilder addOrCondition(StringBuilder conditionalQuery) {
		if (!StringUtils.isEmpty(conditionalQuery.toString())) {
			conditionalQuery = conditionalQuery.append(" or ");
		}
		return conditionalQuery;
	}


	private int calculatePageFirstResult(int pageIndex) {
		if (pageIndex == 0) {
			pageIndex = 1;
		}
		return ((pageIndex - 1) * PAGE_SIZE);
	}

	/**
	 * @param filterRequest
	 * @param createNativeQuery
	 * @param city
	 * @param state
	 * @param county
	 * @param streetAddress
	 * @param postal_code
	 */
	public void setInputParams(SrpFilterRequest filterRequest, Query createNativeQuery, String city, String state,
							   String county, String streetAddress, String postal_code) {
		log.info("Setting input Params for the query");
		if (!CollectionUtils.isEmpty(filterRequest.getHomeTypes())) {
			createNativeQuery.setParameter("recordType",
					filterRequest.getHomeTypes().stream().map(i -> i.toLowerCase()).collect(Collectors.toList()));
		}
		if (!CollectionUtils.isEmpty(filterRequest.getSaleTypes())) {
			createNativeQuery.setParameter("saleType",
					filterRequest.getSaleTypes().stream().map(i -> i.toLowerCase()).collect(Collectors.toList()));
		}
		if (filterRequest.getAmount() != null && filterRequest.getMaxAmount() != null) {
			createNativeQuery.setParameter("minAmt", filterRequest.getAmount());
			createNativeQuery.setParameter("maxAmt", filterRequest.getMaxAmount());
		} else if (filterRequest.getAmount() != null) {
			createNativeQuery.setParameter("amount", filterRequest.getAmount());
		} else if (filterRequest.getMaxAmount() != null) {
			createNativeQuery.setParameter("maxAmount", filterRequest.getMaxAmount());
		}
		if (filterRequest.getBedRooms() != null) {
			createNativeQuery.setParameter("bedrooms", filterRequest.getBedRooms());
		}
		if (filterRequest.getBathsTotal() != null) {
			createNativeQuery.setParameter("bathstotal", filterRequest.getBathsTotal());
		}
		if (filterRequest.getLivingSquareFeet() != null && filterRequest.getMaxLivingSquareFeet() != null) {
			createNativeQuery.setParameter("minLivSqrFt", filterRequest.getLivingSquareFeet());
			createNativeQuery.setParameter("maxLivsqrFt", filterRequest.getMaxLivingSquareFeet());
		} else if (filterRequest.getLivingSquareFeet() != null) {
			createNativeQuery.setParameter("livingSqrft", filterRequest.getLivingSquareFeet());
		} else if (filterRequest.getMaxLivingSquareFeet() != null) {
			createNativeQuery.setParameter("maxLivingSqrft", filterRequest.getMaxLivingSquareFeet());
		}

		if (filterRequest.getLotSize() != null && filterRequest.getMaxLotSize() != null) {
			createNativeQuery.setParameter("minLotSize", filterRequest.getLotSize());
			createNativeQuery.setParameter("maxLotSize", filterRequest.getMaxLotSize());
		} else if (filterRequest.getLotSize() != null) {
			createNativeQuery.setParameter("lotSize", filterRequest.getLotSize());
		} else if (filterRequest.getMaxLotSize() != null) {
			createNativeQuery.setParameter("maxLotSize", filterRequest.getMaxLotSize());
		}
		if (filterRequest.getSqftPrice() != null && filterRequest.getMaxSqftPrice() != null) {
			createNativeQuery.setParameter("minSqftPrice", filterRequest.getSqftPrice());
			createNativeQuery.setParameter("maxSqftPrice", filterRequest.getMaxSqftPrice());
		} else if (filterRequest.getSqftPrice() != null) {
			createNativeQuery.setParameter("minSqftPrice", filterRequest.getSqftPrice());
		} else if (filterRequest.getMaxSqftPrice() != null) {
			createNativeQuery.setParameter("maxSqftPrice", filterRequest.getMaxSqftPrice());
		}
		if (filterRequest.getQualityScore() != null) {
			createNativeQuery.setParameter("qualityScore", filterRequest.getQualityScore());
		}
		if (filterRequest.getYear() != null && filterRequest.getMaxYear() != null) {
			createNativeQuery.setParameter("minYear", filterRequest.getYear());
			createNativeQuery.setParameter("maxYear", filterRequest.getMaxYear());
		} else if (filterRequest.getYear() != null) {
			createNativeQuery.setParameter("year", String.valueOf(filterRequest.getYear()));
		} else if (filterRequest.getMaxYear() != null) {
			createNativeQuery.setParameter("maxYear", String.valueOf(filterRequest.getMaxYear()));
		}
		if (!StringUtils.isEmpty(city)) {
			createNativeQuery.setParameter("city", city.toLowerCase());
		}
		if (!StringUtils.isEmpty(state)) {
			createNativeQuery.setParameter("state", state.toLowerCase());
		}
		if (!StringUtils.isEmpty(county)) {
			createNativeQuery.setParameter("county", county.toLowerCase());
		}
		if (!StringUtils.isEmpty(streetAddress)) {
			createNativeQuery.setParameter("streetAddress", streetAddress.toLowerCase());
		}
		if (!StringUtils.isEmpty(postal_code)) {
			createNativeQuery.setParameter("postal_code", Integer.valueOf(postal_code));
		}
	}

	private StringBuilder addAndCondition(StringBuilder conditionalQuery) {
		if (!StringUtils.isEmpty(conditionalQuery.toString())) {
			conditionalQuery = conditionalQuery.append(" and ");
		}
		return conditionalQuery;
	}
}
