package com.nic.properties.sevice;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nic.properties.entity.TBLPropertyEntity;
import com.nic.properties.entity.TblePropertyEntitySRPFilter;
import com.nic.properties.entity.ZIPEntity;
import com.nic.properties.model.PDPResponseDTO;
import com.nic.properties.model.SRPZipResponseDTO;
import com.nic.properties.model.TblePropertyModelSRPFilter;
import com.nic.properties.repository.CityZIPRepository;
import com.nic.properties.repository.SRPRepository;
import com.nic.properties.repository.TblePropertyEntitySRPFilterRepository;
import com.nic.properties.repository.smartZip.SmartZipBulkPropertyRepository;
import com.nic.properties.srpenum.ResultTypeENUM;
import com.nic.properties.srpenum.SRPEnum;
import com.nic.properties.util.ImageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class SrpService {

    private static final String Foreclosure = "ForeclosuresRS2";
    private static final String RENTTOOWN = "RentToOwn";
    private final Logger log = LoggerFactory.getLogger(SrpService.class);
    private final Long maximumValueOfLong = 9_223_372_036_854_775_807L;
    @Autowired
    private SRPRepository srpRepository;
    @Autowired
    private TblePropertyEntitySRPFilterRepository tblePropertyEntitySRPFilterRepository;
    @Autowired
    private SmartZipBulkPropertyRepository smartZipBulkPropertyRepository;
    @Autowired
    private CityZIPRepository cityZIPRepository;
    @Autowired
    private ImageUtils imageUtils;
    @Autowired
    private StreetName streetName;


    /**
     * @param cityName
     * @param state
     * @return
     * @code this method is intended to fetch search Results for Mini SRP
     */
    @Cacheable("fetchSearchResultsForMiniSRP")
    public List<TblePropertyModelSRPFilter> fetchSearchResultsForMiniSRP(String cityName, String state) {
        log.info("Fetching search results for Mini SRP");
        List<TblePropertyEntitySRPFilter> fetchSearchResults = new ArrayList<>();
        List<TblePropertyEntitySRPFilter> fetchSearchResultsForMiniSRP = tblePropertyEntitySRPFilterRepository
                .fetchSearchResultsForMiniSRPRentToOwn(RENTTOOWN, cityName, state);
        if (CollectionUtils.isEmpty(fetchSearchResultsForMiniSRP)) {
            fetchSearchResultsForMiniSRP = tblePropertyEntitySRPFilterRepository.fetchSearchResultsRentToOwn20Miles(RENTTOOWN, cityName, state);
        }

        List<String> foreclosureDetails = getForeclosureDetails(Foreclosure);
        List<TblePropertyEntitySRPFilter> fetchSearchResultsForMiniSRPVAFC = tblePropertyEntitySRPFilterRepository
                .fetchSearchResultsForMiniVaforeclosure(foreclosureDetails, cityName, state);

        if (CollectionUtils.isEmpty(fetchSearchResultsForMiniSRPVAFC)) {
            fetchSearchResultsForMiniSRPVAFC = tblePropertyEntitySRPFilterRepository
                    .fetchSearchResultsforSRPForeclosurenext20miles(foreclosureDetails, cityName, state);
        }
        fetchSearchResultsForMiniSRPVAFC = updateSaleType(fetchSearchResultsForMiniSRPVAFC, SRPEnum.ForeclosuresRS2.name());
        fetchSearchResults.addAll(fetchSearchResultsForMiniSRPVAFC);
        fetchSearchResults.addAll(fetchSearchResultsForMiniSRP);
        ObjectMapper objMap = new ObjectMapper();
        List<TblePropertyModelSRPFilter> convertValue = objMap.convertValue(fetchSearchResults, new TypeReference<List<TblePropertyModelSRPFilter>>() {
        });
        convertValue.forEach(x -> {
            if ((x.getStreetName() == null)) {
                x.setStreetName(streetName.getStreet(x.getStreetAddress()));
            }
            if (!(x.getSaleType().equalsIgnoreCase("FSBO")) && !(x.getSaleType().equalsIgnoreCase("ResaleMLS"))) {
                x.setStreetAddress(x.getStreetName());
            } else
                x.setStreetNumber(streetName.getStreetNumber(x.getStreetAddress()));
            if ((x.getLivingAreaSquareFeet() != null) && (x.getAmount() != null)) {
                x.setPricePerSqft(x.getAmount() / x.getLivingAreaSquareFeet());
            }
            if (x.getPostal_code().length() == 4)
                x.setPostal_code("0" + x.getPostal_code());
        });
        convertValue = imageUtils.updateImageHexaValue(convertValue);
        return convertValue;
    }


    /**
     * @param entityList
     * @param saleType
     * @return
     */
    public List<TblePropertyEntitySRPFilter> updateSaleType(List<TblePropertyEntitySRPFilter> entityList, String saleType) {
        List<TblePropertyEntitySRPFilter> newList = new ArrayList<>();
        for (TblePropertyEntitySRPFilter entity : entityList) {
            entity.setSaleType(saleType);
            newList.add(entity);
        }
        return newList;
    }

    /**
     * @param foreclosures
     * @return
     */
    private List<String> getForeclosureDetails(String foreclosures) {
        List<String> foreClosureTypes = new ArrayList<>();
        foreClosureTypes = srpRepository.fetchForeclosureTypes(foreclosures);
        return foreClosureTypes;
    }

    /**
     * @param id
     * @param address
     * @return
     */
    @Cacheable("searchAddressSubmit")
    public Object getSearchResultsForAddressUsingPropertyIDAndAddress(String id, String address) {
        PDPResponseDTO pdpResponseDTO;
        pdpResponseDTO = this.getPDPSRPResult(id);
        if (pdpResponseDTO.getPropertyId() == null) {
            SRPZipResponseDTO zipsrpResult = this.getZIPSRPResult(address);
            return zipsrpResult;
        } else {
            return pdpResponseDTO;
        }
    }

    /**
     * @param propertyId
     * @return
     */
    public PDPResponseDTO getPDPSRPResult(String propertyId) {
        log.info("Fetching PDP SRP Result");
        PDPResponseDTO pdpResponseDTO;
        Long parsedPropertyID = Long.parseLong(propertyId);
        boolean propertyIsValid = propertyId.length() <= 19 && parsedPropertyID < maximumValueOfLong;
        if (!propertyIsValid) {
            return new PDPResponseDTO();
        } else {
            Long parsedPropertyId = Long.parseLong(propertyId);
            TBLPropertyEntity propertyEntity = this.srpRepository.findFirstByPropertyIdEqualsOrAddressIdEquals(parsedPropertyId, parsedPropertyID);
            if (propertyEntity != null && propertyEntity.getPropertyId() != null) {
                pdpResponseDTO = this.setPDPDetailsForPDPResponseDTO(propertyEntity);
                log.info("Completed getPDPSRPResult");
                return pdpResponseDTO;
            } else {
                log.info("GetPDPSRPResult could not find the results for |" + propertyId + "|");
                return new PDPResponseDTO();
            }
        }
    }

    /**
     * @param searchKeyword
     * @return
     */
    public SRPZipResponseDTO getZIPSRPResult(String searchKeyword) {
        log.info("Fetching ZIP SRP Result ");
        SRPZipResponseDTO srpZipResponseDTO;
        List<String> zipSearchKeywordList = Arrays.asList(searchKeyword.split(","));
        int zipSearchKeywordListIndex = zipSearchKeywordList.size() - 1;
        List<String> zipCodeAndStateCode = Arrays.asList(zipSearchKeywordList.get(zipSearchKeywordListIndex).split(" "));
        int zipCodeIndex = zipCodeAndStateCode.size() - 1;
        BigInteger zipCode = new BigInteger(zipCodeAndStateCode.get(zipCodeIndex).trim());
        ZIPEntity zipEntity = this.cityZIPRepository.findFirstByCityZipCodeEquals(zipCode);
        if (zipEntity != null && zipEntity.getCityZipCode() != null) {
            srpZipResponseDTO = this.setZIPDetailsForSRPZipResponseDTO(zipEntity);
            log.info("Completed getZIPSRPResult");
            return srpZipResponseDTO;
        } else {
            log.info("GetZIPSRPResult could not find the results for |" + searchKeyword + "|");
            return new SRPZipResponseDTO();
        }
    }

    /**
     * @param zipEntity
     * @return
     */
    private SRPZipResponseDTO setZIPDetailsForSRPZipResponseDTO(ZIPEntity zipEntity) {
        SRPZipResponseDTO srpZipResponseDTO = new SRPZipResponseDTO();
        srpZipResponseDTO.setResultType(ResultTypeENUM.ZIP_SRP);
        srpZipResponseDTO.setStateCode(zipEntity.getStateName());
        srpZipResponseDTO.setCountyName(zipEntity.getCounty().getCountyName());
        srpZipResponseDTO.setCityName(zipEntity.getCityName());
        String zipCode = zipEntity.getCityZipCode().toString().trim();
        if (zipCode.length() == 4) {
            zipCode = "0".concat(zipCode);
        }
        srpZipResponseDTO.setZipCode(zipCode);
        return srpZipResponseDTO;
    }

    /**
     * @param propertyEntity
     * @return
     */
    private PDPResponseDTO setPDPDetailsForPDPResponseDTO(TBLPropertyEntity propertyEntity) {
        PDPResponseDTO pdpResponseDTO = new PDPResponseDTO();
        pdpResponseDTO.setResultType(ResultTypeENUM.PDP_SRP);
        pdpResponseDTO.setStateCode(propertyEntity.getStateOrProvince());
        pdpResponseDTO.setCountyName(propertyEntity.getCounty());
        pdpResponseDTO.setCityName(propertyEntity.getCity());
        String zipCode = propertyEntity.getPostal_code().toString().trim();
        if (zipCode.length() == 4) {
            zipCode = "0".concat(zipCode);
        }
        pdpResponseDTO.setZipCode(zipCode);
        pdpResponseDTO.setPropertyId(BigInteger.valueOf(propertyEntity.getPropertyId()));
        if (propertyEntity.getSaleType().equalsIgnoreCase("ResaleMLS") || propertyEntity.getSaleType().equalsIgnoreCase("FSBO")) {
            pdpResponseDTO.setStreetAddress(propertyEntity.getStreetAddress());
        } else {
            pdpResponseDTO.setStreetAddress("");
        }
        return pdpResponseDTO;
    }

}
