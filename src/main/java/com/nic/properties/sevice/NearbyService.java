package com.nic.properties.sevice;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nic.properties.entity.MedianHomePriceByGeo;
import com.nic.properties.model.NearByMediamHomePriceByGeoModel;
import com.nic.properties.repository.CountyRepository;
import com.nic.properties.repository.SRPRepository;
import com.nic.properties.repository.ZipRepository;
import com.nic.properties.repository.smartZip.MedianHomePriceByGeoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


import java.util.*;

@Service
public class NearbyService {

    Logger log = LoggerFactory.getLogger(NearbyService.class);
    @Autowired
    private CountyRepository countyRepository;
    @Autowired
    private MedianHomePriceByGeoRepository medianHomePriceByGeoRepository;
    @Autowired
    private ZipRepository zipRepository;
    @Autowired
    private SRPRepository srpRepository;

    /**
     * @param state
     * @param county
     * @param saleType
     * @return
     */
    @Cacheable("NearByCounty")
    public Map<String, Object> getNearbyCounties(String state, String county, String saleType) {
        log.info("fetching near by counties");
        List<String> saleTypesList = getSaleTypesList(saleType);
        List<MedianHomePriceByGeo> list = new ArrayList<>();
        List<NearByMediamHomePriceByGeoModel> nearByMediamHomePriceByGeoModelList = new ArrayList<>();
        Map<String, Object> zipMap = new HashMap<>();
        List<MedianHomePriceByGeo> medianHomePriceByGeoList = new ArrayList<>();
        List<String> city = new ArrayList<>();
        Integer fips = countyRepository.getfips(state, county, saleTypesList);
        String cityForZip = null;
        zipMap.put("Nearby Counties", medianHomePriceByGeoRepository.getMedianHomePriceListByFips(fips));

        if (fips == null) {
            fips = countyRepository.getfipsByStateIfNull(state, saleTypesList);
        }
        zipMap.put("Nearby Counties", medianHomePriceByGeoRepository.getMedianHomePriceListByFips(fips));
        city = zipRepository.getCityByFips(fips);
        for (String strCity : city) {
            medianHomePriceByGeoList = medianHomePriceByGeoRepository.getMedianHomePriceByCity(state, strCity, saleTypesList);
            if (!medianHomePriceByGeoList.isEmpty()) {
                cityForZip = strCity;
                break;
            }
        }
        list.addAll(medianHomePriceByGeoList);
        nearByMediamHomePriceByGeoModelList.addAll(convertEntityList(list));
        nearByMediamHomePriceByGeoModelList.parallelStream().forEach(x -> {
            x.setCounty(countyRepository.getCountyByCity(new StringTokenizer(x.getGeo(), ",").nextToken()));
        });
        zipMap.put("NearBy Cities", nearByMediamHomePriceByGeoModelList);
        List<Integer> zipList = zipRepository.getzipsListByCity(cityForZip);
        List<MedianHomePriceByGeo> medianHomePriceByGeoByZipList = new ArrayList<>();
        for (Integer zipStr : zipList) {
            medianHomePriceByGeoByZipList = medianHomePriceByGeoRepository.getMedianHomePriceByZip(zipStr, saleTypesList);
            if (!medianHomePriceByGeoByZipList.isEmpty()) {
                break;
            }
        }
        nearByMediamHomePriceByGeoModelList = convertEntityList(medianHomePriceByGeoByZipList);
        nearByMediamHomePriceByGeoModelList.parallelStream().forEach(x -> {
            x.setCounty(countyRepository.getCountyByZip(Integer.valueOf(x.getGeo())));
            x.setCity(zipRepository.getcity(Integer.valueOf(x.getGeo())));
            x.setState(zipRepository.getStateByZip(Integer.valueOf(x.getGeo())));
            if(x.getGeo().length()==4)
                x.setGeo("0"+x.getGeo());
        });
        zipMap.put("nearby Zip", nearByMediamHomePriceByGeoModelList);
        return zipMap;
    }

    /**
     * @param state
     * @param county
     * @param city
     * @param saleType
     * @return
     */
    @Cacheable("NearByCity")
    public Map<String, Object> getNearbyCities(String state, String county, String city, String saleType) {
        log.info("Fetching near by cities");
        List<String> saleTypesList = getSaleTypesList(saleType);
        List<NearByMediamHomePriceByGeoModel> nearByMediamHomePriceByGeoModelList = new ArrayList<>();
        Map<String, Object> zipMap = new HashMap<>();
        Integer fips = countyRepository.getfipsByCity(state, city, saleTypesList);
        if (fips == null) {
            fips = countyRepository.getCountyByName(county);
        }
        List<MedianHomePriceByGeo> medianHomePriceByGeoList = new ArrayList<>();
        List<Integer> zipList = zipRepository.getzipsListByCityAndStateAndFips(city, state, fips);
        for (Integer zipStr : zipList) {
            medianHomePriceByGeoList = medianHomePriceByGeoRepository.getMedianHomePriceByZip(zipStr, saleTypesList);
            if (!medianHomePriceByGeoList.isEmpty())
                break;
        }
        zipMap.put("Nearby Counties", medianHomePriceByGeoRepository.getMedianHomePriceListByFips(fips));
        nearByMediamHomePriceByGeoModelList.addAll(convertEntityList((medianHomePriceByGeoRepository.getMedianHomePriceByCity(state, city, saleTypesList))));
        nearByMediamHomePriceByGeoModelList.parallelStream().forEach(x -> {
            x.setCounty(countyRepository.getCountyByCity(new StringTokenizer(x.getGeo(), ",").nextToken()));
        });
        zipMap.put("NearBy Cities", nearByMediamHomePriceByGeoModelList);
        nearByMediamHomePriceByGeoModelList = convertEntityList(medianHomePriceByGeoList);
        nearByMediamHomePriceByGeoModelList.parallelStream().forEach(x -> {
            x.setCounty(countyRepository.getCountyByZip(Integer.valueOf(x.getGeo())));
            x.setCity(zipRepository.getcity(Integer.valueOf(x.getGeo())));
            x.setState(zipRepository.getStateByZip(Integer.valueOf(x.getGeo())));
            if(x.getGeo().length()==4)
                x.setGeo("0"+x.getGeo());
        });
        zipMap.put("nearby Zip", nearByMediamHomePriceByGeoModelList);
        return zipMap;
    }

    /**
     * @param state
     * @param county
     * @param city
     * @param zip
     * @param saleType
     * @return
     */
    @Cacheable("NearByZips")
    public Map<String, Object> getNearbyZips(String state, String county, String city, Integer zip, String saleType) {
        log.info("Fetching near by Zips");
        List<String> saleTypesList = getSaleTypesList(saleType);
        List<NearByMediamHomePriceByGeoModel> nearByMediamHomePriceByGeoModelList = new ArrayList<>();
        Map<String, Object> zipMap = new HashMap<>();
        Integer fips = countyRepository.getfips(state, county, saleTypesList);
        List<String> cityList = zipRepository.getCityByFips(fips);
        zipMap.put("NearBy Counties", medianHomePriceByGeoRepository.getMedianHomePriceListByFips(fips));
        nearByMediamHomePriceByGeoModelList.addAll(convertEntityList(medianHomePriceByGeoRepository.getMedianHomePriceByZip(zip, saleTypesList)));

        nearByMediamHomePriceByGeoModelList.parallelStream().forEach(x -> {
            x.setCounty(countyRepository.getCountyByZip(Integer.valueOf(x.getGeo())));
            x.setCity(zipRepository.getcity(Integer.valueOf(x.getGeo())));
            x.setState(zipRepository.getStateByZip(Integer.valueOf(x.getGeo())));
            if(x.getGeo().length()==4)
                x.setGeo("0"+x.getGeo());
        });
        zipMap.put("nearby Zip", nearByMediamHomePriceByGeoModelList);
        List<MedianHomePriceByGeo> medianHomePriceByGeoList = medianHomePriceByGeoRepository.getMedianHomePriceByCity(state, city, saleTypesList);
        if (medianHomePriceByGeoList.isEmpty()) {
            for (String strCity : cityList) {
                medianHomePriceByGeoList = medianHomePriceByGeoRepository.getMedianHomePriceByCity(state, strCity, saleTypesList);
                if (!medianHomePriceByGeoList.isEmpty()) {
                    break;
                }
            }
        }
        nearByMediamHomePriceByGeoModelList = convertEntityList(medianHomePriceByGeoList);
        nearByMediamHomePriceByGeoModelList.parallelStream().forEach(x -> {
            x.setCounty(countyRepository.getCountyByCity(new StringTokenizer(x.getGeo(), ",").nextToken()));
        });
        zipMap.put("NearBy Cities", nearByMediamHomePriceByGeoModelList);
        return zipMap;
    }

    public List<String> getSaleTypesList(String saleType) {
        List<String> saleTyepList = new ArrayList<String>();
        if (saleType.equalsIgnoreCase("ForeclosuresRS2") || saleType.equalsIgnoreCase("PreForeclosures") || saleType.equalsIgnoreCase("Auctions") || saleType.equalsIgnoreCase("HUDHomesForeclosureRS2") || saleType.equalsIgnoreCase("TaxSales") || saleType.equalsIgnoreCase("HUDOrGovtForeclosures") || saleType.equalsIgnoreCase("BankForeclosures")) {
            saleTyepList = srpRepository.fetchForeclosureTypes(saleType);
        } else {
            saleTyepList.add(saleType);
        }
        return saleTyepList;
    }

    public List<NearByMediamHomePriceByGeoModel> convertEntityList(List<MedianHomePriceByGeo> entityList) {
        if (entityList == null || entityList.isEmpty()) {
            return Collections.emptyList();
        }
        ObjectMapper objMap = new ObjectMapper();
        return objMap.convertValue(entityList, new TypeReference<List<NearByMediamHomePriceByGeoModel>>() {
        });
    }
}
