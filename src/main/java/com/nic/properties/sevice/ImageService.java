package com.nic.properties.sevice;

import com.nic.properties.repository.ImagesRepository;
import com.nic.properties.util.ImageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImageService {
    @Autowired
    ImagesRepository imagesRepository;
    @Autowired
    ImageUtils imageUtils;
    @Cacheable("getImagesByPid")
    public List<String> getImagesByPid(Integer propertyId)
    {
     return  imageUtils.getImageUrlHexForImageList(imagesRepository.getImagesUrlById(propertyId));
    }

}
