package com.nic.properties.sevice;

import com.nic.properties.QEntities.QProperty;
import com.nic.properties.model.PropertyDTO;
import com.nic.properties.repositoryImpl.NearestCityRepository;
import com.nic.properties.repositoryImpl.PropertyRepositoryImpl;
import com.nic.properties.util.PropertyUtils;
import com.querydsl.core.BooleanBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Service for Property.java processes.
 */
@Service
public class PropertyService {

    private static final QProperty qProperty = QProperty.property;
    private static final Map<String, List<String>> saleGroupsAndSaleTypesMap = new HashMap<>();

    static {
        saleGroupsAndSaleTypesMap.put("foreclosuresrs2", new ArrayList<>(
                        Arrays.asList(
                                "PreForeclosureNOD",
                                "PreForeclosureLisPendens",
                                "ShortSale",
                                "REOForeclosure",
                                "HUDForeclosure",
                                "FannieMaeOrFreddieMac",
                                "VAForeclosure",
                                "Auction"
                        )
                )
        );
        saleGroupsAndSaleTypesMap.put("taxsales", new ArrayList<>(
                        Arrays.asList(
                                "TaxDeed",
                                "TaxLien",
                                "RedeemableDeed",
                                "TaxTaking"
                        )
                )
        );
        saleGroupsAndSaleTypesMap.put("renttoown", new ArrayList<>(
                        Collections.singletonList(
                                "RentToOwn"
                        )
                )
        );
        saleGroupsAndSaleTypesMap.put("hudorgovtforeclosures", new ArrayList<>(
                        Arrays.asList(
                                "HUDForeclosure",
                                "FannieMaeOrFreddieMac",
                                "VAForeclosure"
                        )
                )
        );
        saleGroupsAndSaleTypesMap.put("resalemls", new ArrayList<>(
                        Collections.singletonList(
                                "ResaleMLS"
                        )
                )
        );
        saleGroupsAndSaleTypesMap.put("shortsale", new ArrayList<>(
                        Collections.singletonList(
                                "ShortSale"
                        )
                )
        );
        saleGroupsAndSaleTypesMap.put("hudhomesforeclosurers2", new ArrayList<>(
                        Arrays.asList(
                                "PreForeclosureNOD",
                                "PreForeclosureLisPendens"
                        )
                )
        );
        saleGroupsAndSaleTypesMap.put("rental", new ArrayList<>(
                        Collections.singletonList(
                                "Rental"
                        )
                )
        );
        saleGroupsAndSaleTypesMap.put("preforeclosures", new ArrayList<>(
                        Arrays.asList(
                                "PreForeclosureNOD",
                                "PreForeclosureLisPendens"
                        )
                )
        );
        saleGroupsAndSaleTypesMap.put("auctions", new ArrayList<>(
                        Collections.singletonList(
                                "Auction"
                        )
                )
        );
        saleGroupsAndSaleTypesMap.put("bankforeclosures", new ArrayList<>(
                        Collections.singletonList(
                                "REOForeclosure"
                        )
                )
        );
        saleGroupsAndSaleTypesMap.put("fsbo", new ArrayList<>(
                        Collections.singletonList(
                                "FSBO"
                        )
                )
        );
    }

    private final NearestCityRepository nearestCityRepository;
    private final PropertyRepositoryImpl propertyRepositoryImpl;
    private final PropertyUtils propertyUtils;

    public PropertyService(NearestCityRepository nearestCityRepository, PropertyRepositoryImpl propertyRepositoryImpl, PropertyUtils propertyUtils) {
        this.nearestCityRepository = nearestCityRepository;
        this.propertyRepositoryImpl = propertyRepositoryImpl;
        this.propertyUtils = propertyUtils;
    }


    @Cacheable("findByStateAndCityAndMultipleSaleTypesForMiniSRP")
    public List<PropertyDTO> findByStateAndCityAndMultipleSaleTypesForMiniSRP(String stateCode, String cityName, List<String> saleTypes) {
        List<PropertyDTO> result = new ArrayList<>();
        saleTypes.forEach(saleType ->  {
            List<PropertyDTO> resultList = this.findByStateAndCityAndSaleTypeForMiniSRP(stateCode, cityName, saleType);
            result.addAll(resultList);
        });
        return result;
    }

    /**
     * Looks for 4 properties for the given parameters stateCode, cityName and saleType
     * if not found within the provided parameters,
     * looks for cities within 20 miles of radius from the provided cityName.
     *
     * @return a list of PropertyDTO.java as a result of the search parameters.
     */
    private List<PropertyDTO> findByStateAndCityAndSaleTypeForMiniSRP(String stateCode, String cityName, String saleType) {
        // SaleTypes list to hold all the saleTypes that need to be searched for.
        List<String> saleTypesToBeSearchedFor = new ArrayList<>();

        if (saleType == null || saleType.isEmpty() || saleType.isBlank()) {
            saleTypesToBeSearchedFor.add("ForeclosuresRS2");
        } else {
            if (saleGroupsAndSaleTypesMap.containsKey(saleType.toLowerCase())) {
                saleTypesToBeSearchedFor.addAll(saleGroupsAndSaleTypesMap.get(saleType.toLowerCase()));
            } else {
                saleTypesToBeSearchedFor.add(saleType);
            }
        }
        List<PropertyDTO> result = new ArrayList<>();

        // Search criterion for the provided parameters.
        BooleanBuilder searchCriterion = new BooleanBuilder();
        searchCriterion.and(qProperty.stateOrProvince.equalsIgnoreCase(stateCode));
        searchCriterion.and(qProperty.city.equalsIgnoreCase(cityName));
        searchCriterion.and(qProperty.saleType.in(saleTypesToBeSearchedFor));

        Long count = this.propertyRepositoryImpl.createCountQueryForProperties(searchCriterion);

        // Get result for cities withing twenty miles of radius if *createCountQueryForProperties* returns 0.
        if (count == 0L) {
            List<String> cities = this.nearestCityRepository.getNearestCitiesByStateCodeAndCityName(stateCode, cityName);
            result.addAll(this.propertyRepositoryImpl.findByStateCodeAndCityInAndSaleTypesInWithinTwentyMilesOfRadius(stateCode, cities, saleTypesToBeSearchedFor));
        } else {
            result.addAll(this.propertyRepositoryImpl.findByStateCodeCityAndSaleTypeForMiniSRP(stateCode, cityName, saleTypesToBeSearchedFor));
        }
        if (!result.isEmpty()) {
            // Modify PropertyDTO according to the class properties required.
            this.propertyUtils.transformPropertyDTOs(result, saleType);
        }
        return result;
    }
}
