package com.nic.properties.repository.smartZip;

import com.nic.properties.entity.MedianHomePriceByGeo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedianHomePriceByGeoRepository extends JpaRepository<MedianHomePriceByGeo, String> {

    @Query(value = "select mhg.mdn, mhg.geo, mhg.geo_type, nc.proximity FROM nic_pdb.tbl_nearest_counties nc left join nic_pdb.CNTY c "
            + "	on nc.fips = c.FIPS "
            + "	left join nic_smartzip.median_home_price_by_geo mhg on concat(c.name, ',', c.state)=mhg.geo "
            + "	where nc.search_fips= :fips "
            + "	and mhg.geo_type= 'county' "
            + "	group by nc.fips "
            + "	HAVING nc.proximity <=nc.proximity+ 15.00 "
            + "	order by proximity ASC limit 20;", nativeQuery = true)
    List<MedianHomePriceByGeo> getMedianHomePriceListByFips(@Param("fips") Integer fips);


    @Query(value = "SELECT median.geo , median.mdn, median.geo_type FROM nic_pdb.tbl_nearest_zips zips LEFT JOIN nic_pdb.tbl_sum_listings_by_zip_sale_type sumListingsByZipSaleType ON zips.zip = sumListingsByZipSaleType.postal_code LEFT JOIN nic_smartzip.median_home_price_by_geo median ON zips.zip = median.geo WHERE zips.search_zip = :zip AND median.geo_type = 'zipcode' AND (sumListingsByZipSaleType.sale_type ) IN (:saleTypesList) GROUP BY zips.proximity HAVING zips.proximity <= zips.proximity + 15.00 ORDER BY zips.proximity ASC LIMIT 20", nativeQuery = true)
    List<MedianHomePriceByGeo> getMedianHomePriceByZip(@Param("zip") Integer zip, @Param("saleTypesList") List<String> saleTypesList);


    @Query(value = "SELECT "
            + "   median.geo, median.mdn, median.geo_type "
            + "FROM "
            + "   nic_pdb.tbl_nearest_cities cities "
            + "       LEFT JOIN "
            + "   nic_pdb.tbl_sum_listings_by_city_sale_type sbc ON cities.city = sbc.city "
            + "       LEFT JOIN "
            + "   nic_smartzip.median_home_price_by_geo median ON CONCAT(cities.city, ',', cities.state) = median.geo "
            + "WHERE "
            + "   cities.search_state = :state "
            + "   AND cities.search_city = :city "
            + "       AND median.geo_type = 'city' "
            + "       And (sbc.sale_type ) IN (:saleTypesList) "
            + "GROUP BY cities.proximity "
            + "HAVING cities.proximity <= cities.proximity + 15.00 "
            + "ORDER BY cities.proximity ASC limit 20", nativeQuery = true)
    List<MedianHomePriceByGeo> getMedianHomePriceByCity(@Param("state") String state, @Param("city") String city, @Param("saleTypesList") List<String> saleTypesList);

}
