package com.nic.properties.repository.smartZip;

import com.nic.properties.entity.SmartZipBulkProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface SmartZipBulkPropertyRepository extends JpaRepository<SmartZipBulkProperty, BigInteger> {

}
