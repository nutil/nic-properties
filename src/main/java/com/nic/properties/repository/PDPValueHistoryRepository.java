package com.nic.properties.repository;

import com.nic.properties.entity.ValueHistoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PDPValueHistoryRepository extends JpaRepository<ValueHistoryEntity, Long> {

    @Query(value = "select H.pid, H.yrmo,H.avm"
            + " FROM nic_smartzip.tbl_smartzip_avm_history H where H.pid=:smartZipId limit 10;", nativeQuery = true)
    List<ValueHistoryEntity> findEstimatedValueHistory(String smartZipId);

}
