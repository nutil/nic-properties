package com.nic.properties.repository;

import com.nic.properties.entity.TBLPropertyEntity;
import com.nic.properties.model.CountyResultModel;
import com.nic.properties.model.PopularNearbyZips;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

public interface SRPRepository extends JpaRepository<TBLPropertyEntity, Long> {

    @Query(value = "SELECT P1.property_id,P1.full_street_name, P1.first_merge_date,P1.sale_type,P1.amount,P1.bedrooms,P1.fixer_upper,P1.baths_total,P1.living_area_square_feet,P1.year_built,P1.lot_size,P1.home_score,P1.investor_score,P1.street_address,P1.city,P1.county,P1.state_or_province,P1.postal_code ,P2.source_url,P2.url "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id  where LOWER(P1.sale_type )like %:saleType AND LOWER(P1.city )like %:cityName% AND LOWER(P1.state_or_province) like %:state  limit 10", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResults(@Param("saleType") String saleType, @Param("cityName") String cityName,
                                               @Param("state") String state);

    @Query(value = "SELECT P1.property_id,P1.full_street_name, P1.first_merge_date,P1.sale_type,P1.amount,P1.bedrooms,P1.fixer_upper,P1.baths_total,P1.living_area_square_feet,P1.lot_size,P1.year_built,P1.home_score,P1.investor_score,P1.street_address,P1.city,P1.county,P1.state_or_province,P1.postal_code ,P2.source_url,P2.url "
            + " FROM nic_pdb.tbl_prop P1 join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where LOWER(P1.sale_type) LIKE %:saleType AND  LOWER(P1.state_or_province) LIKE %:state and LOWER(P1.city)  IN (select LOWER(search_city) from nic_pdb.tbl_nearest_cities where LOWER(search_city) = :cityName AND LOWER(state_or_province)=:state and proximity<=20) ORDER BY P1.quality_score_3 DESC limit 8", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsfornext20miles(@Param("saleType") String saleType,
                                                             @Param("cityName") String cityName, @Param("state") String state);

    @Query(value = "SELECT P1.full_street_name, P1.property_id,P1.first_merge_date,P1.sale_type,P1.amount,P1.bedrooms,P1.fixer_upper,P1.baths_total,P1.living_area_square_feet,P1.year_built,P1.lot_size,P1.home_score,P1.investor_score,P1.street_address,P1.city,P1.county,P1.state_or_province,P1.postal_code ,P2.source_url,P2.url "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where LOWER(P1.sale_type) IN (:saleType)  AND LOWER(P1.state_or_province) like %:state AND (P1.city) IN (select LOWER(search_city) from nic_pdb.tbl_nearest_cities where LOWER(search_city) = :cityName AND LOWER(state_or_province)=:state and proximity<=20) ORDER BY P1.quality_score_3 DESC limit 8", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsfornext20milesForForeClosure(@Param("saleType") List<String> saleType,
                                                                           @Param("cityName") String cityName, @Param("state") String state);

    //Added by Raghuveera Reddy on 09-09-2021
    @Query(value = "select distinct sale_group_type from nic_pdb.property_taxonomy where sale_group_type not in (:saleType)", nativeQuery = true)
    List<String> fetchSaleGroupTypes(@Param("saleType") String saleType);

    @Query(value = "select distinct sale_type from nic_pdb.property_taxonomy where sale_group_type = :saleType", nativeQuery = true)
    List<String> fetchForeclosureTypes(@Param("saleType") String saleType);

    @Query(value = "SELECT DISTINCT P1.* "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where (P1.sale_type)=:rentToOwn AND (P1.city)=:cityName AND (P1.state_or_province)=:state ORDER BY P1.quality_score_2 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsForMiniSRPRentToOwn(@Param("rentToOwn") String rentToOwn,
                                                                  @Param("cityName") String cityName, @Param("state") String state);

    @Query(value = "SELECT DISTINCT P1.* "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 ON P1.property_id = P2.property_id where (P1.sale_type ) IN (:foreclosureDetails) AND (P1.city)=:cityName AND (P1.state_or_province)=:state ORDER BY P1.quality_score_2 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsForMiniVaforeclosure(
            @Param("foreclosureDetails") List<String> foreclosureDetails, @Param("cityName") String cityName,
            @Param("state") String state);

    @Query(value = "SELECT DISTINCT P1.* "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where (P1.sale_type )=:rentToOwn AND (P1.city) IN (select search_city from nic_pdb.tbl_nearest_cities where (search_city) = :cityName AND (state_or_province)=:state and proximity<=20) AND (P1.state_or_province)=:state ORDER BY P1.quality_score_2 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsRentToOwn20Miles(@Param("rentToOwn") String rentToOwn,
                                                               @Param("cityName") String cityName, @Param("state") String state);

    @Query(value = "SELECT DISTINCT P1.* "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where (P1.sale_type) IN (:saleType)  AND (P1.state_or_province) like %:state% AND (P1.city) IN (select search_city from nic_pdb.tbl_nearest_cities where search_city = :cityName AND state_or_province=:state and proximity<=20) ORDER BY P1.quality_score_2 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsforSRPForeclosurenext20miles(@Param("saleType") List<String> saleType,
                                                                           @Param("cityName") String cityName, @Param("state") String state);

    @Query(name = "fetchPopularNearbyZips", nativeQuery = true)
    List<PopularNearbyZips> findByStateNameAndSaleType(
            @Param(value = "stateToBeSearched") String stateToBeSearched,
            @Param(value = "saleTypesToBeSearchedFor") Collection<String> saleTypesToBeSearchedFor
    );

    @Query(value = "SELECT" +
            " new com.nic.properties.model.CountyResultModel(" +
            " p.county," +
            " p.city," +
            " COUNT(p.propertyId))" +
            " FROM " +
            " TBLPropertyEntity p" +
            " WHERE " +
            " p.stateOrProvince = :stateNameToBeSearchedFor " +
            " AND p.saleType IN (:saleTypesToBeSearchedFor) " +
            " GROUP BY p.county, p.city " +
            " ORDER BY p.county ASC")
    List<CountyResultModel> fetchCountyCountsByStateNameAndSaleTypeGroupByCounty(
            @Param(value = "stateNameToBeSearchedFor") String stateNameToBeSearchedFor,
            @Param(value = "saleTypesToBeSearchedFor") List<String> saleTypesToBeSearchedFor);

    @Query(value = "SELECT" +
            " new com.nic.properties.model.CountyResultModel(" +
            " p.county," +
            " p.city," +
            " COUNT(p.propertyId))" +
            " FROM " +
            " TBLPropertyEntity p" +
            " WHERE " +
            " p.stateOrProvince = :stateNameToBeSearchedFor " +
            " AND p.county = :countyNameToBeSearchedFor "+
            " AND p.city = :cityNameToBeSearchedFor "+
            " AND p.saleType IN (:saleTypesToBeSearchedFor) " +
            " GROUP BY p.county, p.city " +
            " ORDER BY p.city ASC")
    List<CountyResultModel> fetchCountyCountsByStateNameAndSaleTypeGroupByCity(
            @Param(value = "stateNameToBeSearchedFor") String stateNameToBeSearchedFor,
            @Param(value = "countyNameToBeSearchedFor") String countyNameToBeSearchedFor,
            @Param(value = "cityNameToBeSearchedFor") String cityNameToBeSearchedFor,
            @Param(value = "saleTypesToBeSearchedFor") List<String> saleTypesToBeSearchedFor);

    TBLPropertyEntity findFirstByPropertyIdEqualsOrAddressIdEquals(Long propertyId, Long addressId);

@Query(value = "select FIPS from nic_pdb.tbl_prop where county=?1 and state_or_province=?2 limit 1", nativeQuery = true)
    Integer getFipsbyStateAndCounty(String county, String state);
}
