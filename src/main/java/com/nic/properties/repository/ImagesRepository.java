package com.nic.properties.repository;

import com.nic.properties.entity.TBLImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImagesRepository extends JpaRepository<TBLImageEntity, Integer> {
    @Query(value = "Select url from nic_pdb.tbl_images where property_id =?1", nativeQuery = true)
    List<String> getImagesUrlById(Integer propertyId);
}
