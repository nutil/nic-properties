package com.nic.properties.repository;


import com.nic.properties.entity.TBLPropertyAssociatesEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PDPAssociateRepository extends JpaRepository<TBLPropertyAssociatesEntity, Long> {

    @Query(value = "select P1.associate_id,P1.property_id,A1.given_name,A1.surname,A1.company_name,A1.full_name, A1.street_address,A1.city,A1.county,A1.state_code,A1.phone"
            + " FROM nic_pdb.tbl_prop_associates P1 left join nic_pdb.tbl_associates A1 on P1.associate_id = A1.associate_id where P1.property_id = :propertyId order by A1.associate_id ASC;", nativeQuery = true)
    List<TBLPropertyAssociatesEntity> findAssociatesByPropertyId(@Param("propertyId") Integer propertyId);

}
