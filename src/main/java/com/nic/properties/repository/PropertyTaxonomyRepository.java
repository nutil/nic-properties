package com.nic.properties.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nic.properties.entity.PropertyTaxonomy;
@Repository
public interface PropertyTaxonomyRepository extends JpaRepository<PropertyTaxonomy, String>
{
@Query(value="Select sale_group_type from nic_pdb.property_taxonomy where sale_type=:saleType and sale_group_type in ( 'ResaleMLS ', 'ForeclosuresRS2 ', 'HUDHomesForeclosureRS2 ', 'FSBO ', 'BankForeclosures ', 'PreForeclosures ', 'ShortSale ', 'RentToOwn ', 'Rental ', 'Auction ', 'TaxSales') group by sale_group_type limit 1;",nativeQuery = true)
String getSaleGroupTypeById(@Param("saleType") String saleType);

}
