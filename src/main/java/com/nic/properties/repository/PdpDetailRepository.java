package com.nic.properties.repository;

import com.nic.properties.entity.PropertyDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PdpDetailRepository extends JpaRepository<PropertyDetails, Integer> {

    @Query(value = "SELECT sale_type FROM nic_pdb.tbl_prop where property_id =:id ;", nativeQuery = true)
    String getSaleTypebyId(@Param("id") Integer id);

    @Query(value = "SELECT property_id, sale_type,  full_street_name, city, latitude, longitude,  description, state_or_province, (amount/living_area_square_feet) as amount, lot_size, total_units, total_rooms,  room_list, stories, parking, parking_spaces, heating, roof_type, year_built,  mls_number, parcel_number, property_record_type, county, legal_description, zoning, reo_document_number,reo_recording_date, nots_trustee_sale_number, auction_date, auction_time, opening_bid, nots_auction_address, nots_auction_recording_date, nots_loan_default_amount, nots_loan_date, nots_loan_no, nod_date_defaulted_lien, nod_amount_default, nod_date_default, nod_recording_date, nod_recording_year, nod_document_number FROM nic_pdb.tbl_prop where property_id= :id  order by property_id;", nativeQuery = true)
    PropertyDetails getPdById(@Param("id") Integer id);

    @Query(value = "Select street_address from nic_pdb.tbl_prop where property_id=?1", nativeQuery = true)
    String getStreetAddressbyPid(Integer propertyId);

}