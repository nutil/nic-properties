package com.nic.properties.repository;

import com.nic.properties.entity.TBLPropertyEntity;
import com.nic.properties.entity.TblePropertyEntitySRPFilter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TblePropertyEntitySRPFilterRepository extends JpaRepository<TblePropertyEntitySRPFilter, Long> {

    @Query(value = "SELECT DISTINCT P1.* "
            + " FROM nic_pdb.tbl_prop P1 where (P1.sale_type)=:rentToOwn AND (P1.city)=:cityName AND (P1.state_or_province)=:state ORDER BY P1.quality_score_2 DESC limit 4", nativeQuery = true)
    List<TblePropertyEntitySRPFilter> fetchSearchResultsForMiniSRPRentToOwn(@Param("rentToOwn") String rentToOwn,
                                                                            @Param("cityName") String cityName, @Param("state") String state);

    @Query(value = "SELECT DISTINCT P1.* "
            + " FROM nic_pdb.tbl_prop P1  where (P1.sale_type ) IN (:foreclosureDetails) AND (P1.city)=:cityName AND (P1.state_or_province)=:state ORDER BY P1.quality_score_2 DESC limit 4", nativeQuery = true)
    List<TblePropertyEntitySRPFilter> fetchSearchResultsForMiniVaforeclosure(
            @Param("foreclosureDetails") List<String> foreclosureDetails, @Param("cityName") String cityName,
            @Param("state") String state);

    @Query(value = "SELECT DISTINCT P1.* "
            + " FROM nic_pdb.tbl_prop P1 where (P1.sale_type )=:rentToOwn AND (P1.city) IN (select search_city from nic_pdb.tbl_nearest_cities where (search_city) = :cityName AND (state_or_province)=:state and proximity<=20) AND (P1.state_or_province)=:state ORDER BY P1.quality_score_2 DESC limit 4", nativeQuery = true)
    List<TblePropertyEntitySRPFilter> fetchSearchResultsRentToOwn20Miles(@Param("rentToOwn") String rentToOwn,
                                                                         @Param("cityName") String cityName, @Param("state") String state);

    @Query(value = "SELECT DISTINCT P1.* "
            + " FROM nic_pdb.tbl_prop P1 where (P1.sale_type) IN (:saleType)  AND (P1.state_or_province) like %:state% AND (P1.city) IN (select search_city from nic_pdb.tbl_nearest_cities where search_city = :cityName AND state_or_province=:state and proximity<=20) ORDER BY P1.quality_score_2 DESC limit 4", nativeQuery = true)
    List<TblePropertyEntitySRPFilter> fetchSearchResultsforSRPForeclosurenext20miles(@Param("saleType") List<String> saleType,
                                                                                     @Param("cityName") String cityName, @Param("state") String state);
    @Query(value = "SELECT P1.* "
            + " FROM nic_pdb.tbl_prop P1  where P1.property_id != :propertyId and P1.sale_type IN (:saleTypesList) AND P1.city=:cityName AND P1.county=:county AND P1.state_or_province=:state group by P1.property_id ORDER BY P1.quality_score_2 DESC limit 4", nativeQuery = true)
    List<TblePropertyEntitySRPFilter> fetchSearchResultsForPDPByCounty(@Param("propertyId") String propertyId,
                                                             @Param("saleTypesList") List<String> saleTypesList, @Param("cityName") String cityName,
                                                             @Param("state") String state, @Param("county") String county);

    @Query(value = "SELECT P1.* "
            + " FROM nic_pdb.tbl_prop P1 where P1.property_id != :propertyId and P1.sale_type IN (:saleTypesList) AND P1.city=:cityName AND P1.state_or_province=:state AND P1.postal_code=:zip AND P1.amount BETWEEN :lowerLimitCost AND :upperLimitCost group by P1.property_id ORDER BY P1.quality_score_2 DESC limit 4", nativeQuery = true)
    List<TblePropertyEntitySRPFilter> fetchSearchResultsForPDPByZipAndCost(@Param("propertyId") Integer propertyId,
                                                                 @Param("saleTypesList") List<String> saleTypesList, @Param("cityName") String cityName,
                                                                 @Param("state") String state, @Param("zip") String zip, @Param("upperLimitCost") Integer upperLimitCost, @Param("lowerLimitCost") Integer lowerLimitCost);


    @Query(value = "SELECT P1.* "
            + " FROM nic_pdb.tbl_prop P1 where P1.property_id=:propertyId group by P1.property_id limit 4", nativeQuery = true)
    List<TblePropertyEntitySRPFilter> fetchSearchResultsForPDPPropertyId(@Param("propertyId") Integer propertyId);


}
