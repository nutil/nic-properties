package com.nic.properties.repository;

import com.nic.properties.entity.TBLImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageRepository extends JpaRepository<TBLImageEntity, Integer> {

    @Query(value = "SELECT  image_id, property_id, url, source_url, remote, alt_image FROM nic_pdb.tbl_images where property_id=:id ;", nativeQuery = true)
    List<TBLImageEntity> getImages(@Param(value = "id") Integer id);

}
