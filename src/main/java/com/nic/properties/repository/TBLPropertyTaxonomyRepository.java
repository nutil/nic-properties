package com.nic.properties.repository;

import com.nic.properties.entity.TBLPropertyTaxonomy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TBLPropertyTaxonomyRepository extends JpaRepository<TBLPropertyTaxonomy, Integer> {

    @Query(name = "getSaleTypeFromSaleGroupType")
    List<String> findSaleTypesBySaleGroupType(@Param(value = "saleGroupToBeSearchedFor") String saleGroupToBeSearchedFor);

}
