package com.nic.properties.repository;

import com.nic.properties.entity.Zips;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ZipRepository extends JpaRepository<Zips, Integer> {

	@Query(value = "select zip from nic_pdb.ZIP where fips=:fips limit 1", nativeQuery = true)
	Integer getzips(@Param(value = "fips") Integer fips);

	@Query(value = "select city from nic_pdb.ZIP where zip=:zip limit 1", nativeQuery = true)
	String getcity(@Param(value = "zip") Integer zip);

	@Query(value = "select city from nic_pdb.ZIP where FIPS=:fips group by city", nativeQuery = true)
	List<String> getCityByFips(@Param("fips") Integer fips);

	@Query(value = "select zip  from nic_pdb.ZIP where city=:city limit 1", nativeQuery = true)
	Integer getzipsByCity(@Param(value = "city") String city);

	@Query(value = "select zip  from nic_pdb.ZIP where city=:city", nativeQuery = true)
	List<Integer> getzipsListByCity(@Param(value = "city") String city);

	@Query(value = "select zip  from nic_pdb.ZIP where city=:city and state=:state", nativeQuery = true)
	List<Integer> getzipsListByCityAndState(@Param(value = "city") String city, @Param("state") String state);

	@Query(value = "select state from nic_pdb.ZIP where ZIP =?1 limit 1", nativeQuery = true)
	String getStateByZip(Integer valueOf);


	@Query(value = "select zip  from nic_pdb.ZIP where city=:city and state=:state and fips=:fips", nativeQuery = true)
	List<Integer> getzipsListByCityAndStateAndFips(@Param(value = "city") String city, @Param("state") String state, @Param("fips") Integer fips);
	@Query(value = "select count(city) from nic_pdb.ZIP where city=?1", nativeQuery = true)
	Integer containsCity(String city);
	@Query(value = "select count(ZIP) from nic_pdb.ZIP where ZIP=?1", nativeQuery = true)
	Integer containsZip(Integer zip);
	@Query(value = "select city from nic_pdb.ZIP where zip=:zip", nativeQuery = true)
	List<String> getcityList(Integer zip);

}
