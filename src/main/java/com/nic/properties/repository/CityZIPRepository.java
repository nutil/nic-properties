package com.nic.properties.repository;

import com.nic.properties.entity.ZIPEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface CityZIPRepository extends JpaRepository<ZIPEntity, BigInteger> {

    ZIPEntity findFirstByCityZipCodeEquals(BigInteger cityZipCode);

}
