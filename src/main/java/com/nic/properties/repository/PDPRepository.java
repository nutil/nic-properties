package com.nic.properties.repository;

import com.nic.properties.entity.TBLPropertyEntity;
import com.nic.properties.entity.TblePropertyEntitySRPFilter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PDPRepository extends JpaRepository<TBLPropertyEntity, Long> {

    @Query(value = "select sale_type from nic_pdb.property_taxonomy where sale_type =:saleType OR  main_category_type =:saleType", nativeQuery = true)
    List<String> fetchForeclosureTypes(@Param("saleType") String saleType);

    @Query(value = "SELECT P1.* "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where P1.sale_type=:rentToOwn AND P1.city=:cityName AND P1.state_or_province=:state AND P1.postal_code=:zip AND P1.sale_type=:saletype ORDER BY P1.quality_score_3 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsForPDPRentToOwn(@Param("rentToOwn") String rentToOwn,
                                                              @Param("cityName") String cityName, @Param("state") String state, @Param("zip") String zip, @Param("saletype") String saletype);


    @Query(value = "SELECT P1.* "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where P1.sale_type IN (:foreclosureDetails) AND P1.city=:cityName AND P1.state_or_province=:state ORDER BY P1.quality_score_3 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsForMiniVaforeclosure(
            @Param("foreclosureDetails") List<String> foreclosureDetails, @Param("cityName") String cityName,
            @Param("state") String state);

    @Query(value = "SELECT P1.* "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where P1.sale_type IN (:foreclosureDetails) AND P1.property_id=:propertyId group by P1.property_id limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsForPDPVaforeclosureByPropertyId(
            @Param("foreclosureDetails") List<String> foreclosureDetails, @Param("propertyId") Integer propertyId);

    @Query(value = "SELECT P1.property_id, P1.full_street_name,P1.first_merge_date,P1.sale_type,P1.amount,P1.bedrooms,P1.fixer_upper,P1.baths_total,P1.living_area_square_feet,P1.lot_size,P1.year_built,P1.home_score,P1.investor_score,P1.street_address,P1.city,P1.county,P1.state_or_province,P1.postal_code,P2.source_url,P2.url "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where P1.sale_type=:rentToOwn AND P1.city IN (select search_city from nic_pdb.tbl_nearest_cities where search_city = :cityName AND state_or_province=:state and proximity<=20) AND P1.state_or_province=:state ORDER BY P1.quality_score_3 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsRentToOwn20Miles(@Param("rentToOwn") String rentToOwn,
                                                               @Param("cityName") String cityName, @Param("state") String state);

    @Query(value = "SELECT P1.property_id,P1.first_merge_date,P1.full_street_name,P1.sale_type,P1.amount,P1.bedrooms,P1.fixer_upper,P1.baths_total,P1.living_area_square_feet,P1.lot_size,P1.year_built,P1.home_score,P1.investor_score,P1.street_address,P1.city,P1.county,P1.state_or_province,P1.postal_code,P2.source_url,P2.url "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where P1.sale_type IN (:saleType)  AND P1.state_or_province like %:state% AND P1.city IN (select search_city from nic_pdb.tbl_nearest_cities where search_city = :cityName AND state_or_province=:state AND P1.postal_code=:zip AND P1.sale_type=:saletype and proximity<=20) ORDER BY P1.quality_score_3 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsforPDPForeclosurenext20miles(@Param("saleType") List<String> saleType,
                                                                           @Param("cityName") String cityName, @Param("state") String state, @Param("zip") String zip, @Param("saletype") String saletype);

    @Query(value = "SELECT P1.property_id,P1.first_merge_date,P1.full_street_name,P1.sale_type,P1.amount,P1.bedrooms,P1.fixer_upper,P1.baths_total,P1.living_area_square_feet,P1.lot_size,P1.year_built,P1.home_score,P1.investor_score,P1.street_address,P1.city,P1.county,P1.state_or_province,P1.postal_code,P2.source_url,P2.url "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where P1.sale_type IN (:saleType)  AND P1.state_or_province like %:state% AND P1.city IN (select search_city from nic_pdb.tbl_nearest_cities where search_city = :cityName AND state_or_province=:state AND P1.county=:county AND P1.sale_type=:saletype and proximity<=20) ORDER BY P1.quality_score_3 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsforPDPForeclosureByCountynext20miles(@Param("saleType") List<String> saleType,
                                                                                   @Param("cityName") String cityName, @Param("state") String state, @Param("county") String county, @Param("saletype") String saletype);

    @Query(value = "SELECT P1.property_id,P1.first_merge_date,P1.full_street_name,P1.sale_type,P1.amount,P1.bedrooms,P1.fixer_upper,P1.baths_total,P1.living_area_square_feet,P1.lot_size,P1.year_built,P1.home_score,P1.investor_score,P1.street_address,P1.city,P1.county,P1.state_or_province,P1.postal_code,P2.source_url,P2.url "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where P1.sale_type IN (:saleType)  AND P1.state_or_province like %:state% AND P1.city IN (select search_city from nic_pdb.tbl_nearest_cities where search_city = :cityName AND state_or_province=:state AND P1.county=:county AND P1.amount<=:cost AND P1.sale_type=:saletype and proximity<=20) ORDER BY P1.quality_score_3 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsforPDPForeclosureByCostnext20miles(@Param("saleType") List<String> saleType,
                                                                                 @Param("cityName") String cityName, @Param("state") String state, @Param("county") String county, @Param("saletype") String saletype, @Param("cost") Integer cost);


    @Query(value = "SELECT P1.property_id,P1.first_merge_date,P1.full_street_name, P1.sale_type,P1.amount,P1.bedrooms,P1.fixer_upper,P1.baths_total,P1.living_area_square_feet,P1.lot_size,P1.year_built,P1.home_score,P1.investor_score,P1.street_address,P1.city,P1.county,P1.state_or_province,P1.postal_code,P2.source_url,P2.url "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where P1.sale_type IN (:saleType) AND P1.property_id =:propertyId ORDER BY P1.quality_score_3 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsforPDPForeclosureByPropertyIdnext20miles(@Param("saleType") List<String> saleType,
                                                                                       @Param("propertyId") Integer propertyId);

    @Query(value = "SELECT P1.* "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where P1.sale_type IN (:foreclosureDetails) AND P1.city=:cityName AND P1.state_or_province=:state AND P1.amount<=:cost ORDER BY P1.quality_score_3 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsForMiniVaforeclosureByCost(
            @Param("foreclosureDetails") List<String> foreclosureDetails, @Param("cityName") String cityName,
            @Param("state") String state, @Param("cost") Integer cost);

    @Query(value = "SELECT P1.property_id,P1.first_merge_date,P1.special_financing,P1.sale_type,P1.bargain_price,P1.exclusive,P1.rto_potential, P1.property_record_type,P1.quality_score_3,P1.rto_financing, P1.address_id, P1.amount,P1.bedrooms,P1.fixer_upper,P1.baths_total,P1.living_area_square_feet,P1.lot_size,P1.year_built,P1.home_score,P1.investor_score,P1.street_address,P1.city,P1.county,P1.state_or_province,P1.postal_code,P2.source_url,P2.url "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where P1.state_or_province like %:state% AND P1.city IN (select search_city from nic_pdb.tbl_nearest_cities where search_city = :cityName AND state_or_province=:state AND P1.postal_code=:zip AND P1.sale_type=:saletype and proximity<=20) ORDER BY P1.quality_score_3 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsforPDPBySaleTypeAndCostnext20miles(@Param("cityName") String cityName, @Param("state") String state,
                                                                                 @Param("zip") String zip, @Param("saletype") String saletype);

    @Query(value = "SELECT P1.* "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where P1.property_id != :propertyId and P1.sale_type IN (:saleTypesList) AND P1.city=:cityName AND P1.state_or_province=:state AND P1.postal_code=:zip AND P1.amount BETWEEN :lowerLimitCost AND :upperLimitCost group by P1.property_id ORDER BY P1.quality_score_2 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsForPDPByZipAndCost(@Param("propertyId") Integer propertyId,
                                                                 @Param("saleTypesList") List<String> saleTypesList, @Param("cityName") String cityName,
                                                                 @Param("state") String state, @Param("zip") String zip, @Param("upperLimitCost") Integer upperLimitCost, @Param("lowerLimitCost") Integer lowerLimitCost);

    @Query(value = "SELECT P1.* "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where P1.sale_type IN (:saleTypesList) AND P1.city=:cityName AND P1.state_or_province=:state AND P1.county=:county AND P1.amount<=:cost group by P1.property_id ORDER BY P1.quality_score_2 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsForPDPByCost(
            @Param("saleTypesList") List<String> saleTypesList, @Param("cityName") String cityName,
            @Param("state") String state, @Param("county") String county, @Param("cost") Integer cost);

    @Query(value = "SELECT P1.* "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where P1.property_id != :propertyId and P1.sale_type IN (:saleTypesList) AND P1.city=:cityName AND P1.county=:county AND P1.state_or_province=:state group by P1.property_id ORDER BY P1.quality_score_2 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsForPDPByCounty(@Param("propertyId") String propertyId,
                                                             @Param("saleTypesList") List<String> saleTypesList, @Param("cityName") String cityName,
                                                             @Param("state") String state, @Param("county") String county);

    @Query(value = "SELECT P1.* "
            + " FROM nic_pdb.tbl_prop P1 left join nic_pdb.tbl_images P2 on P1.property_id = P2.property_id where P1.sale_type IN (:saleTypesList) AND P1.city=:cityName AND P1.postal_code=:zip AND P1.state_or_province=:state group by P1.property_id ORDER BY P1.quality_score_2 DESC limit 4", nativeQuery = true)
    List<TBLPropertyEntity> fetchSearchResultsForPDPByZip(
            @Param("saleTypesList") List<String> saleTypesList, @Param("cityName") String cityName,
            @Param("state") String state, @Param("zip") String zip);

    @Query(value = "select property_id from nic_pdb.tbl_prop where address_id=?1", nativeQuery = true)
    Integer getPropertyIdComp(long szID);
    @Query(value = "Select amount_definition from nic_pdb.tbl_prop where property_id =?1", nativeQuery = true)
    String getAmountDefinition(Integer propertyId);
}
