package com.nic.properties.repository;

import com.nic.properties.entity.County;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public interface CountyRepository extends JpaRepository<County, Integer>  {

    @Query(value = "SELECT cnty.fips FROM nic_pdb.CNTY cnty left join nic_pdb.tbl_sum_listings_by_county_sale_type sumlistings on cnty.NAME=sumlistings.county where cnty.NAME=:county and cnty.state=:state and (sumlistings.sale_type ) IN (:saleTypesList) limit 1", nativeQuery = true)
    Integer getfips(@Param("state") String state, @Param("county") String county,
                    @Param("saleTypesList") List<String> saleTypesList);

    @Query(value = "SELECT cnty.fips FROM nic_pdb.CNTY cnty left join nic_pdb.tbl_sum_listings_by_county_sale_type sumlistings on cnty.NAME=sumlistings.county where cnty.state=:state and (sumlistings.sale_type ) IN (:saleTypesList) limit 1", nativeQuery = true)
    Integer getfipsByStateIfNull(@Param("state") String state,
                                 @Param("saleTypesList") List<String> saleTypesList);

    @Query(value = "SELECT cnty.FIPS FROM nic_pdb.ZIP cnty left join nic_pdb.tbl_sum_listings_by_zip_sale_type sumlistings\r\n"
            + " on cnty.ZIP=sumlistings.postal_code\r\n"
            + " where cnty.ZIP=:zip and (sumlistings.sale_type ) IN (:saleTypesList) limit 1", nativeQuery = true)
    Integer getfipByZip(@Param("zip") Integer zip, @Param("saleTypesList") List<String> saleTypesList);

    @Query(value = "SELECT cnty.fips FROM nic_pdb.ZIP cnty \r\n"
            + "left join nic_pdb.tbl_sum_listings_by_city_sale_type sumlistings on cnty.CITY=sumlistings.city \r\n"
            + "where cnty.CITY=:city and cnty.state=:state and (sumlistings.sale_type ) IN (:saleTypesList) limit 1", nativeQuery = true)
    Integer getfipsByCity(@Param("state") String state, @Param("city") String city, @Param("saleTypesList") List<String> saleTypesList);

    @Query(value = "select Distinct(c.NAME) from nic_pdb.CNTY c left join nic_pdb.ZIP z on c.FIPS= z.FIPS where z.CITY=:city limit 1", nativeQuery = true)
    String getCountyByCity(@Param("city") String city);

    @Query(value = "select Distinct(c.NAME) from nic_pdb.CNTY c left join nic_pdb.ZIP z on c.FIPS= z.FIPS where z.ZIP=:zip limit 1", nativeQuery = true)
    String getCountyByZip(@Param("zip") Integer zip);

    @Query(value = "SELECT Distinct FIPS from nic_pdb.CNTY where NAME=?1 limit 1", nativeQuery = true)
    Integer getCountyByName(String county);

    @Query(value = "SELECT count(Name) from nic_pdb.CNTY where Name=?1", nativeQuery = true)
    Integer containsCounty(String county);
    @Query(value = "SELECT count(fips) from nic_pdb.CNTY where fips=?1", nativeQuery = true)
    Integer containsFips(String county);

    @Query(value = "select Distinct(c.NAME) from nic_pdb.CNTY c left join nic_pdb.ZIP z on c.FIPS= z.FIPS where z.CITY=:city ", nativeQuery = true)
    List<String> getCountiesByCity(String city);
    @Query(value="SELECT STATE FROM nic_pdb.CNTY where NAME=?1", nativeQuery = true)
    List<String> getStateByCounty(String county);
    @Query(value = "select Distinct(c.FIPS) from nic_pdb.CNTY c left join nic_pdb.ZIP z on c.FIPS= z.FIPS where z.CITY=:city ", nativeQuery = true)
    List<Integer> getFipsByCity(String city);

    @Query(value="SELECT STATE FROM nic_pdb.CNTY where FIPS=?1", nativeQuery = true)
   List<String> getStateByFips(Integer fips);
}





