package com.nic.properties.repository;

import com.nic.properties.entity.FsboOrMlsProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FsboOrMlsPropertyRepository extends JpaRepository<FsboOrMlsProperty, Integer> {

    @Query(value = "SELECT property_id, sale_type, construction_type, street_address, full_street_name, city, latitude, longitude, description, state_or_province, (amount/living_area_square_feet) as amount, lot_size, total_units, total_rooms,  room_list, stories, parking, parking_spaces, heating, roof_type, year_built,  mls_number, parcel_number, property_record_type, county, legal_description, zoning, exterior_walls, estimated_value, avm_low, avm_high, estimated_rent, avm_confidence, list_date, avm_date FROM nic_pdb.tbl_prop where property_id= :id ", nativeQuery = true)
    FsboOrMlsProperty getPd(@Param("id") Integer id);

}
