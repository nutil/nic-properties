package com.nic.properties.controller;

import com.nic.properties.model.*;
import com.nic.properties.sevice.SRPFilterService;
import com.nic.properties.sevice.SRPFilterServiceWoImages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/v1/")
public class SRPFilterController {
    Logger log = LogManager.getLogger(SRPFilterController.class);
    @Autowired
    private SRPFilterService srpFilterService;
    @Autowired
    private SRPFilterServiceWoImages srpFilterServiceWoImages;
    @GetMapping(path = "/welcome")
    public String getStatus() {
        return "status = up";
    }


    @GetMapping(value = "/filter/", produces = MediaType.APPLICATION_JSON_VALUE)
    public SrpFilterResponse fetchDataResults(SrpFilterRequest srpFilterRequest,
                                              @RequestParam(name = "state", required = false) String state,
                                              @RequestParam(name = "city", required = false) String city,
                                              @RequestParam(name = "county", required = false) String county,
                                              @RequestParam(name = "streetAddress", required = false) String streetAddress,
                                              @RequestParam(name = "postal_code", required = false) String postal_code
    ) {
        log.info("Calling filter api from V1");
        return srpFilterService.fetchDataByFiltering(srpFilterRequest, city, state, county, postal_code, streetAddress);
    }
    @GetMapping(value = "/filterWoImages/", produces = MediaType.APPLICATION_JSON_VALUE)
    public SrpFilterResponseWoImages fetchDataResultsWoImages(SrpFilterRequest srpFilterRequest,
                                                              @RequestParam(name = "state", required = false) String state,
                                                              @RequestParam(name = "city", required = false) String city,
                                                              @RequestParam(name = "county", required = false) String county,
                                                              @RequestParam(name = "streetAddress", required = false) String streetAddress,
                                                              @RequestParam(name = "postal_code", required = false) String postal_code
    ) {
        log.info("Calling filter api from V1");
        return srpFilterServiceWoImages.fetchDataByFilteringWoImages(srpFilterRequest, city, state, county, postal_code, streetAddress);
    }

    @GetMapping(value = "/countyCounts/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> fetchCountyDetails(@RequestParam(name = "state") String state, @RequestParam(name = "saleType") String saleType) {
        if (state == null || state.isEmpty() || state.isBlank() || saleType == null || saleType.isEmpty() || saleType.isBlank()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        log.info("Calling countyCounts api from V1");
        StateCountiesDTO stateCountiesDTO = srpFilterService.fetchCountyResultCounts(state, saleType);
        return new ResponseEntity<StateCountiesDTO>(stateCountiesDTO, HttpStatus.OK);

    }

    @GetMapping(value = "/getPostalCodes/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> fetchPostalDetails(@RequestParam(name = "state", required = false) String state, @RequestParam(name = "saleType", required = false) String saleType) {
        if (state == null || state.isEmpty() || state.isBlank() || saleType == null || saleType.isEmpty() || saleType.isBlank()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        log.info("Calling getPostalCodes api from V1");
        List<PopularNearbyZips> fetchCountyResultCounts = srpFilterService.fetchPostalDetails(state, saleType);
        return new ResponseEntity<List<PopularNearbyZips>>(fetchCountyResultCounts, HttpStatus.OK);

    }
}
