package com.nic.properties.controller;

import com.nic.properties.sevice.PdpNearbyService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PdpNearbyController {
    Logger log = LogManager.getLogger(PdpNearbyController.class);
    @Autowired
    private PdpNearbyService pdpNearbyService;

    @GetMapping(value = "getPdpNear/{state}/{county}/{city}/{zip}/{saleType}")
    public ResponseEntity<Object> getPdpNearby(@PathVariable(name = "state") String state, @PathVariable(name = "county") String county, @PathVariable(name = "city") String city, @PathVariable(name = "zip") Integer zip, @PathVariable(name = "saleType") String saleType) {
        log.info("Calling getPdpNear api ");
        return new ResponseEntity(pdpNearbyService.getPdpNearby(state, county, city, zip, saleType), HttpStatus.OK);
    }
}
