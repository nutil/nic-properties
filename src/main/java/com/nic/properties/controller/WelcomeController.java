package com.nic.properties.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class WelcomeController {

    @GetMapping(path = "/welcome")
    public String getStatus() {
        return "status = up";
    }

}
