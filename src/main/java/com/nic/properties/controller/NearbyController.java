package com.nic.properties.controller;

import com.nic.properties.sevice.NearbyService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class NearbyController {
    Logger log = LogManager.getLogger(NearbyController.class);
    @Autowired
    private NearbyService nearbyService;

    @GetMapping(value = "getNearByCounty/{state}/{county}/{saleType}")
    private ResponseEntity<Object> getNearbyCounties(@PathVariable(name = "state") String state, @PathVariable(name = "county") String county, @PathVariable(name = "saleType") String saleType) {
        log.info("Calling getNearByCounty API ");
        return new ResponseEntity(nearbyService.getNearbyCounties(state, county, saleType), HttpStatus.OK);
    }


    @GetMapping(value = "getNearByCity/{state}/{county}/{city}/{saleType}")
    public ResponseEntity<Object> getNearbyCities(@PathVariable(name = "state") String state, @PathVariable(name = "county") String county, @PathVariable(name = "city") String city, @PathVariable(name = "saleType") String saleType) {
        log.info("Calling getNearByCity API");
        return new ResponseEntity(nearbyService.getNearbyCities(state, county, city, saleType), HttpStatus.OK);
    }


    @GetMapping(value = "getNearByZip/{state}/{county}/{city}/{zip}/{saleType}")
    public ResponseEntity<Object> getNearbyZips(@PathVariable(name = "state") String state, @PathVariable(name = "county") String county, @PathVariable(name = "city") String city, @PathVariable(name = "zip") Integer zip, @PathVariable(name = "saleType") String saleType) {
        log.info("Calling getNearByZip API");
        return new ResponseEntity(nearbyService.getNearbyZips(state, county, city, zip, saleType), HttpStatus.OK);
    }
}
