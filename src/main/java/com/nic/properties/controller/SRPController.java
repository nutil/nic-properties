package com.nic.properties.controller;

import com.nic.properties.model.PropertyDTO;
import com.nic.properties.model.TblePropertyModelSRPFilter;
import com.nic.properties.sevice.PropertyService;
import com.nic.properties.sevice.SrpService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(path = "/srp/")
public class SRPController {

    private final PropertyService propertyService;
    Logger log = LogManager.getLogger(SRPController.class);
    @Autowired
    private SrpService srpService;

    public SRPController(PropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @GetMapping(path = "/welcome")
    public String getStatus() {
        return "status = up";
    }


    @GetMapping(value = "/fetchSearchResultsForMiniSRP/{cityName}/{state}", produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity<List<TblePropertyModelSRPFilter>> fetchSearchResultsForMiniSRP(@PathVariable("cityName") String cityName,
                                                                                         @PathVariable("state") String state) {
        if (cityName == null || cityName.isEmpty() || cityName.isBlank() || state == null || state.isEmpty() || state.isBlank()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<TblePropertyModelSRPFilter> fetchSearchResultsForMiniSRP = srpService.fetchSearchResultsForMiniSRP(cityName, state);
        log.info("Calling fetchSearchResultsForMiniSRP API ");
        return new ResponseEntity(fetchSearchResultsForMiniSRP, HttpStatus.OK);

    }

    @GetMapping(value = "/getSearchResultsForMiniSRPForCustomSaleType/{state}/{cityName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PropertyDTO>> getPropertiesByStateCityAndSaleTypeForMiniSRP(@PathVariable("cityName") String cityName,
                                                                                           @PathVariable("state") String state,
                                                                                           @RequestParam("saleTypes") List<String> saleTypes) {
        if (cityName == null ||  cityName.isEmpty() || cityName.isBlank() ||
                state == null || state.isEmpty() || state.isBlank() ||
                saleTypes == null || saleTypes.isEmpty()
        ) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<PropertyDTO> propertyDTOS  = this.propertyService.findByStateAndCityAndMultipleSaleTypesForMiniSRP(state,  cityName, saleTypes);
        return new ResponseEntity<>(propertyDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/searchAddressSubmit")
    public ResponseEntity<?> searchByAddress(@RequestParam("id") String propertyId, @RequestParam("address") String address) {
        log.info("Calling searchAddressSubmit API");
        if (
                propertyId == null ||
                        propertyId.isEmpty() ||
                        propertyId.isBlank() ||
                        address == null ||
                        address.isEmpty() ||
                        address.isBlank() ||
                        address.length() < 5) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        List<String> zipSearchKeywordList = Arrays.asList(address.split(","));
        //Get the size of the list
        int zipSearchKeywordListSize = zipSearchKeywordList.size() - 1;
        List<String> zipCodeAndStateCode = Arrays.asList(zipSearchKeywordList.get(zipSearchKeywordListSize).split(" "));
        int zipCodeAndStateCodeLength = zipCodeAndStateCode.size();
        String zipCode = zipCodeAndStateCode.get(zipCodeAndStateCodeLength - 1).trim();
        if (propertyId.length() > 19 || !Pattern.matches("^[0-9]+$", propertyId)) {
            return new ResponseEntity("Invalid Property Id", HttpStatus.BAD_REQUEST);
        }
        if (zipCode.length() > 5 || zipCode.length() < 4 || !Pattern.matches("^[0-9]+$", zipCode) || Pattern.matches("^[0]{5}$", zipCode)) {
            return new ResponseEntity("Invalid Zip Code", HttpStatus.BAD_REQUEST);
        }
        Object result = this.srpService.getSearchResultsForAddressUsingPropertyIDAndAddress(propertyId, address);
        if (result == null) {
            return new ResponseEntity("Invalid Request", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(result, HttpStatus.OK);
    }

}
