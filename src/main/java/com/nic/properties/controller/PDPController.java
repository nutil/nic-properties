package com.nic.properties.controller;

import com.nic.properties.entity.SmartzipCompsResult;
import com.nic.properties.entity.TBLPropertyAssociatesEntity;
import com.nic.properties.entity.ValueHistoryEntity;
import com.nic.properties.model.MiniSRPPropertyModel;
import com.nic.properties.model.TBLPropertyModel;
import com.nic.properties.model.TblePropertyModelSRPFilter;
import com.nic.properties.sevice.PdpService;
import com.nic.properties.sevice.SmartZipService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(path = "/pdp/")
public class PDPController {

    Logger log = LogManager.getLogger(PdpNearbyController.class);
    @Autowired
    private SmartZipService smartZipService;
    @Autowired
    private PdpService pdpService;

    @GetMapping(value = "/fetchSearchResultsForPDPByZip/{cityName}/{state}/{zip}/{saletype}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> fetchSearchResultsForPDPByZip(@PathVariable("cityName") String cityName,
                                                           @PathVariable("state") String state, @PathVariable("zip") String zip,
                                                           @PathVariable("saletype") String saletype) {
        log.info("Calling fetchSearchResultsForPDPByZip API");
        List<TBLPropertyModel> fetchSearchResultsForPDPByZip = pdpService.fetchSearchResultsForPDPByZip(cityName, state,
                zip, saletype);

        return new ResponseEntity(fetchSearchResultsForPDPByZip, HttpStatus.OK);

    }

    @GetMapping(value = "/fetchSearchResultsForPDPByZipAndByCost/{propertyId}/{cityName}/{state}/{zip}/{saletype}/{cost}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> fetchSearchResultsForPDPByZipAndByCost(@PathVariable("propertyId") Integer propertyId, @PathVariable("cityName") String cityName,
                                                                    @PathVariable("state") String state, @PathVariable("zip") String zip,
                                                                    @PathVariable("saletype") String saletype, @PathVariable("cost") Integer cost) {
        List<TblePropertyModelSRPFilter> fetchSearchResultsForPDPByZipAndCost = pdpService
                .fetchSearchResultsForPDPByZipAndCost(propertyId, cityName, state, zip, saletype, cost);
        log.info("Calling fetchSearchResultsForPDPByZipAndByCost API");
        return new ResponseEntity(fetchSearchResultsForPDPByZipAndCost, HttpStatus.OK);

    }

    @GetMapping(value = "/fetchSearchResultsForPDPByCounty/{propertyId}/{cityName}/{state}/{county}/{saletype}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> fetchSearchResultsForPDPByCounty(@PathVariable("propertyId") String propertyId, @PathVariable("cityName") String cityName,
                                                              @PathVariable("state") String state, @PathVariable("county") String county,
                                                              @PathVariable("saletype") String saletype) {
        List<TblePropertyModelSRPFilter> fetchSearchResultsForPDPByCounty = pdpService.fetchSearchResultsForPDPByCounty(propertyId, cityName,
                state, county, saletype);
        log.info("Calling fetchSearchResultsForPDPByCounty API");
        return new ResponseEntity(fetchSearchResultsForPDPByCounty, HttpStatus.OK);
    }

    @GetMapping(value = "/fetchSearchResultsForPDPByCost/{cityName}/{state}/{county}/{saletype}/{cost}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> fetchSearchResultsForPDPByCost(@PathVariable("cityName") String cityName,
                                                            @PathVariable("state") String state, @PathVariable("county") String county,
                                                            @PathVariable("saletype") String saletype, @PathVariable("cost") Integer cost) {
        List<TBLPropertyModel> fetchSearchResultsForPDPByCost = pdpService.fetchSearchResultsForPDPByCost(cityName,
                state, county, saletype, cost);
        log.info("Calling fetchSearchResultsForPDPByCost API");
        return new ResponseEntity(fetchSearchResultsForPDPByCost, HttpStatus.OK);
    }

    @GetMapping(value = "/fetchSearchResultsForPDPByPropertyId/{propertyId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> fetchSearchResultsForPDPByPropertyId(@PathVariable("propertyId") String propertyId) {

        List<MiniSRPPropertyModel> resultsForPDPByPropertyId = pdpService.fetchSearchResultsForPDPByPropertyId(propertyId);
        log.info("Calling fetchSearchResultsForPDPByPropertyId API");
        return new ResponseEntity(resultsForPDPByPropertyId, HttpStatus.OK);
    }

    @GetMapping(value = "/fetchSearchResultsForPDPByNextPropertyId/{propertyId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> fetchSearchResultsForPDPByNextPropertyId(@PathVariable("propertyId") String propertyId) {
        String nextPropertyId = propertyId + "1";
        List<MiniSRPPropertyModel> fetchSearchResultsForPDPByCounty = pdpService.fetchSearchResultsForPDPByPropertyId(nextPropertyId);
        log.info("Calling fetchSearchResultsForPDPByNextPropertyId API");
        return new ResponseEntity(fetchSearchResultsForPDPByCounty, HttpStatus.OK);
    }

    @GetMapping(value = "/getEstimatedValueHistory/{smartZipId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getEstimatedValueHistory(@PathVariable("smartZipId") String smartZipId) {
        log.info("Calling getEstimatedValueHistory API");
        List<ValueHistoryEntity> estimatedValueHistory = pdpService.findEstimatedValueHistory(smartZipId);
        return new ResponseEntity(estimatedValueHistory, HttpStatus.OK);
    }

    @GetMapping(value = "/getAssociates/{propertyId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAssociates(@PathVariable("propertyId") Integer propertyId) {
        log.info("Calling getAssociates API");
        List<TBLPropertyAssociatesEntity> associates = pdpService.findAssociatesForPDP(propertyId);
        return new ResponseEntity(associates, HttpStatus.OK);
    }

    @GetMapping(value = "/getSmartzipComparables/{smartZipId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getSmartzipComparables(@PathVariable("smartZipId") String propertyId) {
        log.info("Calling getSmartzipComparables API");
        SmartzipCompsResult smartZipResultsById = smartZipService.findSmartzipComparables(propertyId);
        return new ResponseEntity(smartZipResultsById, HttpStatus.OK);
    }

    @GetMapping(value = "/getPdpforPropertyId/{propertyId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Object>> getPdpDetail(@PathVariable Integer propertyId) throws Exception {
        log.info("Calling getPdpforPropertyId API");
        ArrayList<Object> listObject = new ArrayList();
        Object object = pdpService.getPdp(propertyId);
        listObject.add(object);
        listObject.add(pdpService.updateImageHexaValue(propertyId));
        if (propertyId >= 0 && propertyId != null) {
            return listObject.equals(null) ? new ResponseEntity("No Data Found", HttpStatus.NOT_FOUND)
                    : new ResponseEntity(listObject, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
