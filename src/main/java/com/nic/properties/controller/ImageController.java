package com.nic.properties.controller;

import com.nic.properties.model.TBLPropertyModel;
import com.nic.properties.repository.ImagesRepository;
import com.nic.properties.sevice.ImageService;
import com.nic.properties.util.ImageUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ImageController {
    Logger log = LogManager.getLogger(ImageController.class);
    @Autowired
    ImageService imageService;

    @GetMapping(value = "/getImagesByPid/{propertyId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getImagesByPid(@PathVariable("propertyId") Integer propertyId) {
        log.info("Calling getImagesByPid API");
        return new ResponseEntity(imageService.getImagesByPid(propertyId), HttpStatus.OK);
    }


}