package com.nic.properties.srpenum;

public enum ResultTypeENUM {

    PDP_SRP("PDPSRP"),
    COUNTY_SRP("COUNTYSRP"),
    CITY_SRP("CITYSRP"),
    ZIP_SRP("ZIPSRP");
    private final String resultTypeENUM;

    ResultTypeENUM(String resultTypeENUM) {
        this.resultTypeENUM = resultTypeENUM;
    }

}
