package com.nic.properties.srpenum;

public enum ImageSizeEnum {

    thumbnail,
    small,
    medium2,
    original,

}
