package com.nic.properties.srpenum;

import java.util.Arrays;

public enum SortEnum {
    BESTMATCH("qualityScore"),
    NEWHOMES("firstMergeDate Desc"),
    BEDS("bedrooms Desc"),
    BATHS("bathsTotal Desc"),
    PRICELOWESTFIRST("amount asc"),
    PRICEHIGHESTFIRST("amount Desc"),
    SQFTLOWESTFIRST("livingAreaSquareFeet Asc"),
    SQFTHIGHESTFIRST("livingAreaSquareFeet Desc"),
    YEARBUILT("YearBuilt Desc"),
    AUCTIONDATE("auctionDate Desc");

    public final String value;
    SortEnum(String value) {
        this.value = value;
    }



}
