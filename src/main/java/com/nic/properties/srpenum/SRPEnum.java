package com.nic.properties.srpenum;

public enum SRPEnum {
    SALE("sale"), ForeclosuresRS2("ForeclosuresRS2"), PreForeclosures("PreForeclosures"), BANK_OWNED_REO("REOForeclosure"),
    HUD_HOMES("HUDForeclosure"), SHORT_SALES("ShortSale"), HOUSE_AUCTIONS("Auction"), RENT_TO_OWN("RentToOwn"),
    HOUSE_FOR_RENT("Rental"), FOR_SALE("ResaleMLS"), FANNIEMAEORFREDDIEMAC("FannieMaeOrFreddieMac"), FSBO("FSBO"), TAXLEIN("TaxLien"),
    VAFORECLOSURE("VAForeclosure"), PREFORECLOSURENOD("PreForeclosureNOD"), PREFORECLOSURELISPENDENS("PreForeclosureLisPendens"),
    BANKFORECLOSURES("BankForeclosures"), HUDORGOVTFORECLOSURES("HUDOrGovtForeclosures"), TAXSALES("TaxSales"),
    OWNERFINANCING("OwnerFinancing"), HUDHOMESFORECLOSURERS2("HUDHomesForeclosureRS2");

    public final String value;

    SRPEnum(String value) {
        this.value = value;
    }

    public static SRPEnum valueOfLabel(String value) {
        for (SRPEnum e : values()) {
            if (e.value.equals(value)) {
                return e;
            }
        }
        return null;
    }
    
}