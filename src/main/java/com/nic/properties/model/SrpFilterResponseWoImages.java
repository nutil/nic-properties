package com.nic.properties.model;

import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SrpFilterResponseWoImages implements Serializable {


	private List<TblePropertyModelSRPFilter> propertyList;
	private List<TblePropertyModelSRPFilter> milesPropertyList;
	private List<TblePropertyModelSRPFilter> subtypesPropertyList;
	private Long count;
	private Long milesCount;
	private Long subTypesCount;
	private Boolean locationValid;
	private List<NextProperties> nextPropertyIDs;
private HttpStatus httpStatus;
	public SrpFilterResponseWoImages() {
	}

	public SrpFilterResponseWoImages(List<TblePropertyModelSRPFilter> propertyList, List<TblePropertyModelSRPFilter> milesPropertyList,
                                     List<TblePropertyModelSRPFilter> subtypesPropertyList, Long count, Long milesCount, Long subTypesCount,
			 HttpStatus httpStatus, Boolean locationValid, List<NextProperties> nextPropertyIDs) {
		super();
		this.propertyList = propertyList;
		this.milesPropertyList = milesPropertyList;
		this.subtypesPropertyList = subtypesPropertyList;
		this.count = count;
		this.milesCount = milesCount;
		this.subTypesCount = subTypesCount;
		this.httpStatus = httpStatus;
		this.locationValid = locationValid;
		this.nextPropertyIDs = nextPropertyIDs;
	}

	public List<TblePropertyModelSRPFilter> getPropertyList() {
		return propertyList;
	}

	public void setPropertyList(List<TblePropertyModelSRPFilter> propertyList) {
		this.propertyList = propertyList;
	}

	public List<TblePropertyModelSRPFilter> getMilesPropertyList() {
		return milesPropertyList;
	}

	public void setMilesPropertyList(List<TblePropertyModelSRPFilter> milesPropertyList) {
		if(milesPropertyList instanceof Serializable)
			this.milesPropertyList = milesPropertyList;
		else
			this.milesPropertyList = new ArrayList<>(milesPropertyList);
	}

	public List<TblePropertyModelSRPFilter> getSubtypesPropertyList() {
		return subtypesPropertyList;
	}

	public void setSubtypesPropertyList(List<TblePropertyModelSRPFilter> subtypesPropertyList) {
		this.subtypesPropertyList = subtypesPropertyList;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long getMilesCount() {
		return milesCount;
	}

	public void setMilesCount(Long milesCount) {
		this.milesCount = milesCount;
	}

	public Long getSubTypesCount() {
		return subTypesCount;
	}

	public void setSubTypesCount(Long subTypesCount) {
		this.subTypesCount = subTypesCount;
	}

 public HttpStatus getHttpStatus() {
          return httpStatus;
      }

      public void setHttpStatus(HttpStatus httpStatus) {
          this.httpStatus = httpStatus;
      }


	public Boolean getLocationValid() {
		return locationValid;
	}

	public void setLocationValid(Boolean locationValid) {
		this.locationValid = locationValid;
	}

	public List<NextProperties> getNextPropertyIDs() {
		return nextPropertyIDs;
	}

	public void setNextPropertyIDs(List<NextProperties> nextPropertyIDs) {
		this.nextPropertyIDs = nextPropertyIDs;
	}






}
