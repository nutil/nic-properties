package com.nic.properties.model;

import java.io.Serializable;

public class TBLImageModel implements Serializable {
    private Integer imageId;
    private String url;
    private String sourceUrl;
    private Boolean remote;
    private String altImage;
    private String urlLarge;
    private String urlHex;
    private Boolean primary;


    public TBLImageModel() {
    }


    public TBLImageModel(Integer imageId, String url, String sourceUrl, Boolean remote, String altImage, String urlLarge, String urlHex, Boolean primary) {
        this.imageId = imageId;
        this.url = url;
        this.sourceUrl = sourceUrl;
        this.remote = remote;
        this.altImage = altImage;
        this.urlLarge = urlLarge;
        this.urlHex = urlHex;
        this.primary = primary;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public Boolean getRemote() {
        return remote;
    }

    public void setRemote(Boolean remote) {
        this.remote = remote;
    }

    public String getAltImage() {
        return altImage;
    }

    public void setAltImage(String altImage) {
        this.altImage = altImage;
    }

    public String getUrlLarge() {
        return urlLarge;
    }

    public void setUrlLarge(String urlLarge) {
        this.urlLarge = urlLarge;
    }

    public String getUrlHex() {
        return urlHex;
    }

    public void setUrlHex(String urlHex) {
        this.urlHex = urlHex;
    }

    public Boolean getPrimary() {
        return primary;
    }

    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }
}
