package com.nic.properties.model;

import java.util.List;
import java.util.Map;

public class StateCountiesDTO implements java.io.Serializable{

    Boolean isStateHavingCounties;
    Map<String, List<CountyResultModel>> counties;

    public StateCountiesDTO() {
    }

    public StateCountiesDTO(Boolean isStateHavingCounties, Map<String, List<CountyResultModel>> counties) {
        this.isStateHavingCounties = isStateHavingCounties;
        this.counties = counties;
    }

    public Boolean getStateHavingCounties() {
        return isStateHavingCounties;
    }

    public void setStateHavingCounties(Boolean stateHavingCounties) {
        isStateHavingCounties = stateHavingCounties;
    }

    public Map<String, List<CountyResultModel>> getCounties() {
        return counties;
    }

    public void setCounties(Map<String, List<CountyResultModel>> counties) {
        this.counties = counties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StateCountiesDTO)) return false;

        StateCountiesDTO that = (StateCountiesDTO) o;

        if (isStateHavingCounties != null ? !isStateHavingCounties.equals(that.isStateHavingCounties) : that.isStateHavingCounties != null)
            return false;
        return getCounties() != null ? getCounties().equals(that.getCounties()) : that.getCounties() == null;
    }

    @Override
    public int hashCode() {
        int result = isStateHavingCounties != null ? isStateHavingCounties.hashCode() : 0;
        result = 31 * result + (getCounties() != null ? getCounties().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "StateCountiesDTO{" +
                "isStateHavingCounties=" + isStateHavingCounties +
                ", counties=" + counties +
                '}';
    }
}
