package com.nic.properties.model;

import com.nic.properties.srpenum.ResultTypeENUM;

import java.io.Serializable;

public class BaseSRPResponseDTO implements Serializable {
    protected ResultTypeENUM resultType;
    protected String stateCode;

    public BaseSRPResponseDTO() {
    }

    public BaseSRPResponseDTO(ResultTypeENUM resultType, String stateCode) {
        this.resultType = resultType;
        this.stateCode = stateCode;
    }

    public ResultTypeENUM getResultType() {
        return resultType;
    }

    public void setResultType(ResultTypeENUM resultType) {
        this.resultType = resultType;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }
}
