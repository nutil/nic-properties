package com.nic.properties.model;

import lombok.*;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Component
public class NextProperties implements Serializable {
    private Integer propertyId;
    private String streetAddress;
    private String city;
    private String county;
    private String stateOrProvince;
    private String postal_code;

}
