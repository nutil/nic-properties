package com.nic.properties.model;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class TBLPropertyModel implements Serializable {


    private Integer propertyId;
    private Date firstMergeDate;
    private String saleType;
    private Integer amount;
    private Integer bedrooms;
    private Boolean fixerUpper;
    private BigDecimal bathsTotal;
    private Integer livingAreaSquareFeet;
    private String lotSize;
    private Integer homeScore;
    private Integer investorScore;
    private String streetAddress;
    private String city;
    private String county;
    private String stateOrProvince;
    private Integer postal_code;
    /*private Integer qualityScore1;*/
    private Integer qualityScore2;
    /*private Integer qualityScore3;
    private Integer qualityScore4;
    private Integer qualityScore5;
    private Integer qualityScore6;
    private Integer qualityScore7;
    private Integer qualityScore8;
    private Integer qualityScore9;
    private Integer qualityScore10;*/
    private String propertyRecordType;
    private Boolean bargainPrice;
    private Boolean exclusive;
    private Boolean rtoPotential;
    private Boolean specialFinancing;
    private Boolean rtoFinancing;
    private Date YearBuilt;
    private Long addressId;
    private List<TBLImageModel> imageEntity = new ArrayList<>();
    private String streetName;
    private Integer pricePerSqft;
    private String streetNumber;
    private String yearBuiltDate;
    private String year;
    private String primaryImage;
    private String amountDefinition;

}
