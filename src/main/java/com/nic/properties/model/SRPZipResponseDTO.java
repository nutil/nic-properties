package com.nic.properties.model;

import com.nic.properties.srpenum.ResultTypeENUM;

import java.io.Serializable;

public class SRPZipResponseDTO implements Serializable {
    private ResultTypeENUM resultType;
    private String stateCode;
    private String countyName;
    private String cityName;
    private String zipCode;

    public SRPZipResponseDTO() {
    }

    public SRPZipResponseDTO(ResultTypeENUM resultType, String stateCode, String countyName, String cityName, String zipCode) {
        this.resultType = resultType;
        this.stateCode = stateCode;
        this.countyName = countyName;
        this.cityName = cityName;
        this.zipCode = zipCode;
    }

    public ResultTypeENUM getResultType() {
        return resultType;
    }

    public void setResultType(ResultTypeENUM resultType) {
        this.resultType = resultType;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SRPZipResponseDTO)) return false;

        SRPZipResponseDTO that = (SRPZipResponseDTO) o;

        if (getResultType() != that.getResultType()) return false;
        if (getStateCode() != null ? !getStateCode().equals(that.getStateCode()) : that.getStateCode() != null)
            return false;
        if (getCountyName() != null ? !getCountyName().equals(that.getCountyName()) : that.getCountyName() != null)
            return false;
        if (getCityName() != null ? !getCityName().equals(that.getCityName()) : that.getCityName() != null)
            return false;
        return getZipCode() != null ? getZipCode().equals(that.getZipCode()) : that.getZipCode() == null;
    }

    @Override
    public int hashCode() {
        int result = getResultType() != null ? getResultType().hashCode() : 0;
        result = 31 * result + (getStateCode() != null ? getStateCode().hashCode() : 0);
        result = 31 * result + (getCountyName() != null ? getCountyName().hashCode() : 0);
        result = 31 * result + (getCityName() != null ? getCityName().hashCode() : 0);
        result = 31 * result + (getZipCode() != null ? getZipCode().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SRPZipResponseDTO{" +
                "resultType=" + resultType +
                ", stateCode='" + stateCode + '\'' +
                ", countyName='" + countyName + '\'' +
                ", cityName='" + cityName + '\'' +
                ", zipCode='" + zipCode + '\'' +
                '}';
    }
}
