package com.nic.properties.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class TblePropertyModelSRPFilter implements Serializable {

	private Integer propertyId;
	private java.sql.Date firstMergeDate;
	private String saleType;
	private Integer amount;
	private Integer bedrooms;
	private Boolean fixerUpper;
	private BigDecimal bathsTotal;
	private Integer livingAreaSquareFeet;
	private String lotSize;
	private Integer homeScore;
	private Integer investorScore;
	private String streetAddress;
	private String city;
	private String county;
	private String stateOrProvince;

	private String postal_code;

	private Integer qualityScore2;

	private String propertyRecordType;
	private Boolean bargainPrice;
	private Boolean exclusive;
	private Boolean rtoPotential;
	private Boolean specialFinancing;
	private Boolean rtoFinancing;
	private Date YearBuilt;
	private Long addressId;
	private String urlHex;
	private String streetName;
	private Integer pricePerSqft;
	private String streetNumber;
	private String yearBuiltDate;
	private String year;
	private String amountDefinition;
	private java.util.Date auctionDate;

	private Integer photoCount;


}
	
	
	

