package com.nic.properties.model;

import java.math.BigDecimal;
import java.util.List;

public class SrpFilterRequest {

    private List<String> homeTypes;
    private List<String> saleTypes;
    private Integer amount;
    private Integer maxAmount;
    private Integer bedRooms;
    private BigDecimal bathsTotal;
    private Integer livingSquareFeet;
    private Integer maxLivingSquareFeet;
    private String lotSize;
    private String maxLotSize;
    private Integer qualityScore;
    private List<String> specialOption;
    private String year;
    private String maxYear;
    private Boolean image;
    private Integer pageIndex;
    private Integer sqftPrice;
    private Integer maxSqftPrice;
    private String sortBy;

    public SrpFilterRequest(List<String> homeTypes, List<String> saleTypes, Integer amount, Integer bedRooms,
                            BigDecimal bathsTotal, Integer livingSquareFeet, String lotSize, Integer qualityScore,
                            List<String> specialOption, String year, Boolean image, String sortBy) {
        super();
        this.homeTypes = homeTypes;
        this.saleTypes = saleTypes;
        this.amount = amount;
        this.bedRooms = bedRooms;
        this.bathsTotal = bathsTotal;
        this.livingSquareFeet = livingSquareFeet;
        this.lotSize = lotSize;
        this.qualityScore = qualityScore;
        this.specialOption = specialOption;
        this.year = year;
        this.image = image;
        this.sortBy= sortBy;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }
    public List<String> getHomeTypes() {
        return homeTypes;
    }

    public void setHomeTypes(List<String> homeTypes) {
        this.homeTypes = homeTypes;
    }

    public List<String> getSaleTypes() {
        return saleTypes;
    }

    public void setSaleTypes(List<String> saleTypes) {
        this.saleTypes = saleTypes;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getBedRooms() {
        return bedRooms;
    }

    public void setBedRooms(Integer bedRooms) {
        this.bedRooms = bedRooms;
    }

    public BigDecimal getBathsTotal() {
        return bathsTotal;
    }

    public void setBathsTotal(BigDecimal bathsTotal) {
        this.bathsTotal = bathsTotal;
    }

    public Integer getLivingSquareFeet() {
        return livingSquareFeet;
    }

    public void setLivingSquareFeet(Integer livingSquareFeet) {
        this.livingSquareFeet = livingSquareFeet;
    }

    public String getLotSize() {
        return lotSize;
    }

    public void setLotSize(String lotSize) {
        this.lotSize = lotSize;
    }

    public Integer getQualityScore() {
        return qualityScore;
    }

    public void setQualityScore(Integer qualityScore) {
        this.qualityScore = qualityScore;
    }

    public List<String> getSpecialOption() {
        return specialOption;
    }

    public void setSpecialOption(List<String> specialOption) {
        this.specialOption = specialOption;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Boolean getImage() {
        return image;
    }

    public void setImage(Boolean image) {
        this.image = image;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(Integer maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Integer getMaxLivingSquareFeet() {
        return maxLivingSquareFeet;
    }

    public void setMaxLivingSquareFeet(Integer livingSquareFeet) {
        this.maxLivingSquareFeet = livingSquareFeet;
    }

    public String getMaxLotSize() {
        return maxLotSize;
    }

    public void setMaxLotSize(String lotSize) {
        this.maxLotSize = lotSize;
    }

    public String getMaxYear() {
        return maxYear;
    }

    public void setMaxYear(String year) {
        this.maxYear = year;
    }

    public Integer getSqftPrice() {
        return sqftPrice;
    }

    public void setSqftPrice(Integer sqftPrice) {
        this.sqftPrice = sqftPrice;
    }

    public Integer getMaxSqftPrice() {
        return maxSqftPrice;
    }

    public void setMaxSqftPrice(Integer maxSqftPrice) {
        this.maxSqftPrice = maxSqftPrice;
    }

    @Override
    public String toString() {
        return "SrpFilterRequest " +","+homeTypes+","+saleTypes
                +","+amount
                +","+maxAmount
                +","+bedRooms   +","+bathsTotal
                +","+livingSquareFeet
                +","+maxLivingSquareFeet
                +","+lotSize +","+'\''
                +","+maxLotSize +","+'\'' +","+qualityScore
                +","+specialOption
                +","+year +","+'\''
                +","+maxYear +","+'\''
                +","+image
                +","+pageIndex
                +","+sqftPrice
                +","+maxSqftPrice
                +","+sortBy
                ;

    }
}
