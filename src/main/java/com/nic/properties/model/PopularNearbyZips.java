package com.nic.properties.model;

import java.io.Serializable;

public class PopularNearbyZips implements Serializable {

    String county;
    String city;
    String zipCode;

    public PopularNearbyZips() {
    }

    public PopularNearbyZips(String county, String city, String zipCode) {
        this.county = county;
        this.city = city;
        this.zipCode = zipCode;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PopularNearbyZips)) return false;

        PopularNearbyZips that = (PopularNearbyZips) o;

        if (getCounty() != null ? !getCounty().equals(that.getCounty()) : that.getCounty() != null) return false;
        if (getCity() != null ? !getCity().equals(that.getCity()) : that.getCity() != null) return false;
        return getZipCode() != null ? getZipCode().equals(that.getZipCode()) : that.getZipCode() == null;
    }

    @Override
    public int hashCode() {
        int result = getCounty() != null ? getCounty().hashCode() : 0;
        result = 31 * result + (getCity() != null ? getCity().hashCode() : 0);
        result = 31 * result + (getZipCode() != null ? getZipCode().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PopularNearbyZips{" +
                "county='" + county + '\'' +
                ", city='" + city + '\'' +
                ", zipCode='" + zipCode + '\'' +
                '}';
    }
}
