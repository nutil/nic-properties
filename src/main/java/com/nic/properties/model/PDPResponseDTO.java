package com.nic.properties.model;

import com.nic.properties.srpenum.ResultTypeENUM;

import java.io.Serializable;
import java.math.BigInteger;

public class PDPResponseDTO extends BaseSRPResponseDTO implements Serializable {
    String countyName;
    String cityName;
    String streetAddress;
    String zipCode;
    BigInteger propertyId;

    public PDPResponseDTO() {
    }

    public PDPResponseDTO(ResultTypeENUM resultType, String stateCode, String countyName, String cityName, String streetAddress, String zipCode, BigInteger propertyId) {
        super(resultType, stateCode);
        this.countyName = countyName;
        this.cityName = cityName;
        this.streetAddress = streetAddress;
        this.zipCode = zipCode;
        this.propertyId = propertyId;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public BigInteger getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(BigInteger propertyId) {
        this.propertyId = propertyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PDPResponseDTO)) return false;

        PDPResponseDTO that = (PDPResponseDTO) o;

        if (getCountyName() != null ? !getCountyName().equals(that.getCountyName()) : that.getCountyName() != null)
            return false;
        if (getCityName() != null ? !getCityName().equals(that.getCityName()) : that.getCityName() != null)
            return false;
        if (getStreetAddress() != null ? !getStreetAddress().equals(that.getStreetAddress()) : that.getStreetAddress() != null)
            return false;
        if (getZipCode() != null ? !getZipCode().equals(that.getZipCode()) : that.getZipCode() != null) return false;
        return getPropertyId() != null ? getPropertyId().equals(that.getPropertyId()) : that.getPropertyId() == null;
    }

    @Override
    public int hashCode() {
        int result = getCountyName() != null ? getCountyName().hashCode() : 0;
        result = 31 * result + (getCityName() != null ? getCityName().hashCode() : 0);
        result = 31 * result + (getStreetAddress() != null ? getStreetAddress().hashCode() : 0);
        result = 31 * result + (getZipCode() != null ? getZipCode().hashCode() : 0);
        result = 31 * result + (getPropertyId() != null ? getPropertyId().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PDPResponseDTO{" +
                "resultType=" + resultType +
                ", stateCode='" + stateCode + '\'' +
                ", countyName='" + countyName + '\'' +
                ", cityName='" + cityName + '\'' +
                ", streetAddress='" + streetAddress + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", propertyId=" + propertyId +
                '}';
    }
}
