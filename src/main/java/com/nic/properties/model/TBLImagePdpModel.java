package com.nic.properties.model;

public class TBLImagePdpModel {
    private Integer imageId;
    private Integer propertyId;
    private String url;
    private String sourceUrl;
    private Boolean remote;
    private String altImage;

    public TBLImagePdpModel(Integer imageId, Integer propertyId, String url, String sourceUrl, Boolean remote, String altImage) {
        super();
        this.imageId = imageId;
        this.propertyId = propertyId;
        this.url = url;
        this.sourceUrl = sourceUrl;
        this.remote = remote;
        this.altImage = altImage;

    }

    public TBLImagePdpModel() {
    }

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public Boolean getRemote() {
        return remote;
    }

    public void setRemote(Boolean remote) {
        this.remote = remote;
    }

    public String getAltImage() {
        return altImage;
    }

    public void setAltImage(String altImage) {
        this.altImage = altImage;
    }

}
