package com.nic.properties.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

public class PropertyDTO implements java.io.Serializable {
    private Long propertyId;
    private Timestamp firstMergeDate;
    private String saleType;
    private Integer amount;
    private Integer bedrooms;
    private Boolean fixerUpper;
    private BigDecimal bathsTotal;
    private Integer livingAreaSquareFeet;
    private String lotSize;
    private Integer homeScore;
    private Integer investorScore;
    private String streetAddress;
    private String city;
    private String county;
    private String stateOrProvince;
    private String postal_code;
    private Integer qualityScore2;
    private String propertyRecordType;
    private Boolean bargainPrice;
    private Boolean exclusive;
    private Boolean rtoPotential;
    private Boolean specialFinancing;
    private Boolean rtoFinancing;
    private Date YearBuilt;
    private Long addressId;
    private String fullStreetName;
    private String urlHex;
    private String amountDefinition;
    private Integer pricePerSqft;
    private String yearBuiltDate;
    private String year;
    private String streetNumber;

    public PropertyDTO() {
    }

    public PropertyDTO(
            Long propertyId,
            Timestamp firstMergeDate,
            String saleType,
            Integer amount,
            Integer bedrooms,
            Boolean fixerUpper,
            BigDecimal bathsTotal,
            Integer livingAreaSquareFeet,
            String lotSize,
            Integer homeScore,
            Integer investorScore,
            String streetAddress,
            String city,
            String county,
            String stateOrProvince,
            String postal_code,
            Integer qualityScore2,
            String propertyRecordType,
            Boolean bargainPrice,
            Boolean exclusive,
            Boolean rtoPotential,
            Boolean specialFinancing,
            Boolean rtoFinancing,
            Date yearBuilt,
            Long addressId,
            String fullStreetName,
            String urlHex,
            String amountDefinition) {
        this.propertyId = propertyId;
        this.firstMergeDate = firstMergeDate;
        this.saleType = saleType;
        this.amount = amount;
        this.bedrooms = bedrooms;
        this.fixerUpper = fixerUpper;
        this.bathsTotal = bathsTotal;
        this.livingAreaSquareFeet = livingAreaSquareFeet;
        this.lotSize = lotSize;
        this.homeScore = homeScore;
        this.investorScore = investorScore;
        this.streetAddress = streetAddress;
        this.city = city;
        this.county = county;
        this.stateOrProvince = stateOrProvince;
        this.postal_code = postal_code;
        this.qualityScore2 = qualityScore2;
        this.propertyRecordType = propertyRecordType;
        this.bargainPrice = bargainPrice;
        this.exclusive = exclusive;
        this.rtoPotential = rtoPotential;
        this.specialFinancing = specialFinancing;
        this.rtoFinancing = rtoFinancing;
        YearBuilt = yearBuilt;
        this.addressId = addressId;
        this.fullStreetName = fullStreetName;
        this.streetAddress = streetAddress;
        this.urlHex = urlHex;
        this.amountDefinition = amountDefinition;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    public Timestamp getFirstMergeDate() {
        return firstMergeDate;
    }

    public void setFirstMergeDate(Timestamp firstMergeDate) {
        this.firstMergeDate = firstMergeDate;
    }

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(Integer bedrooms) {
        this.bedrooms = bedrooms;
    }

    public Boolean getFixerUpper() {
        return fixerUpper;
    }

    public void setFixerUpper(Boolean fixerUpper) {
        this.fixerUpper = fixerUpper;
    }

    public BigDecimal getBathsTotal() {
        return bathsTotal;
    }

    public void setBathsTotal(BigDecimal bathsTotal) {
        this.bathsTotal = bathsTotal;
    }

    public Integer getLivingAreaSquareFeet() {
        return livingAreaSquareFeet;
    }

    public void setLivingAreaSquareFeet(Integer livingAreaSquareFeet) {
        this.livingAreaSquareFeet = livingAreaSquareFeet;
    }

    public String getLotSize() {
        return lotSize;
    }

    public void setLotSize(String lotSize) {
        this.lotSize = lotSize;
    }

    public Integer getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(Integer homeScore) {
        this.homeScore = homeScore;
    }

    public Integer getInvestorScore() {
        return investorScore;
    }

    public void setInvestorScore(Integer investorScore) {
        this.investorScore = investorScore;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getStateOrProvince() {
        return stateOrProvince;
    }

    public void setStateOrProvince(String stateOrProvince) {
        this.stateOrProvince = stateOrProvince;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public Integer getQualityScore2() {
        return qualityScore2;
    }

    public void setQualityScore2(Integer qualityScore2) {
        this.qualityScore2 = qualityScore2;
    }

    public String getPropertyRecordType() {
        return propertyRecordType;
    }

    public void setPropertyRecordType(String propertyRecordType) {
        this.propertyRecordType = propertyRecordType;
    }

    public Boolean getBargainPrice() {
        return bargainPrice;
    }

    public void setBargainPrice(Boolean bargainPrice) {
        this.bargainPrice = bargainPrice;
    }

    public Boolean getExclusive() {
        return exclusive;
    }

    public void setExclusive(Boolean exclusive) {
        this.exclusive = exclusive;
    }

    public Boolean getRtoPotential() {
        return rtoPotential;
    }

    public void setRtoPotential(Boolean rtoPotential) {
        this.rtoPotential = rtoPotential;
    }

    public Boolean getSpecialFinancing() {
        return specialFinancing;
    }

    public void setSpecialFinancing(Boolean specialFinancing) {
        this.specialFinancing = specialFinancing;
    }

    public Boolean getRtoFinancing() {
        return rtoFinancing;
    }

    public void setRtoFinancing(Boolean rtoFinancing) {
        this.rtoFinancing = rtoFinancing;
    }

    public Date getYearBuilt() {
        return YearBuilt;
    }

    public void setYearBuilt(Date yearBuilt) {
        YearBuilt = yearBuilt;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public String getFullStreetName() {
        return fullStreetName;
    }

    public void setFullStreetName(String fullStreetName) {
        this.fullStreetName = fullStreetName;
    }

    public String getUrlHex() {
        return urlHex;
    }

    public void setUrlHex(String urlHex) {
        this.urlHex = urlHex;
    }

    public String getAmountDefinition() {
        return amountDefinition;
    }

    public void setAmountDefinition(String amountDefinition) {
        this.amountDefinition = amountDefinition;
    }

    public Integer getPricePerSqft() {
        return pricePerSqft;
    }

    public void setPricePerSqft(Integer pricePerSqft) {
        this.pricePerSqft = pricePerSqft;
    }

    public String getYearBuiltDate() {
        return yearBuiltDate;
    }

    public void setYearBuiltDate(String yearBuiltDate) {
        this.yearBuiltDate = yearBuiltDate;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PropertyDTO)) return false;

        PropertyDTO that = (PropertyDTO) o;

        if (getPropertyId() != null ? !getPropertyId().equals(that.getPropertyId()) : that.getPropertyId() != null)
            return false;
        if (getFirstMergeDate() != null ? !getFirstMergeDate().equals(that.getFirstMergeDate()) : that.getFirstMergeDate() != null)
            return false;
        if (getSaleType() != null ? !getSaleType().equals(that.getSaleType()) : that.getSaleType() != null)
            return false;
        if (getAmount() != null ? !getAmount().equals(that.getAmount()) : that.getAmount() != null) return false;
        if (getBedrooms() != null ? !getBedrooms().equals(that.getBedrooms()) : that.getBedrooms() != null)
            return false;
        if (getFixerUpper() != null ? !getFixerUpper().equals(that.getFixerUpper()) : that.getFixerUpper() != null)
            return false;
        if (getBathsTotal() != null ? !getBathsTotal().equals(that.getBathsTotal()) : that.getBathsTotal() != null)
            return false;
        if (getLivingAreaSquareFeet() != null ? !getLivingAreaSquareFeet().equals(that.getLivingAreaSquareFeet()) : that.getLivingAreaSquareFeet() != null)
            return false;
        if (getLotSize() != null ? !getLotSize().equals(that.getLotSize()) : that.getLotSize() != null) return false;
        if (getHomeScore() != null ? !getHomeScore().equals(that.getHomeScore()) : that.getHomeScore() != null)
            return false;
        if (getInvestorScore() != null ? !getInvestorScore().equals(that.getInvestorScore()) : that.getInvestorScore() != null)
            return false;
        if (getStreetAddress() != null ? !getStreetAddress().equals(that.getStreetAddress()) : that.getStreetAddress() != null)
            return false;
        if (getCity() != null ? !getCity().equals(that.getCity()) : that.getCity() != null) return false;
        if (getCounty() != null ? !getCounty().equals(that.getCounty()) : that.getCounty() != null) return false;
        if (getStateOrProvince() != null ? !getStateOrProvince().equals(that.getStateOrProvince()) : that.getStateOrProvince() != null)
            return false;
        if (getPostal_code() != null ? !getPostal_code().equals(that.getPostal_code()) : that.getPostal_code() != null)
            return false;
        if (getQualityScore2() != null ? !getQualityScore2().equals(that.getQualityScore2()) : that.getQualityScore2() != null)
            return false;
        if (getPropertyRecordType() != null ? !getPropertyRecordType().equals(that.getPropertyRecordType()) : that.getPropertyRecordType() != null)
            return false;
        if (getBargainPrice() != null ? !getBargainPrice().equals(that.getBargainPrice()) : that.getBargainPrice() != null)
            return false;
        if (getExclusive() != null ? !getExclusive().equals(that.getExclusive()) : that.getExclusive() != null)
            return false;
        if (getRtoPotential() != null ? !getRtoPotential().equals(that.getRtoPotential()) : that.getRtoPotential() != null)
            return false;
        if (getSpecialFinancing() != null ? !getSpecialFinancing().equals(that.getSpecialFinancing()) : that.getSpecialFinancing() != null)
            return false;
        if (getRtoFinancing() != null ? !getRtoFinancing().equals(that.getRtoFinancing()) : that.getRtoFinancing() != null)
            return false;
        if (getYearBuilt() != null ? !getYearBuilt().equals(that.getYearBuilt()) : that.getYearBuilt() != null)
            return false;
        if (getAddressId() != null ? !getAddressId().equals(that.getAddressId()) : that.getAddressId() != null)
            return false;
        if (getFullStreetName() != null ? !getFullStreetName().equals(that.getFullStreetName()) : that.getFullStreetName() != null)
            return false;
        if (getUrlHex() != null ? !getUrlHex().equals(that.getUrlHex()) : that.getUrlHex() != null) return false;
        if (getAmountDefinition() != null ? !getAmountDefinition().equals(that.getAmountDefinition()) : that.getAmountDefinition() != null)
            return false;
        if (getPricePerSqft() != null ? !getPricePerSqft().equals(that.getPricePerSqft()) : that.getPricePerSqft() != null)
            return false;
        if (getYearBuiltDate() != null ? !getYearBuiltDate().equals(that.getYearBuiltDate()) : that.getYearBuiltDate() != null)
            return false;
        if (getYear() != null ? !getYear().equals(that.getYear()) : that.getYear() != null) return false;
        return getStreetNumber() != null ? getStreetNumber().equals(that.getStreetNumber()) : that.getStreetNumber() == null;
    }

    @Override
    public int hashCode() {
        int result = getPropertyId() != null ? getPropertyId().hashCode() : 0;
        result = 31 * result + (getFirstMergeDate() != null ? getFirstMergeDate().hashCode() : 0);
        result = 31 * result + (getSaleType() != null ? getSaleType().hashCode() : 0);
        result = 31 * result + (getAmount() != null ? getAmount().hashCode() : 0);
        result = 31 * result + (getBedrooms() != null ? getBedrooms().hashCode() : 0);
        result = 31 * result + (getFixerUpper() != null ? getFixerUpper().hashCode() : 0);
        result = 31 * result + (getBathsTotal() != null ? getBathsTotal().hashCode() : 0);
        result = 31 * result + (getLivingAreaSquareFeet() != null ? getLivingAreaSquareFeet().hashCode() : 0);
        result = 31 * result + (getLotSize() != null ? getLotSize().hashCode() : 0);
        result = 31 * result + (getHomeScore() != null ? getHomeScore().hashCode() : 0);
        result = 31 * result + (getInvestorScore() != null ? getInvestorScore().hashCode() : 0);
        result = 31 * result + (getStreetAddress() != null ? getStreetAddress().hashCode() : 0);
        result = 31 * result + (getCity() != null ? getCity().hashCode() : 0);
        result = 31 * result + (getCounty() != null ? getCounty().hashCode() : 0);
        result = 31 * result + (getStateOrProvince() != null ? getStateOrProvince().hashCode() : 0);
        result = 31 * result + (getPostal_code() != null ? getPostal_code().hashCode() : 0);
        result = 31 * result + (getQualityScore2() != null ? getQualityScore2().hashCode() : 0);
        result = 31 * result + (getPropertyRecordType() != null ? getPropertyRecordType().hashCode() : 0);
        result = 31 * result + (getBargainPrice() != null ? getBargainPrice().hashCode() : 0);
        result = 31 * result + (getExclusive() != null ? getExclusive().hashCode() : 0);
        result = 31 * result + (getRtoPotential() != null ? getRtoPotential().hashCode() : 0);
        result = 31 * result + (getSpecialFinancing() != null ? getSpecialFinancing().hashCode() : 0);
        result = 31 * result + (getRtoFinancing() != null ? getRtoFinancing().hashCode() : 0);
        result = 31 * result + (getYearBuilt() != null ? getYearBuilt().hashCode() : 0);
        result = 31 * result + (getAddressId() != null ? getAddressId().hashCode() : 0);
        result = 31 * result + (getFullStreetName() != null ? getFullStreetName().hashCode() : 0);
        result = 31 * result + (getUrlHex() != null ? getUrlHex().hashCode() : 0);
        result = 31 * result + (getAmountDefinition() != null ? getAmountDefinition().hashCode() : 0);
        result = 31 * result + (getPricePerSqft() != null ? getPricePerSqft().hashCode() : 0);
        result = 31 * result + (getYearBuiltDate() != null ? getYearBuiltDate().hashCode() : 0);
        result = 31 * result + (getYear() != null ? getYear().hashCode() : 0);
        result = 31 * result + (getStreetNumber() != null ? getStreetNumber().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PropertyDTO{" +
                "propertyId=" + propertyId +
                ", firstMergeDate=" + firstMergeDate +
                ", saleType='" + saleType + '\'' +
                ", amount=" + amount +
                ", bedrooms=" + bedrooms +
                ", fixerUpper=" + fixerUpper +
                ", bathsTotal=" + bathsTotal +
                ", livingAreaSquareFeet=" + livingAreaSquareFeet +
                ", lotSize='" + lotSize + '\'' +
                ", homeScore=" + homeScore +
                ", investorScore=" + investorScore +
                ", streetAddress='" + streetAddress + '\'' +
                ", city='" + city + '\'' +
                ", county='" + county + '\'' +
                ", stateOrProvince='" + stateOrProvince + '\'' +
                ", postal_code='" + postal_code + '\'' +
                ", qualityScore2=" + qualityScore2 +
                ", propertyRecordType='" + propertyRecordType + '\'' +
                ", bargainPrice=" + bargainPrice +
                ", exclusive=" + exclusive +
                ", rtoPotential=" + rtoPotential +
                ", specialFinancing=" + specialFinancing +
                ", rtoFinancing=" + rtoFinancing +
                ", YearBuilt=" + YearBuilt +
                ", addressId=" + addressId +
                ", fullStreetName='" + fullStreetName + '\'' +
                ", urlHex='" + urlHex + '\'' +
                ", amountDefinition='" + amountDefinition + '\'' +
                ", pricePerSqft=" + pricePerSqft +
                ", yearBuiltDate='" + yearBuiltDate + '\'' +
                ", year='" + year + '\'' +
                ", streetNumber='" + streetNumber + '\'' +
                '}';
    }
}
