package com.nic.properties.model;

import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.List;

public class SrpFilterResponse implements Serializable {


    private List<TBLPropertyModel> propertyList;
    private List<TBLPropertyModel> milesPropertyList;
    private List<TBLPropertyModel> subtypesPropertyList;
    private Long count;
    private Long milesCount;
    private Long subTypesCount;
    //private HttpStatus httpStatus;
    private String errorMessage;
    private List<NextProperties> nextPropertyIDs;

    public SrpFilterResponse() {
    }

    public SrpFilterResponse(List<TBLPropertyModel> propertyList, List<TBLPropertyModel> milesPropertyList,
                             List<TBLPropertyModel> subtypesPropertyList, Long count, Long milesCount, Long subTypesCount,
                            /* HttpStatus httpStatus,*/ String errorMessage, List<NextProperties> nextPropertyIDs) {
        super();
        this.propertyList = propertyList;
        this.milesPropertyList = milesPropertyList;
        this.subtypesPropertyList = subtypesPropertyList;
        this.count = count;
        this.milesCount = milesCount;
        this.subTypesCount = subTypesCount;
        //this.httpStatus = httpStatus;
        this.errorMessage = errorMessage;
        this.nextPropertyIDs = nextPropertyIDs;
    }

    public List<TBLPropertyModel> getPropertyList() {
        return propertyList;
    }

    public void setPropertyList(List<TBLPropertyModel> propertyList) {
        this.propertyList = propertyList;
    }

    public List<TBLPropertyModel> getMilesPropertyList() {
        return milesPropertyList;
    }

    public void setMilesPropertyList(List<TBLPropertyModel> milesPropertyList) {
        this.milesPropertyList = milesPropertyList;
    }

    public List<TBLPropertyModel> getSubtypesPropertyList() {
        return subtypesPropertyList;
    }

    public void setSubtypesPropertyList(List<TBLPropertyModel> subtypesPropertyList) {
        this.subtypesPropertyList = subtypesPropertyList;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getMilesCount() {
        return milesCount;
    }

    public void setMilesCount(Long milesCount) {
        this.milesCount = milesCount;
    }

    public Long getSubTypesCount() {
        return subTypesCount;
    }

    public void setSubTypesCount(Long subTypesCount) {
        this.subTypesCount = subTypesCount;
    }

  /*  public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
*/
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<NextProperties> getNextPropertyIDs() {
        return nextPropertyIDs;
    }

    public void setNextPropertyIDs(List<NextProperties> nextPropertyIDs) {
        this.nextPropertyIDs = nextPropertyIDs;
    }

}
