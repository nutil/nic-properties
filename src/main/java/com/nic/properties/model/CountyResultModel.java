package com.nic.properties.model;

import java.io.Serializable;

public class CountyResultModel implements Serializable {

    private String countyName;
    private String city;
    private Long count;

    public CountyResultModel(String countyName, String city, Long count) {
        super();
        this.countyName = countyName;
        this.city = city;
        this.count = count;
    }

    public CountyResultModel() {
        super();
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "CountyResultModel [countyName=" + countyName + ", city=" + city + ", count=" + count + "]";
    }


}
