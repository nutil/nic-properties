package com.nic.properties.model;

import java.io.Serializable;

public class NearByMediamHomePriceByGeoModel implements Serializable {

    String geo;
    String mdn;
    String county;
    String city;
    String state;

    public NearByMediamHomePriceByGeoModel(String geo, String mdn) {
        super();
        this.geo = geo;
        this.mdn = mdn;
    }

    public NearByMediamHomePriceByGeoModel(String geo, String mdn, String county) {
        super();
        this.geo = geo;
        this.mdn = mdn;
        this.county = county;
    }

    public NearByMediamHomePriceByGeoModel(String geo, String mdn, String county, String city, String state) {
        super();
        this.geo = geo;
        this.mdn = mdn;
        this.county = county;
        this.city = city;
        this.state = state;
    }

    public NearByMediamHomePriceByGeoModel(String geo, String mdn, String county, String city) {
        super();
        this.geo = geo;
        this.mdn = mdn;
        this.county = county;
        this.city = city;
    }

    public NearByMediamHomePriceByGeoModel() {
    }


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGeo() {
        return geo;
    }

    public void setGeo(String geo) {
        this.geo = geo;
    }

    public String getMdn() {
        return mdn;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
