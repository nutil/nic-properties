package com.nic.properties.helper;

import com.nic.properties.QEntities.QProperty;
import com.nic.properties.configuration.NICConfigProperties;
import com.nic.properties.model.PropertyDTO;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public abstract class QJPAHelper {

    protected static final QProperty qProperty = QProperty.property;
    @PersistenceContext
    private EntityManager entityManager;

    public <T> JPAQuery<T> getHibernateHintedJPAQuery() {
        return new JPAQuery<T>(entityManager).setHint(NICConfigProperties.HIBERNATE_HINT, NICConfigProperties.HIBERNATE_HINT_VALUE);
    }

    public QBean<PropertyDTO> getPropertyDTO() {
        QBean<PropertyDTO> propertyDTOQBean = Projections.bean(
                PropertyDTO.class,
                qProperty.propertyId,
                qProperty.firstMergeDate,
                qProperty.saleType,
                qProperty.amount,
                qProperty.bedrooms,
                qProperty.fixerUpper,
                qProperty.bathsTotal,
                qProperty.livingAreaSquareFeet,
                qProperty.lotSize,
                qProperty.homeScore,
                qProperty.investorScore,
                qProperty.streetAddress,
                qProperty.city,
                qProperty.county,
                qProperty.stateOrProvince,
                qProperty.postal_Code.as("postal_code"),
                qProperty.qualityScore2,
                qProperty.propertyRecordType,
                qProperty.bargainPrice,
                qProperty.exclusive,
                qProperty.rtoPotential,
                qProperty.specialFinancing,
                qProperty.rtoFinancing,
                qProperty.YearBuilt,
                qProperty.addressId,
                qProperty.fullStreetName,
                qProperty.urlHex,
                qProperty.amountDefinition
        );
        return propertyDTOQBean;
    }
}
