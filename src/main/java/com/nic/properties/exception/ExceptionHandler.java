package com.nic.properties.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {
    @org.springframework.web.bind.annotation.ExceptionHandler({

    })
    public ResponseEntity<ExceptionMessage> handleKnownAndUnknownExceptions(Exception e) {
        List<String> errorMessages = new ArrayList<>();
        errorMessages.add(e.getClass().getName());
        ExceptionMessage exceptionMessage = new ExceptionMessage(LocalDateTime.now(), "Invalid Request.",
                errorMessages);

        return new ResponseEntity<>(exceptionMessage, HttpStatus.BAD_REQUEST);
    }
}

class ExceptionMessage {
    private LocalDateTime timestamp;
    private String message;
    private List<String> exceptions;

    public ExceptionMessage() {
    }

    public ExceptionMessage(LocalDateTime timestamp, String message, List<String> exceptions) {
        this.timestamp = timestamp;
        this.message = message;
        this.exceptions = exceptions;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getExceptions() {
        return exceptions;
    }

    public void setExceptions(List<String> exceptions) {
        this.exceptions = exceptions;
    }
}
