package com.nic.properties.util;

import com.nic.properties.model.TBLImagePdpModel;
import com.nic.properties.srpenum.ImageSizeEnum;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.xml.bind.DatatypeConverter;

@Component
public class ImagePdpUtils {
    @Value("${image.contentValue}")
    private String contentValue;

    public String getImageUrl(TBLImagePdpModel image, ImageSizeEnum imageSize, boolean isSecure, String contentKey) {
        String url = image.getUrl();
        if (StringUtils.hasText(url)) {

            int lastIndexOfDot = url.lastIndexOf('.');
            String fileExtension = url.substring(lastIndexOfDot);

            // create remote url
            if (!image.getRemote()) {
                StringBuffer remoteUrl = new StringBuffer();


                if (!"local-only".equals(contentValue)) {
                    String[] values = contentValue.split("\\|");

                    String hex = url.substring(url.lastIndexOf('/') + 1, url.lastIndexOf('.'));
                    byte[] binary = DatatypeConverter.parseHexBinary(hex);
                    byte[] xorToggleBinary = DatatypeConverter.parseHexBinary(values[2]);
                    byte[] binaryXored = new byte[binary.length];

                    // circular shift
                    binary = circularLeftShift(binary, Integer.parseInt(values[1]));

                    // XOR toggle
                    for (int i = 0; i < binary.length; i++) {
                        byte b = binary[i];
                        if (xorToggleBinary.length > i) {
                            binaryXored[i] = (byte) (b ^ xorToggleBinary[i]);
                        } else {
                            binaryXored[i] = b;
                        }
                    }
                    String hexFinal = DatatypeConverter.printHexBinary(binaryXored);

                    remoteUrl.append("http");
                    if (isSecure) {
                        remoteUrl.append('s');
                    }
                    remoteUrl.append("://");
                    remoteUrl.append(values[0]);
                    remoteUrl.append('/');
                    remoteUrl.append(hexFinal);
                    remoteUrl.append(fileExtension);

                    url = remoteUrl.toString().toLowerCase();
                    lastIndexOfDot = url.lastIndexOf('.');
                }
            }
            // adding suffix
            String suffix = "";
            switch (imageSize) {
                case thumbnail:
                    suffix = "_th2";
                    break;
                case small:
                    suffix = "_sm2";
                    break;
                case medium2:
                    suffix = "_md2";
                    break;
            }
            url = url.substring(0, lastIndexOfDot) + suffix + fileExtension;
        }
        return url;
    }

    private byte[] circularLeftShift(byte[] target, int bits) {
        int size = target.length * 8;
        bits = bits % size; // in case of bits are same or greater than bytes bits
        int bytes = bits / 8;
        int smallBits = bits % 8; // only bits under 8 to shift
        byte[] result = new byte[target.length];

        for (int i = 0; i < target.length; i++) {
            byte upper = target[(bytes + i) % target.length];
            byte lower = target[(bytes + i + 1) % target.length];
            result[i] = (byte) ((upper << smallBits) | ((lower & 0xff) >>> (8 - smallBits)));
        }
        return result;
    }

}
