package com.nic.properties.util;

import com.nic.properties.model.MiniSRPPropertyModel;
import com.nic.properties.model.TBLImageModel;
import com.nic.properties.model.TblePropertyModelSRPFilter;
import com.nic.properties.srpenum.ImageSizeEnum;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.xml.bind.DatatypeConverter;
import java.util.ArrayList;
import java.util.List;

@Component
public class ImageUtils {

    @Value("${image.contentValue}")
    private String contentValue;

    public String getImageUrl(TBLImageModel image, ImageSizeEnum imageSize, boolean isSecure, String contentKey) {
        String url = image.getUrl();
        if (StringUtils.hasText(url)) {

            int lastIndexOfDot = url.lastIndexOf('.');
            String fileExtension = url.substring(lastIndexOfDot);

            // create remote url
//			if (!image.getRemote()) {
            StringBuffer remoteUrl = new StringBuffer();
            // String contentValue =
            // unstructuredContentService.getRequiredValue(contentKey);

            if (!"local-only".equals(contentValue)) {
                String[] values = contentValue.split("\\|");

                String hex = url.substring(url.lastIndexOf('/') + 1, url.lastIndexOf('.'));
                byte[] binary = DatatypeConverter.parseHexBinary(hex);
                byte[] xorToggleBinary = DatatypeConverter.parseHexBinary(values[2]);
                byte[] binaryXored = new byte[binary.length];

                // circular shift
                binary = circularLeftShift(binary, Integer.parseInt(values[1]));

                // XOR toggle
                for (int i = 0; i < binary.length; i++) {
                    byte b = binary[i];
                    if (xorToggleBinary.length > i) {
                        binaryXored[i] = (byte) (b ^ xorToggleBinary[i]);
                    } else {
                        binaryXored[i] = b;
                    }
                }
                String hexFinal = DatatypeConverter.printHexBinary(binaryXored);

                remoteUrl.append("http");
                if (isSecure) {
                    remoteUrl.append('s');
                }
                remoteUrl.append("://");
                remoteUrl.append(values[0]);
                remoteUrl.append('/');
                remoteUrl.append(hexFinal);
                remoteUrl.append(fileExtension);

                url = remoteUrl.toString().toLowerCase();
                lastIndexOfDot = url.lastIndexOf('.'); // need to calculate again since we changed url
            }


            // adding suffix
            String suffix = "";
            switch (imageSize) {
                case thumbnail:
                    suffix = "_th2";
                    break;
                case small:
                    suffix = "_sm2";
                    break;
                case medium2:
                    suffix = "_md2";
                    break;
            }
            url = url.substring(0, lastIndexOfDot) + suffix + fileExtension;
        }

        return url;
    }

    private byte[] circularLeftShift(byte[] target, int bits) {
        int size = target.length * 8;
        bits = bits % size; // in case of bits are same or greater than bytes bits
        int bytes = bits / 8;
        int smallBits = bits % 8; // only bits under 8 to shift
        byte[] result = new byte[target.length];

        for (int i = 0; i < target.length; i++) {
            byte upper = target[(bytes + i) % target.length];
            byte lower = target[(bytes + i + 1) % target.length];
            result[i] = (byte) ((upper << smallBits) | ((lower & 0xff) >>> (8 - smallBits)));
        }

        return result;
    }

    public String getImageUrlHex(TBLImageModel image) {
        String url = image.getUrl();
        String hex = null;
        if (StringUtils.hasText(url)) {

            if (!"local-only".equals(contentValue)) {
                hex = url.substring(url.lastIndexOf('/') + 1, url.lastIndexOf('.'));
            }

        }
        return hex;
    }
    public String getImageUrlHexForImage(String image) {
        String url = image;

        String hex = null;
        if (StringUtils.hasText(url)) {

            if (!"local-only".equals(contentValue)) {
                hex = url.substring(url.lastIndexOf('/') + 1, url.lastIndexOf('.'));
            }

        }
        return hex;
    }
    /**
     * @param entityList
     * @return
     */
    public List<TblePropertyModelSRPFilter> updateImageHexaValue(List<TblePropertyModelSRPFilter> entityList) {


        entityList.parallelStream().forEach(x->
        {
            x.setUrlHex(getImageUrlHexForImage(x.getUrlHex()));
        });
        return entityList;

    }

    public List<MiniSRPPropertyModel> updateImageHexaValuePDP(List<MiniSRPPropertyModel> convertValue) {

        convertValue.parallelStream().forEach(x->
        {
            x.setUrlHex(getImageUrlHexForImage(x.getUrlHex()));
        });
        return convertValue;
    }


    public List<String> getImageUrlHexForImageList(List<String> images) {
        List<String> imagesHex = new ArrayList<>();
        images.stream().forEach(x->
        {   imagesHex.add(getImageUrlHexForImage(x));

        });
        return imagesHex;
    }
}
