package com.nic.properties.util;

import com.nic.properties.model.CountyResultModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.RecursiveTask;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class CountyCountAsyncSort extends RecursiveTask<TreeMap<String, List<CountyResultModel>>> {
    List<CountyResultModel> countyResultModels;
    TreeMap<String, List<CountyResultModel>> countyResultModelTreeMap = new TreeMap<>();
    int startIndex;
    int endIndex;

    public CountyCountAsyncSort(List<CountyResultModel> countyResultModels, int startIndex, int endIndex) {
        this.countyResultModels = countyResultModels;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }


    @Override
    protected TreeMap<String, List<CountyResultModel>> compute() {
        int THRESHOLD = 250;
        int endIndex = this.countyResultModels.size() - 1;
        int startIndex = this.countyResultModels.indexOf(this.countyResultModels.get(0));
        if (endIndex - startIndex <= THRESHOLD) {
            return sortTheCountyCounts();
        } else {
            int middle = (endIndex - startIndex) / 2;
            List<CountyCountAsyncSort> taskList = new ArrayList<>();
            CountyCountAsyncSort task_1 = new CountyCountAsyncSort(countyResultModels.subList(startIndex, middle), startIndex, middle);
            taskList.add(task_1);
            CountyCountAsyncSort task_2 = new CountyCountAsyncSort(countyResultModels.subList(middle, endIndex), middle, endIndex);
            taskList.add(task_2);
            invokeAll(taskList);
            countyResultModelTreeMap.putAll(task_1.join());
            countyResultModelTreeMap.putAll(task_2.join());
            return countyResultModelTreeMap;
        }
    }

    public TreeMap<String, List<CountyResultModel>> sortTheCountyCounts() {
        Map<String, List<CountyResultModel>> groupedMap = countyResultModels
                .stream()
                .filter(countyResultModel -> {
                            String countyName = countyResultModel.getCountyName();
                            if (
                                    Pattern.matches("^[0-9]+$", countyName) ||
                                            countyName.length() == 1 ||
                                            countyName.contains("#") ||
                                            countyResultModel.getCity().contains("#") ||
                                            countyName.contains(",")) {
                                return false;
                            } else {
                                return true;
                            }
                        }
                )
                .collect(
                        Collectors.groupingBy(CountyResultModel::getCountyName)
                );
        return new TreeMap<>(groupedMap);
    }
}
