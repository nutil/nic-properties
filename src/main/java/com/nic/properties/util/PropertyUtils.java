package com.nic.properties.util;

import com.nic.properties.model.PropertyDTO;
import com.nic.properties.sevice.StreetName;
import org.springframework.stereotype.Component;

import java.util.List;

/***
 *
 * Util class for PropertyDTO to transform/change some fields.
 *
 */

@Component
public class PropertyUtils {
    private final StreetName streetName;
    private final ImageUtils imageUtils;

    /*
    *
    * StreetName and ImageUtils should not be null when injected.
    *
    * */
    public PropertyUtils(StreetName streetName, ImageUtils imageUtils) {
        this.streetName = streetName;
        this.imageUtils = imageUtils;
    }

    // Map PropertyDTO.java.
    private void transformPropertyDTO(PropertyDTO propertyDTO, String saleType) {
        String urlHex;
        int pricePerSquareFeet;

        // Get and add url hex to PropertyDTO.java
        if (propertyDTO.getUrlHex() != null) {
            urlHex = this.imageUtils.getImageUrlHexForImage(propertyDTO.getUrlHex());
            propertyDTO.setUrlHex(urlHex);
        }

        // Calculate and add pricePerSquareFeet and add to the PropertyDTO.java.
        if (propertyDTO.getAmount() != null && propertyDTO.getLivingAreaSquareFeet() != null) {
            pricePerSquareFeet = (propertyDTO.getAmount() / propertyDTO.getLivingAreaSquareFeet());
            propertyDTO.setPricePerSqft(pricePerSquareFeet);
        }

        // Get yearBuildDate and year and add to the PropertyDTO.java.
        if (propertyDTO.getFirstMergeDate() != null) {
            String yearBuiltDate = propertyDTO.getFirstMergeDate().toString().split(" ")[0];
            String year = yearBuiltDate.split("-")[0];
            propertyDTO.setYearBuiltDate(yearBuiltDate);
            propertyDTO.setYear(year);
        }

        //  Get a 4 digit postal code and prepend a zero.
        if (propertyDTO.getPostal_code().length() == 4) {
            String postalCode = "0".concat(propertyDTO.getPostal_code());
            propertyDTO.setPostal_code(postalCode);
        }

        // Assign fullStreetName and streetAddress to sale types like ResaleMLS and FSBO.
        if ((propertyDTO.getFullStreetName() == null)) {
            propertyDTO.setFullStreetName(streetName.getStreet(propertyDTO.getStreetAddress()));
        }
        if (!(propertyDTO.getSaleType().equalsIgnoreCase("FSBO")) && !(propertyDTO.getSaleType().equalsIgnoreCase("ResaleMLS"))) {
            propertyDTO.setStreetAddress(propertyDTO.getFullStreetName());
        } else {
            propertyDTO.setStreetNumber(streetName.getStreetNumber(propertyDTO.getStreetAddress()));
        }

        // Set saleGroups to properties.
        if (saleType.equalsIgnoreCase("ForeclosuresRS2")) {
            propertyDTO.setSaleType("ForeclosuresRS2");
        } else if (saleType.equalsIgnoreCase("PreForeclosures")) {
            propertyDTO.setSaleType("PreForeclosures");
        } else if (saleType.equalsIgnoreCase("BankForeclosures")) {
            propertyDTO.setSaleType("BankForeclosures");
        } else if (saleType.equalsIgnoreCase("Auctions")) {
            propertyDTO.setSaleType("Auctions");
        } else if (saleType.equalsIgnoreCase("ShortSale")) {
            propertyDTO.setSaleType("ShortSale");
        } else if (saleType.equalsIgnoreCase("HUDOrGovtForeclosures")) {
            propertyDTO.setSaleType("HUDOrGovtForeclosures");
        } else if (saleType.equalsIgnoreCase("HUDHomesForeclosureRS2")) {
            propertyDTO.setSaleType("HUDHomesForeclosureRS2");
        } else if (saleType.equalsIgnoreCase("ResaleMLS")) {
            propertyDTO.setSaleType("ResaleMLS");
            propertyDTO.setStreetNumber(streetName.getStreetNumber(propertyDTO.getStreetAddress()));
        } else if (saleType.equalsIgnoreCase("FSBO")) {
            propertyDTO.setSaleType("FSBO");
            propertyDTO.setStreetNumber(streetName.getStreetNumber(propertyDTO.getStreetAddress()));
        } else if (saleType.equalsIgnoreCase("RentToOwn")) {
            propertyDTO.setSaleType("RentToOwn");
        } else if (saleType.equalsIgnoreCase("Rental")) {
            propertyDTO.setSaleType("Rental");
        }
    }

    // Map a list of PropertyDTOs.
    public void transformPropertyDTOs(List<PropertyDTO> propertyDTOS, String saleType) {
        propertyDTOS.forEach(propertyDTO -> transformPropertyDTO(propertyDTO, saleType));
    }

}
