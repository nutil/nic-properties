package com.nic.properties.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.TableGenerator;
import java.io.Serializable;


@Embeddable
@JsonInclude(Include.NON_NULL)
public class Features implements Serializable {

    @Column(name = "amount")

    private Integer pricePerSqft;
    @Column(name = "lot_size", columnDefinition = "TEXT", nullable = true)
    @TableGenerator(name = "tbl_prop")
    private String lotSize;

    @Column(name = "total_units", columnDefinition = "SMALLINT", length = 6)
    @TableGenerator(name = "tbl_prop")
    private Integer totalUnits;
    @Column(name = "total_rooms", columnDefinition = "SMALLINT")
    @TableGenerator(name = "tbl_prop")
    private Integer totalRooms;
    @Column(name = "room_list", columnDefinition = "TEXT", nullable = true)
    @TableGenerator(name = "tbl_prop")
    private String roomList;
    @Column(name = "stories")
    @TableGenerator(name = "tbl_prop")
    private String stories;
    @Column(name = "parking")
    @TableGenerator(name = "tbl_prop")
    private String parking;
    @Column(name = "parking_spaces")
    @TableGenerator(name = "tbl_prop")
    private Integer parkingSpace;
    @Column(name = "heating")
    @TableGenerator(name = "tbl_prop")
    private String heating;
    @Column(name = "roof_type", columnDefinition = "TEXT", nullable = true)
    @TableGenerator(name = "tbl_prop")
    private String roofType;

    public Integer getPricePerSqft() {
        return pricePerSqft;
    }

    public void setPricePerSqft(Integer pricePerSqft) {
        this.pricePerSqft = pricePerSqft;
    }

    public String getLotSize() {
        return lotSize;
    }

    public void setLotSize(String lotSize) {
        this.lotSize = lotSize;
    }

    public Integer getTotalUnits() {
        return totalUnits;
    }

    public void setTotalUnits(Integer totalUnits) {
        this.totalUnits = totalUnits;
    }

    public Integer getTotalRooms() {
        return totalRooms;
    }

    public void setTotalRooms(Integer totalRooms) {
        this.totalRooms = totalRooms;
    }

    public String getRoomList() {
        return roomList;
    }

    public void setRoomList(String roomList) {
        this.roomList = roomList;
    }

    public String getStories() {
        return stories;
    }

    public void setStories(String stories) {
        this.stories = stories;
    }

    public String getParking() {
        return parking;
    }

    public void setParking(String parking) {
        this.parking = parking;
    }

    public Integer getParkingSpace() {
        return parkingSpace;
    }

    public void setParkingSpace(Integer parkingSpace) {
        this.parkingSpace = parkingSpace;
    }

    public String getHeating() {
        return heating;
    }

    public void setHeating(String heating) {
        this.heating = heating;
    }

    public String getRoofType() {
        return roofType;
    }

    public void setRoofType(String roofType) {
        this.roofType = roofType;
    }

}
