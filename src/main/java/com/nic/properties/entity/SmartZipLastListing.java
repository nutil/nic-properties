package com.nic.properties.entity;

import java.io.Serializable;

public class SmartZipLastListing implements Serializable {

    public String date;

    public long price;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

}
