package com.nic.properties.entity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_nearest_cities", schema = "nic_pdb")
public class TBLNearestCitiesEntity {

    @GeneratedValue(strategy = GenerationType.TABLE, generator = "course")
    @TableGenerator(name = "tbl_nearest_cities", table = "GENERATOR_TABLE", pkColumnName = "key", valueColumnName = "next", pkColumnValue = "course", allocationSize = 30)
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "search_state")
    private String searchState;
    @Column(name = "search_city")
    private String searchCity;
    @Column(name = "state")
    private String state;
    @Column(name = "city")
    private String city;
    @Column(name = "proximity")
    private Double proximity;

    public TBLNearestCitiesEntity() {
    }

    public TBLNearestCitiesEntity(Integer id, String searchState, String searchCity, String state, String city,
                                  Double proximity) {
        super();
        this.id = id;
        this.searchState = searchState;
        this.searchCity = searchCity;
        this.state = state;
        this.city = city;
        this.proximity = proximity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSearchState() {
        return searchState;
    }

    public void setSearchState(String searchState) {
        this.searchState = searchState;
    }

    public String getSearchCity() {
        return searchCity;
    }

    public void setSearchCity(String searchCity) {
        this.searchCity = searchCity;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getProximity() {
        return proximity;
    }

    public void setProximity(Double proximity) {
        this.proximity = proximity;
    }

}
