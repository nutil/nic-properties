package com.nic.properties.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "nic_smartzip.median_home_price_by_geo")
public class MedianHomePriceByGeo implements Serializable {

    @Id
    @Column(name = "geo", columnDefinition = "varchar(128)")
    private String geo;
    @JsonIgnore
    @Column(name = "geo_type")
    private String geoType;
    @Column(name = "mdn", columnDefinition = "decimal(12,2)")
    private Long mdn;

    public String getGeo() {
        return geo;
    }

    public String getGeoType() {
        return geoType;
    }

    public Long getMdn() {
        return mdn;
    }

}
