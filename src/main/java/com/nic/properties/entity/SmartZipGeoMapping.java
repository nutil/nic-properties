package com.nic.properties.entity;

import java.io.Serializable;

public class SmartZipGeoMapping implements Serializable {

    public String latitude;

    public String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

}
