package com.nic.properties.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.nic.properties.model.PopularNearbyZips;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SqlResultSetMappings(value = {
        @SqlResultSetMapping(name = "MapPropertiesWithPopularNearByZips",
                classes = {
                        @ConstructorResult(targetClass = com.nic.properties.model.PopularNearbyZips.class,
                                columns = {
                                        @ColumnResult(name = "county", type = String.class),
                                        @ColumnResult(name = "city", type = String.class),
                                        @ColumnResult(name = "zipCode", type = String.class)
                                })})})
@NamedNativeQueries(value = {
        @NamedNativeQuery(
                name = "fetchPopularNearbyZips",
                query = "SELECT " +
                        "	p.county,p.city,p.postal_code AS zipCode " +
                        " from nic_pdb.tbl_prop p " +
                        "	WHERE p.state_or_province = :stateToBeSearched " +
                        " AND " +
                        "	p.sale_type IN (:saleTypesToBeSearchedFor) " +
                        " GROUP BY 3 " +
                        " ORDER BY 3 ASC",
                hints = {
                        @QueryHint(name = "org.hibernate.readOnly", value = "true")
                },
                resultClass = PopularNearbyZips.class,
                resultSetMapping = "MapPropertiesWithPopularNearByZips")

})
@Table(name = "tbl_prop", schema = "nic_pdb")
public class TBLPropertyEntity implements Serializable {

    @GeneratedValue(strategy = GenerationType.TABLE, generator = "course")
    @TableGenerator(name = "tbl_prop", table = "GENERATOR_TABLE", pkColumnName = "key", valueColumnName = "next", pkColumnValue = "course", allocationSize = 30)
    @Id
    @Column(name = "property_id")
    private Long propertyId;

    @Column(name = "first_merge_date")
    private Date firstMergeDate;

    @Column(name = "sale_type", nullable = false, columnDefinition = "VARCHAR")
    private String saleType;

    @Column(name = "amount")
    private Integer amount;

    @Column(name = "bedrooms", columnDefinition = "BIT", length = 4)
    private Integer bedrooms;

    @Column(name = "fixer_upper")
    private Boolean fixerUpper;

    @Column(name = "baths_total")
    private BigDecimal bathsTotal;

    @Column(name = "living_area_square_feet")
    private Integer livingAreaSquareFeet;

    @Column(name = "lot_size", columnDefinition = "TEXT")
    private String lotSize;

    @Column(name = "home_score", columnDefinition = "BIT", length = 3)
    private Integer homeScore;

    @Column(name = "investor_score", columnDefinition = "BIT", length = 3)
    private Integer investorScore;

    @Column(name = "street_address")
    private String streetAddress;

    @Column(name = "address_id")
    private Long addressId;

    @Column(name = "city")
    private String city;

    @Column(name = "county")
    private String county;

    @Column(name = "state_or_province", columnDefinition = "CHAR")
    private String stateOrProvince;

    @Column(name = "postal_code")
    private Integer postal_code;
/*    @Column(name = "quality_score_1")
    private Integer qualityScore1;*/

    @Column(name = "quality_score_2")
    private Integer qualityScore2;

    /* @Column(name = "quality_score_3")
     private Integer qualityScore3;

     @Column(name = "quality_score_4")
     private Integer qualityScore4;

     @Column(name = "quality_score_5")
     private Integer qualityScore5;

     @Column(name = "quality_score_6")
     private Integer qualityScore6;

     @Column(name = "quality_score_7")
     private Integer qualityScore7;

     @Column(name = "quality_score_8")
     private Integer qualityScore8;

     @Column(name = "quality_score_9")
     private Integer qualityScore9;

     @Column(name = "quality_score_10")
     private Integer qualityScore10;*/
    @Column(name = "property_record_type")
    private String propertyRecordType;
    @Column(name = "bargain_price")
    private Boolean bargainPrice;

    @Column(name = "exclusive")
    private Boolean exclusive;

    @Column(name = "rto_potential")
    private Boolean rtoPotential;

    @Column(name = "special_financing")
    private Boolean specialFinancing;
    @Column(name = "rto_financing")
    private Boolean rtoFinancing;

    @Column(name = "year_built")
    @Temporal(TemporalType.DATE)
    private Date YearBuilt;
    @Column(name = "full_street_name")
    private String streetName;
    @Column(name = "picture_data_url", columnDefinition = "VARCHAR(512)")
    private String primaryImage;
    @OneToMany(mappedBy = "propertyEntity", fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<TBLImageEntity> imageEntity = new ArrayList<>();
}
