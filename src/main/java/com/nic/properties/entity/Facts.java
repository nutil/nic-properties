package com.nic.properties.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Embeddable
@JsonInclude(Include.ALWAYS)
public class Facts implements Serializable {

    @Column(name = "year_built", columnDefinition = "date", nullable = true)
    @Temporal(TemporalType.DATE)
    private Date YearBuilt;
    @TableGenerator(name = "tbl_prop")
    @Column(name = "property_id", insertable = false, updatable = false)
    private Integer propertyId;
    @Column(name = "mls_number")
    @TableGenerator(name = "tbl_prop")
    private String mlsNumber;
    @Column(name = "parcel_number")
    @TableGenerator(name = "tbl_prop")
    private String parcelNumber;
    @Column(name = "property_record_type")
    @TableGenerator(name = "tbl_prop")
    private String propertyType;
    @Column(name = "county", nullable = true)
    private String county;
    @Column(name = "legal_description", columnDefinition = "TEXT", nullable = true)
    @TableGenerator(name = "tbl_prop")
    private String legalDescription;
    @Column(name = "zoning")
    @TableGenerator(name = "tbl_prop")
    private String zoning;
    @Column(name = "exterior_walls")
    @TableGenerator(name = "tbl_prop")
    private String exterior_walls;

    public Date getYearBuilt() {
        return YearBuilt;
    }

    public void setYearBuilt(Date yearBuilt) {
        YearBuilt = yearBuilt;
    }

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public String getMlsNumber() {
        return mlsNumber;
    }

    public void setMlsNumber(String mlsNumber) {
        this.mlsNumber = mlsNumber;
    }

    public String getParcelNumber() {
        return parcelNumber;
    }

    public void setParcelNumber(String parcelNumber) {
        this.parcelNumber = parcelNumber;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getLegalDescription() {
        return legalDescription;
    }

    public void setLegalDescription(String legalDescription) {
        this.legalDescription = legalDescription;
    }

    public String getZoning() {
        return zoning;
    }

    public void setZoning(String zoning) {
        this.zoning = zoning;
    }

    public String getExterior_walls() {
        return exterior_walls;
    }

    public void setExterior_walls(String exterior_walls) {
        this.exterior_walls = exterior_walls;
    }

    @Override
    public String toString() {
        return "Facts [YearBuilt=" + YearBuilt + ", propertyId=" + propertyId + ", mlsNumber=" + mlsNumber
                + ", parcelNumber=" + parcelNumber + ", propertyType=" + propertyType + ", county=" + county
                + ", legalDescription=" + legalDescription + ", zoning=" + zoning + ", exterior_features = "+ exterior_walls + " ]";
    }
}
