package com.nic.properties.entity;

import java.io.Serializable;

public class SmartzipComparable implements Serializable {

    public long id;

    public String address;

    public String unit_number;

    public String city;

    public String state;

    public String zip;

    public SmartZipGeoMapping geo_mapping;

    public String streetview_picture_url;

    public SmartZipPropertyAttributes property_attributes;

    public String link;

    public SmartZipLastListing last_listing;

    public SmartZipLastTransaction last_transaction;
    private Integer propertyId;

    public SmartZipLastTransaction getLast_transaction() {
        return last_transaction;
    }

    public void setLast_transaction(SmartZipLastTransaction last_transaction) {
        this.last_transaction = last_transaction;
    }

    public SmartZipLastListing getLast_listing() {
        return last_listing;
    }

    public void setLast_listing(SmartZipLastListing last_listing) {
        this.last_listing = last_listing;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUnit_number() {
        return unit_number;
    }

    public void setUnit_number(String unit_number) {
        this.unit_number = unit_number;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public SmartZipGeoMapping getGeo_mapping() {
        return geo_mapping;
    }

    public void setGeo_mapping(SmartZipGeoMapping geo_mapping) {
        this.geo_mapping = geo_mapping;
    }

    public String getStreetview_picture_url() {
        return streetview_picture_url;
    }

    public void setStreetview_picture_url(String streetview_picture_url) {
        this.streetview_picture_url = streetview_picture_url;
    }

    public SmartZipPropertyAttributes getProperty_attributes() {
        return property_attributes;
    }

    public void setProperty_attributes(SmartZipPropertyAttributes property_attributes) {
        this.property_attributes = property_attributes;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }


}
