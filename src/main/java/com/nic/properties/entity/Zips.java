package com.nic.properties.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "nic_pdb.ZIP")
public class Zips implements Serializable {

    @Id
    @Column(name = "ZIP", columnDefinition = "mediumint")
    private Integer zips;
    @Column(name = "city")
    private String city;
    @Column(name = "fips", columnDefinition = "mediumint")
    private Integer fips;

    public Integer getZips() {
        return zips;
    }

    public void setZips(Integer zips) {
        this.zips = zips;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getFips() {
        return fips;
    }

    public void setFips(Integer fips) {
        this.fips = fips;
    }

}
