package com.nic.properties.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;


@Entity(name = "tbl_smartzip_bulk_prop")
@Table(schema = "nic_smartzip", name = "tbl_smartzip_bulk_prop")
public class SmartZipBulkProperty implements Serializable {
    @Id
    @Column(name = "id", columnDefinition = "bigint(20)", precision = 20)
    private BigInteger propertyId;
    @Column(name = "addr_streetaddress", columnDefinition = "varchar(45)", length = 45)
    private String streetAddress;
    @Column(name = "addr_cityname", columnDefinition = "varchar(25)", length = 25)
    private String cityName;
    @Column(name = "county_name", columnDefinition = "varchar(20)", length = 20)
    private String countyName;
    @Column(name = "addr_statecode", columnDefinition = "char(2)", length = 2)
    private String stateCode;
    @Column(name = "addr_zip5", columnDefinition = "int(11)", precision = 11)
    private Integer zipCode;

    public SmartZipBulkProperty() {
    }

    public SmartZipBulkProperty(BigInteger propertyId, String streetAddress, String cityName, String countyName, String stateCode, Integer zipCode) {
        this.propertyId = propertyId;
        this.streetAddress = streetAddress;
        this.cityName = cityName;
        this.countyName = countyName;
        this.stateCode = stateCode;
        this.zipCode = zipCode;
    }

    public BigInteger getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(BigInteger propertyId) {
        this.propertyId = propertyId;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public Integer getZipCode() {
        return zipCode;
    }

    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SmartZipBulkProperty that = (SmartZipBulkProperty) o;

        if (propertyId != null ? !propertyId.equals(that.propertyId) : that.propertyId != null) return false;
        if (streetAddress != null ? !streetAddress.equals(that.streetAddress) : that.streetAddress != null)
            return false;
        if (cityName != null ? !cityName.equals(that.cityName) : that.cityName != null) return false;
        if (countyName != null ? !countyName.equals(that.countyName) : that.countyName != null) return false;
        if (stateCode != null ? !stateCode.equals(that.stateCode) : that.stateCode != null) return false;
        return zipCode != null ? zipCode.equals(that.zipCode) : that.zipCode == null;
    }

    @Override
    public int hashCode() {
        int result = propertyId != null ? propertyId.hashCode() : 0;
        result = 31 * result + (streetAddress != null ? streetAddress.hashCode() : 0);
        result = 31 * result + (cityName != null ? cityName.hashCode() : 0);
        result = 31 * result + (countyName != null ? countyName.hashCode() : 0);
        result = 31 * result + (stateCode != null ? stateCode.hashCode() : 0);
        result = 31 * result + (zipCode != null ? zipCode.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SmartZipBulkProperty{" +
                "propertyId=" + propertyId +
                ", streetAddress='" + streetAddress + '\'' +
                ", cityName='" + cityName + '\'' +
                ", countyName='" + countyName + '\'' +
                ", stateCode='" + stateCode + '\'' +
                ", zipCode=" + zipCode +
                '}';
    }
}
