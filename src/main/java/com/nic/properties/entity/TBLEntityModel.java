package com.nic.properties.entity;

import java.math.BigDecimal;
import java.util.Date;

public class TBLEntityModel {
    private Integer propertyId;


    private Date firstMergeDate;

    private String saleType;

    private Integer amount;

    private Integer bedrooms;

    private Integer fixerUpper;

    private BigDecimal bathsTotal;

    private Integer livingAreaSquareFeet;

    private String lotSize;

    private Integer homeScore;

    private Integer investorScore;

//	private String streetAddress;

    private String city;

    private String county;

    private String stateOrProvince;

    private Integer postal_code;

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public String geturl(String url) {
        return url;
    }

    public void setFirstMergeDate(Date firstMergeDate) {
        this.firstMergeDate = firstMergeDate;
    }

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(Integer bedrooms) {
        this.bedrooms = bedrooms;
    }

    public Integer getfixerUpper(Integer fixerUpper) {
        return fixerUpper;
    }

    public void setfixerUpper(Integer fixerUpper) {
        this.fixerUpper = fixerUpper;
    }

    public BigDecimal getBathsTotal() {
        return bathsTotal;
    }

    public void setBathsTotal(BigDecimal bathsTotal) {
        this.bathsTotal = bathsTotal;
    }

    public Integer getLivingAreaSquareFeet() {
        return livingAreaSquareFeet;
    }

    public void setLivingAreaSquareFeet(Integer livingAreaSquareFeet) {
        this.livingAreaSquareFeet = livingAreaSquareFeet;
    }

    public String getLotSize() {
        return lotSize;
    }

    public void setLotSize(String lotSize) {
        this.lotSize = lotSize;
    }

    public Integer getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(Integer homeScore) {
        this.homeScore = homeScore;
    }

    public Integer getInvestorScore() {
        return investorScore;
    }

    public void setInvestorScore(Integer investorScore) {
        this.investorScore = investorScore;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getStateOrProvince() {
        return stateOrProvince;
    }

    public void setStateOrProvince(String stateOrProvince) {
        this.stateOrProvince = stateOrProvince;
    }

    public Integer getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(Integer postal_code) {
        this.postal_code = postal_code;
    }

}
