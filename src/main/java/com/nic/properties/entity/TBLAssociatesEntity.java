package com.nic.properties.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tbl_associates", schema = "nic_pdb")
public class TBLAssociatesEntity implements Serializable {

    @GeneratedValue(strategy = GenerationType.TABLE)
    @TableGenerator(name = "tbl_associates")
    @Id
    @Column(name = "associate_id")
    private Integer associateId;

    @Column(name = "given_name", nullable = false, columnDefinition = "VARCHAR")
    private String givenName;

    @Column(name = "surname", nullable = false, columnDefinition = "VARCHAR")
    private String surname;

    @Column(name = "company_name", nullable = false, columnDefinition = "VARCHAR")
    private String companyName;

    @Column(name = "full_name", nullable = false, columnDefinition = "VARCHAR")
    private String fullName;

    @Column(name = "street_address", nullable = false, columnDefinition = "VARCHAR")
    private String streetAddress;

    @Column(name = "city", nullable = false, columnDefinition = "VARCHAR")
    private String city;

    @Column(name = "county", nullable = false, columnDefinition = "VARCHAR")
    private String county;

    @Column(name = "state_code", nullable = false, columnDefinition = "VARCHAR")
    private String stateCode;

    @Column(name = "phone", nullable = false, columnDefinition = "VARCHAR")
    private String phone;
    @ManyToOne
    @JoinColumn(name = "associate_id", nullable = false, insertable = false, updatable = false)
    @JsonIgnore
    private TBLPropertyAssociatesEntity propertyAssociateEntity;

    public String getStreetAddress() {
        return streetAddress;
    }

    public String getCity() {
        return city;
    }

    public String getCounty() {
        return county;
    }

    public String getStateCode() {
        return stateCode;
    }

    public String getPhone() {
        return phone;
    }

    public TBLPropertyAssociatesEntity getPropertyAssociateEntity() {
        return propertyAssociateEntity;
    }

    public Integer getAssociateId() {
        return associateId;
    }

    public String getGivenName() {
        return givenName;
    }

    public String getSurname() {
        return surname;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getFullName() {
        return fullName;
    }

}
