package com.nic.properties.entity;

import javax.persistence.*;
import java.io.Serializable;

@NamedQueries(value = {
        @NamedQuery(
                name = "getSaleTypeFromSaleGroupType",
                query = "SELECT DISTINCT p.saleType FROM TBLPropertyTaxonomy p WHERE p.saleGroup = :saleGroupToBeSearchedFor",
                hints = {
                        @QueryHint(name = "org.hibernate.readOnly", value = "true")
                })})
@Entity
@Table(name = "property_taxonomy", schema = "nic_pdb")
public class TBLPropertyTaxonomy implements Serializable {
    @Id
    @Column(name = "id_property_taxonomy", columnDefinition = "int(11)")
    private Integer id;
    @Column(name = "main_category_type")
    private String mainCategory;
    @Column(name = "sale_group_type")
    private String saleGroup;
    @Column(name = "sale_type")
    private String saleType;

    public TBLPropertyTaxonomy() {
    }

    public TBLPropertyTaxonomy(Integer id, String mainCategory, String saleGroup, String saleType) {
        this.id = id;
        this.mainCategory = mainCategory;
        this.saleGroup = saleGroup;
        this.saleType = saleType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(String mainCategory) {
        this.mainCategory = mainCategory;
    }

    public String getSaleGroup() {
        return saleGroup;
    }

    public void setSaleGroup(String saleGroup) {
        this.saleGroup = saleGroup;
    }

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }

    @Override
    public String toString() {
        return "TBLPropertyTaxonomy{" +
                "id=" + id +
                ", mainCategory='" + mainCategory + '\'' +
                ", saleGroup='" + saleGroup + '\'' +
                ", saleType='" + saleType + '\'' +
                '}';
    }
}
