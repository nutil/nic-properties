package com.nic.properties.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tbl_prop_associates", schema = "nic_pdb")
public class TBLPropertyAssociatesEntity implements Serializable {

    @GeneratedValue(strategy = GenerationType.TABLE)
    @TableGenerator(name = "tbl_prop_associates")
    @Id
    @Column(name = "associate_id")
    private Integer associateId;

    @Column(name = "property_id", nullable = true)
    private Integer propertyId;

    @OneToMany(mappedBy = "propertyAssociateEntity", fetch = FetchType.EAGER)
    @JsonManagedReference
    @JsonIgnore
    private  List<TBLAssociatesEntity> tblAssociatesEntity = new ArrayList<>();

    public Integer getAssociateId() {
        return associateId;
    }

    public Integer getPropertyId() {
        return propertyId;
    }

    public List<TBLAssociatesEntity> getTblAssociatesEntity() {
        return tblAssociatesEntity;
    }

}
