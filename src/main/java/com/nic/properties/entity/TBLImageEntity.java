package com.nic.properties.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tbl_images", schema = "nic_pdb")
public class TBLImageEntity implements Serializable {

    @GeneratedValue(strategy = GenerationType.TABLE, generator = "course")
    @TableGenerator(name = "tbl_images", table = "GENERATOR_TABLE", pkColumnName = "key", valueColumnName = "next", pkColumnValue = "course", allocationSize = 30)
    @Id
    @Column(name = "image_id")
    private Integer imageId;

    @ManyToOne
    @JoinColumn(name = "property_id", nullable = false, referencedColumnName = "property_id")
    @JsonBackReference
    private TBLPropertyEntity propertyEntity;

    @Column(name = "url")
    private String url;

    @Column(name = "source_url")
    private String sourceUrl;

    @Column(name = "remote")
    private Boolean remote;

    @Column(name = "alt_image")
    private String altImage;

    public TBLImageEntity() {
        super();
        // TODO Auto-generated constructor stub
    }

    public TBLImageEntity(Integer imageId, TBLPropertyEntity propertyEntity, String url, String sourceUrl,
                          Boolean remote, String altImage) {
        super();
        this.imageId = imageId;
        this.propertyEntity = propertyEntity;
        this.url = url;
        this.sourceUrl = sourceUrl;
        this.remote = remote;
        this.altImage = altImage;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public Boolean getRemote() {
        return remote;
    }

    public void setRemote(Boolean remote) {
        this.remote = remote;
    }

    public String getAltImage() {
        return altImage;
    }

    public void setAltImage(String altImage) {
        this.altImage = altImage;
    }

    public TBLPropertyEntity getPropertyEntity() {
        return propertyEntity;
    }

    public void setPropertyEntity(TBLPropertyEntity propertyEntity) {
        this.propertyEntity = propertyEntity;
    }

    @Override
    public String toString() {
        return "TBLImageEntity [imageId=" + imageId + ", propertyEntity=" + propertyEntity + ", url=" + url
                + ", sourceUrl=" + sourceUrl + ", remote=" + remote + ", altImage=" + altImage + "]";
    }

}
