package com.nic.properties.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tbl_smartzip_avm_history", schema = "nic_smartzip")
public class ValueHistoryEntity implements Serializable {

    @GeneratedValue(strategy = GenerationType.TABLE, generator = "course")
    @TableGenerator(name = "tbl_smartzip_avm_history", table = "GENERATOR_TABLE", pkColumnName = "key", valueColumnName = "next", pkColumnValue = "course", allocationSize = 30)
    @Id
    @Column(name = "pid")
    private Long pid;

    @Column(name = "yrmo", nullable = false, columnDefinition = "VARCHAR")
    private Character yrmo;

    @Column(name = "avm", nullable = true)
    private Integer avm;

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Character getYrmo() {
        return yrmo;
    }

    public void setYrmo(Character yrmo) {
        this.yrmo = yrmo;
    }

    public Integer getAvm() {
        return avm;
    }

    public void setAvm(Integer avm) {
        this.avm = avm;
    }


}
