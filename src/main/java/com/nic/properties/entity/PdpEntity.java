package com.nic.properties.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tbl_prop", schema = "nic_pdb")
public class PdpEntity {

    @Id
    @Column(name = "property_id")
    private Integer propertyId;
    @Column(name = "sale_type", nullable = false, columnDefinition = "VARCHAR")
    private String saleType;
    @Column(name = "amount")
    private Integer amount;
    @Column(name = "bedrooms", columnDefinition = "BIT", length = 4)
    private Integer bedrooms;
    @Column(name = "fixer_upper")
    private Boolean fixerUpper;
    @Column(name = "baths_total")
    private BigDecimal bathsTotal;
    @Column(name = "living_area_square_feet")
    private Integer livingAreaSquareFeet;
    @Column(name = "lot_size", columnDefinition = "TEXT")
    private String lotSize;
    @Column(name = "home_score", columnDefinition = "BIT", length = 3)
    private Integer homeScore;
    @Column(name = "investor_score", columnDefinition = "BIT", length = 3)
    private Integer investorScore;
    @Column(name = "street_address")
    private String streetAddress;
    @Column(name = "city")
    private String city;
    @Column(name = "county")
    private String county;
    @Column(name = "state_or_province", columnDefinition = "CHAR")
    private String stateOrProvince;
    @Column(name = "postal_code")
    private Integer postal_code;
    @Column(name = "quality_score_3")
    private Integer qualityScore3;
    @Column(name = "property_record_type")
    private String propertyRecordType;
    @Column(name = "bargain_price")
    private Boolean bargainPrice;
    @Column(name = "exclusive")
    private Boolean exclusive;
    @Column(name = "rto_potential")
    private Boolean rtoPotential;
    @Column(name = "special_financing")
    private Boolean specialFinancing;
    @Column(name = "rto_financing")
    private Boolean rtoFinancing;
    @Column(name = "year_built")
    @Temporal(TemporalType.DATE)
    private Date YearBuilt;

}
