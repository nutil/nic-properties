package com.nic.properties.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;

@Entity(name = "CNTY")
@Table(name = "CNTY", schema = "nic_pdb")
public class CountyEntity implements Serializable {
    @Id
    @Column(name = "fips", columnDefinition = "mediumint(5) unsigned zerofill", precision = 5, nullable = false, unique = true)
    private BigInteger fips;

    @Column(name = "name", columnDefinition = "varchar(25)", length = 25, nullable = false)
    private String countyName;

    @Column(columnDefinition = "char(2)", name = "state", length = 2)
    private String stateCode;

    public CountyEntity() {
    }

    public CountyEntity(BigInteger fips, String countyName, String stateCode) {
        this.fips = fips;
        this.countyName = countyName;
        this.stateCode = stateCode;
    }

    public BigInteger getFips() {
        return fips;
    }

    public void setFips(BigInteger fips) {
        this.fips = fips;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    @Override
    public String toString() {
        return "CountyEntity{" +
                "fips=" + fips +
                ", countyName='" + countyName + '\'' +
                ", stateCode='" + stateCode + '\'' +
                '}';
    }
}
