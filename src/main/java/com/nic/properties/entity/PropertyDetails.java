package com.nic.properties.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Calendar;

@Entity
@Table(name = "tbl_prop", schema = "nic_pdb")
@JsonInclude(Include.NON_NULL)
public class PropertyDetails implements Serializable {
    @Embedded
    Features features;
    @Embedded
    Facts facts;
    @Embedded
    ForeclosureInfo foreclosureInfo;
    @Embedded
    PreForeclosureInfo preForeclosureInfo;
    @Id
    @Column(name = "property_id", nullable = false, columnDefinition = "INT", length = 11)

    private Integer propertyId;
    @Column(name = "description", columnDefinition = "text")
    private String description;
    @Column(name = "sale_type")
    private String saleType;
    @Column(name = "full_street_name")
    private String streetName;
    @Column(name = "city")
    private String city;
    @Column(name = "state_or_province", nullable = true, columnDefinition = "CHAR")
    private String State;
    @Column(name = "longitude", nullable = true, columnDefinition = "DOUBLE")
    private Double longitude;
    @Column(name = "latitude", nullable = true, columnDefinition = "DOUBLE")
    private Double latitude;

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Features getFeatures() {
        return features;
    }

    public void setFeatures(Features features) {
        this.features = features;
    }

    public Facts getFacts() {
        return facts;
    }

    public void setFacts(Facts facts) {
        this.facts = facts;
    }

    public ForeclosureInfo getForeclosureInfo() {
        return foreclosureInfo;
    }

    public void setForeclosureInfo(ForeclosureInfo foreclosureInfo) {
        this.foreclosureInfo = foreclosureInfo;
    }

    public PreForeclosureInfo getPreForeclosureInfo() {
        return preForeclosureInfo;
    }

    public void setPreForeclosureInfo(PreForeclosureInfo preForeclosureInfo) {
        this.preForeclosureInfo = preForeclosureInfo;
    }

    @Override
    public String toString() {
        return "PropertyDetails [propertyId=" + propertyId + ", saleType=" + saleType + ", streetName=" + streetName
                + ", city=" + city + ", State=" + State + ", longitude=" + longitude + ", latitude=" + latitude
                + ", features=" + features + ", facts=" + facts + ", foreclosureInfo=" + foreclosureInfo
                + ", preForeclosureInfo=" + preForeclosureInfo + "]";
    }

    public PropertyDetails setValuesToNothing() {

        if (this.getFeatures() != null) {
            if (this.getFeatures().getHeating() != null)
                this.getFeatures().setHeating(" ");
            if (this.getFeatures().getLotSize() != null)
                this.getFeatures().setLotSize(" ");
            if (this.getFeatures().getParking() != null)
                this.getFeatures().setParking(" ");
            if (this.getFeatures().getParkingSpace() != null)
                this.getFeatures().setParkingSpace(0);
            if (this.getFeatures().getPricePerSqft() != null)
                this.getFeatures().setPricePerSqft(0);
            if (this.getFeatures().getRoofType() != null)
                this.getFeatures().setRoofType(" ");
            if (this.getFeatures().getRoomList() != null)
                this.getFeatures().setRoomList(" ");
            if (this.getFeatures().getStories() != null)
                this.getFeatures().setStories(" ");
            if (this.getFeatures().getTotalRooms() != null)
                this.getFeatures().setTotalRooms(0);
            if (this.getFeatures().getTotalUnits() != null)
                this.getFeatures().setTotalUnits(0);
        }

        if (this.getFacts().getCounty() != null)
            this.getFacts().setCounty(" ");
        if (this.getFacts().getLegalDescription() != null)
            this.getFacts().setLegalDescription(" ");
        if (this.getFacts().getMlsNumber() != null)
            this.getFacts().setMlsNumber(" ");
        if (this.getFacts().getParcelNumber() != null)
            this.getFacts().setParcelNumber(" ");
        if (this.getFacts().getPropertyType() != null)
            this.getFacts().setPropertyType(" ");
        if (this.getFacts().getYearBuilt() != null)
            this.getFacts().setYearBuilt(Calendar.getInstance().getTime());
        if (this.getFacts().getZoning() != null)
            this.getFacts().setZoning(" ");

        if (this.getForeclosureInfo() == null) {

        } else {
            if (this.getForeclosureInfo().getAuctionAddress() != null)
                this.getForeclosureInfo().setAuctionAddress(" ");

            if (this.getForeclosureInfo().getAuctionDate() != null)
                this.getForeclosureInfo().setAuctionDate(Calendar.getInstance().getTime());

            if (this.getForeclosureInfo().getAuctionRecordingDate() != null)
                this.getForeclosureInfo().setAuctionRecordingDate(Calendar.getInstance().getTime());

            if (this.getForeclosureInfo().getAuctionTime() != null)
                this.getForeclosureInfo().setAuctionTime(Time.valueOf("12:12:12"));

            if (this.getForeclosureInfo().getLaonDate() != null)
                this.getForeclosureInfo().setLaonDate(Calendar.getInstance().getTime());

            if (this.getForeclosureInfo().getLoanDefaultAmount() != null)
                this.getForeclosureInfo().setLoanDefaultAmount(0);

            if (this.getForeclosureInfo().getLoanNumber() != null)
                this.getForeclosureInfo().setLoanNumber(" ");

            if (this.getForeclosureInfo().getOpeningBid() != null)
                this.getForeclosureInfo().setOpeningBid(0);

            if (this.getForeclosureInfo().getReoDocumentNumber() != null)
                this.getForeclosureInfo().setReoDocumentNumber(" ");

            if (this.getForeclosureInfo().getReoRecordingDate() != null)
                this.getForeclosureInfo().setReoRecordingDate(Calendar.getInstance().getTime());
            if (this.getForeclosureInfo().getTrusteeSaleNumber() != null)
                this.getForeclosureInfo().setTrusteeSaleNumber(" ");

        }

        if (this.getPreForeclosureInfo() == null) {

        } else {
            if (this.getPreForeclosureInfo().getAmountDefaulted() != null)
                this.getPreForeclosureInfo().setAmountDefaulted(0);

            if (this.getPreForeclosureInfo().getDateDefaulted() != null)
                this.getPreForeclosureInfo().setDateDefaulted(Calendar.getInstance().getTime());

            if (this.getPreForeclosureInfo().getDateDefaultedLien() != null)
                this.getPreForeclosureInfo().setDateDefaultedLien(Calendar.getInstance().getTime());
            if (this.getPreForeclosureInfo().getDocumentNumber() != null)
                this.getPreForeclosureInfo().setDocumentNumber(" ");
            if (this.getPreForeclosureInfo().getRecordingDate() != null)
                this.getPreForeclosureInfo().setRecordingDate(Calendar.getInstance().getTime());
            if (this.getPreForeclosureInfo().getRecordingYear() != null)
                this.getPreForeclosureInfo().setRecordingYear(Calendar.getInstance().getTime());

        }

        return this;

    }
}
