package com.nic.properties.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.TableGenerator;
import java.io.Serializable;

@Embeddable
@JsonInclude(JsonInclude.Include.ALWAYS)
public class PriceInfo implements Serializable {

    @JsonIgnore
    @TableGenerator(name = "tbl_prop")
    @Column(name = "property_id", insertable = false, updatable = false)
    private Integer propertyId;

    @Column(name = "estimated_value")
    @TableGenerator(name = "tbl_prop")
    private Integer estimated_value;

    @Column(name = "avm_low")
    @TableGenerator(name = "tbl_prop")
    private Integer avm_low;

    @Column(name = "avm_high")
    @TableGenerator(name = "tbl_prop")
    private Integer avm_high;

    @Column(name = "avm_confidence",  columnDefinition = "DECIMAL")
    @TableGenerator(name = "tbl_prop")
    private String avm_confidence;

    @Column(name = "estimated_rent")
    @TableGenerator(name = "tbl_prop")
    private Integer estimated_rent;

    @Column(name = "list_date")
    @TableGenerator(name = "tbl_prop")
    private String list_date;

    @Column(name = "avm_date")
    @TableGenerator(name = "tbl_prop")
    private String avm_date;



    public Integer getAvm_low() {
        return avm_low;
    }

    public void setAvm_low(Integer avm_low) {
        this.avm_low = avm_low;
    }

    public Integer getAvm_high() {
        return avm_high;
    }

    public void setAvm_high(Integer avm_high) {
        this.avm_high = avm_high;
    }

    public String getAvm_confidence() {
        return avm_confidence;
    }

    public void setAvm_confidence(String avm_confidence) {
        this.avm_confidence = avm_confidence;
    }

    public Integer getEstimated_rent() {
        return estimated_rent;
    }

    public void setEstimated_rent(Integer estimated_rent) {
        this.estimated_rent = estimated_rent;
    }

    public Integer getEstimated_value() {
        return estimated_value;
    }

    public void setEstimated_value(Integer estimated_value) {
        this.estimated_value = estimated_value;
    }

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public String getList_date() {
        return list_date;
    }

    public void setList_date(String list_date) {
        this.list_date = list_date;
    }

    public String getAvm_date() {
        return avm_date;
    }

    public void setAvm_date(String avm_date) {
        this.avm_date = avm_date;
    }

}
