package com.nic.properties.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_images", schema = "nic_pdb")
public class TBLImagePdpEntity {

    @Id
    @Column(name = "image_id")
    private Integer imageId;

    @Column(name = "property_id")
    private Integer propertyId;

    @Column(name = "url")
    private String url;

    @Column(name = "source_url")
    private String sourceUrl;

    @Column(name = "remote")
    private Boolean remote;

    @Column(name = "alt_image")
    private String altImage;

    public TBLImagePdpEntity() {
        super();

    }

    public TBLImagePdpEntity(Integer imageId, String url, String sourceUrl,
                             Boolean remote, String altImage) {
        super();
        this.imageId = imageId;

        this.url = url;
        this.sourceUrl = sourceUrl;
        this.remote = remote;
        this.altImage = altImage;
    }

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public Boolean getRemote() {
        return remote;
    }

    public void setRemote(Boolean remote) {
        this.remote = remote;
    }

    public String getAltImage() {
        return altImage;
    }

    public void setAltImage(String altImage) {
        this.altImage = altImage;
    }

}
