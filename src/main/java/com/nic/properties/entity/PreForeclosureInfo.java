package com.nic.properties.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Embeddable
@JsonInclude(Include.NON_NULL)
public class PreForeclosureInfo implements Serializable {

    @Column(name = "nod_date_defaulted_lien", columnDefinition = "date", nullable = true)
    @Temporal(TemporalType.DATE)
    private Date dateDefaultedLien;

    @Column(name = "nod_amount_default", columnDefinition = "DECIMAL")

    @TableGenerator(name = "tbl_prop")
    private Integer amountDefaulted;

    @Column(name = "nod_date_default", columnDefinition = "date", nullable = true)
    @Temporal(TemporalType.DATE)
    private Date dateDefaulted;

    @Column(name = "nod_recording_date", columnDefinition = "date", nullable = true)
    @Temporal(TemporalType.DATE)
    private Date recordingDate;

    @Column(name = "nod_recording_year", nullable = true, columnDefinition = "year(4)")
    @Temporal(TemporalType.DATE)
    private Date recordingYear;

    @Column(name = "nod_document_number")

    @TableGenerator(name = "tbl_prop")
    private String documentNumber;

    public Date getDateDefaultedLien() {
        return dateDefaultedLien;
    }

    public void setDateDefaultedLien(Date dateDefaultedLien) {
        this.dateDefaultedLien = dateDefaultedLien;
    }

    public Integer getAmountDefaulted() {
        return amountDefaulted;
    }

    public void setAmountDefaulted(Integer amountDefaulted) {
        this.amountDefaulted = amountDefaulted;
    }

    public Date getDateDefaulted() {
        return dateDefaulted;
    }

    public void setDateDefaulted(Date dateDefaulted) {
        this.dateDefaulted = dateDefaulted;
    }

    public Date getRecordingDate() {
        return recordingDate;
    }

    public void setRecordingDate(Date recordingDate) {
        this.recordingDate = recordingDate;
    }

    public Date getRecordingYear() {
        return recordingYear;
    }

    public void setRecordingYear(Date recordingYear) {
        this.recordingYear =
                recordingYear;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

}
