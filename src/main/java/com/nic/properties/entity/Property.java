package com.nic.properties.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;


@Entity(name = "tbl_prop")
@Table(name = "tbl_prop", schema = "nic_pdb")
public class Property implements Serializable {

    @Id
    @Column(name = "property_id", unique = true, nullable = false)
    private Long propertyId;

    @Column(name = "list_price")
    private Integer listPrice;

    @Column(name = "amount_definition", nullable = false)
    private String amountDefinition;

    @Column(name = "street_address")
    private String streetAddress;

    @Column(name = "full_street_name", length = 220)
    private String fullStreetName;

    @Column(name = "city")
    private String city;

    @Column(name = "county", length = 25, nullable = false)
    private String county;

    @Column(name = "state_or_province", length = 2, nullable = false)
    private String stateOrProvince;

    @Column(name = "postal_code")
    private String postal_Code;

    @Column(name = "picture_data_url")
    private String urlHex;

    @Column(name = "picture_data_source_url")
    private String pictureDataSourceUrl;

    @Column(name = "picture_remote", nullable = false)
    private Boolean pictureRemote;

    @Column(name = "picture_count")
    private Integer pictureCount;

    @Column(name = "parcel_number", length = 50)
    private String parcelNumber;

    @Column(name = "mls_number")
    private String mlsNumber;

    @Column(name = "mls_disclaimer")
    private String mlsDisclaimer;

    @Column(name = "listing_status")
    private String listingStatus;

    @Column(name = "listing_url")
    private String listingUrl;

    @Column(name = "reo_document_number")
    private String reoDocumentNumber;

    @Column(name = "reo_recording_date")
    private Date reoRecordingDate;

    @Column(name = "judgment_amount")
    private Integer judgmentAmount;

    @Column(name = "legal_description")
    private String legalDescription;

    @Column(name = "property_record_type")
    private String propertyRecordType;

    @Column(name = "baths_total")
    private BigDecimal bathsTotal;

    @Column(name = "bedrooms")
    private Integer bedrooms;

    @Column(name = "stories", length = 25)
    private String stories;

    @Column(name = "living_area_square_feet")
    private Integer livingAreaSquareFeet;

    @Column(name = "living_area")
    private String livingArea;

    @Column(name = "lot_size")
    private String lotSize;

    @Column(name = "year_built")
    private Date YearBuilt;

    @Column(name = "total_units")
    private Integer totalUnits;

    @Column(name = "unit_number")
    private String unitNumber;

    @Column(name = "condition")
    private String condition;

    @Column(name = "total_rooms")
    private Integer totalRooms;

    @Column(name = "den")
    private String den;

    @Column(name = "kitchen")
    private String kitchen;

    @Column(name = "living_room")
    private String livingRoom;

    @Column(name = "family_room")
    private String familyRoom;

    @Column(name = "basement")
    private String basement;

    @Column(name = "heating")
    private String heating;

    @Column(name = "cooling")
    private String cooling;

    @Column(name = "parking")
    private String parking;

    @Column(name = "parking_spaces")
    private String parkingSpaces;

    @Column(name = "zoning")
    private String zoning;

    @Column(name = "list_date")
    private Date listDate;

    @Column(name = "features")
    private String features;

    @Column(name = "pool", length = 35)
    private String pool;

    @Column(name = "style", length = 35)
    private String style;

    @Column(name = "construction_type", length = 35)
    private String constructionType;

    @Column(name = "exterior_walls", length = 35)
    private String exteriorWalls;

    @Column(name = "description")
    private String description;

    @Column(name = "sale_type")
    private String saleType;

    @Column(name = "nod_date_defaulted_lien")
    private Date nodDateDefaultedLien;

    @Column(name = "nod_doc_number_defaulted_lien")
    private Integer nodDocNumberDefaultedLien;

    @Column(name = "nod_amount_default")
    private Double nodAmountDefault;

    @Column(name = "nod_date_default")
    private Date nodDateDefault;

    @Column(name = "nod_recording_date")
    private Date nodRecordingDate;

    @Column(name = "nod_recording_year")
    private Integer nodRecordingYear;

    @Column(name = "nod_document_number")
    private String nodDocumentNumber;

    @Column(name = "tax_sale_jurisdiction_name")
    private String taxSaleJurisdictionName;

    @Column(name = "tax_sale_how_often")
    private String taxSaleHowOften;

    @Column(name = "tax_sale_rate")
    private String taxSaleRate;

    @Column(name = "tax_sale_redemption_period")
    private String taxSaleRedemtionPeriod;

    @Column(name = "tax_sale_bid_method")
    private String taxSaleBidMethod;

    @Column(name = "tax_sale_get_premium_back")
    private Boolean taxSaleGetPremiumBack;

    @Column(name = "auction_date")
    private Date auctionDate;

    @Column(name = "auction_time")
    private Time auctionTime;

    @Column(name = "opening_bid")
    private Double notsOpeningBid;

    @Column(name = "nots_auction_title")
    private String notsAuctionTitle;

    @Column(name = "nots_auction_address")
    private String notsAuctionAddress;

    @Column(name = "nots_auction_city")
    private String notsAuctionCity;

    @Column(name = "nots_auction_state", length = 2)
    private String notsAuctionState;

    @Column(name = "nots_auction_zip")
    private Integer notsAuctionZIP;

    @Column(name = "nots_auction_house_name")
    private String notsAuctionHouseName;

    @Column(name = "nots_auction_description")
    private String notsAuctionDescription;

    @Column(name = "nots_auction_terms")
    private String notsAuctionTerms;

    @Column(name = "nots_loan_date")
    private Date notsLoanDate;

    @Column(name = "nots_loan_no")
    private String notsLoanNo;

    @Column(name = "nots_auction_recording_date")
    private Date notsAuctionRecordingDate;

    @Column(name = "nots_auction_document_number")
    private String notsAuctionDocumentNumber;

    @Column(name = "nots_trustee_sale_number")
    private String notsTrusteeSaleNumber;

    @Column(name = "nots_index_no")
    private String notsIndexNo;

    @Column(name = "nots_loan_default_amount")
    private Double NotsLoanDefaultAmount;

    @Column(name = "lis_pendens_docket_no")
    private String lispendDocketNo;

    @Column(name = "lis_pendens_doc_no")
    private String lispendDocNo;

    @Column(name = "lis_pendens_recording_date")
    private Date lispendRecordingDate;

    @Column(name = "lis_pendens_index_no")
    private String lispendIndexNo;

    @Column(name = "lis_pendens_loan_no")
    private String lispendLoanNo;

    @Column(name = "lis_pendens_loan_date")
    private Date lispendLoanDate;

    @Column(name = "lis_pendens_case_no")
    private String lispendCaseNo;

    @Column(name = "hud_203k_eligible")
    private String hud203kEligible;

    @Column(name = "hud_bid_deadline")
    private String hudBidDeadline;

    @Column(name = "hud_priority")
    private String hudPriority;

    @Column(name = "hud_sale_status")
    private String hudSaleStatus;

    @Column(name = "hud_escrow_amount")
    private Integer hudEscrowAmount;

    @Column(name = "assessed_valuation_land")
    private Integer assessedValuationLand;

    @Column(name = "assessed_valuation_improvement")
    private Integer assessedValuationImprovement;

    @Column(name = "assessed_valuation")
    private Integer assessedValuation;

    @Column(name = "assessed_valuation_year")
    private Integer assessedValuationYear;

    @Column(name = "assessed_tax")
    private Integer assessedTax;

    @Column(name = "assessed_tax_year")
    private Integer assessedTaxYear;

    @Column(name = "assessed_year_delinquent")
    private Integer assessedYearDelinquent;

    @Column(name = "assessed_valuation_rate")
    private Double assessedValuationRate;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "fips", nullable = false)
    private String fips;

    @Column(name = "hoa_fee")
    private String hoaFee;

    @Column(name = "close_date")
    private Date closeDate;

    @Column(name = "close_price")
    private Integer closePrice;

    @Column(name = "school_district")
    private String schoolDistrict;

    @Column(name = "elementary_school")
    private String elementaryschool;

    @Column(name = "middle_school")
    private String middleSchool;

    @Column(name = "high_school")
    private String highSchool;

    @Column(name = "quality_score_member")
    private Integer qualityScoreMember;

    @Column(name = "quality_score_sales")
    private Integer qualityScoreSales;

    @Column(name = "quality_score_1")
    private Integer qualityScore1;

    @Column(name = "quality_score_2")
    private Integer qualityScore2;

    @Column(name = "quality_score_3")
    private Integer qualityScore3;

    @Column(name = "quality_score_4")
    private Integer qualityScore4;

    @Column(name = "quality_score_5")
    private Integer qualityScore5;

    @Column(name = "quality_score_6")
    private Integer qualityScore6;

    @Column(name = "quality_score_7")
    private Integer qualityScore7;

    @Column(name = "quality_score_8")
    private Integer qualityScore8;

    @Column(name = "quality_score_9")
    private Integer qualityScore9;

    @Column(name = "quality_score_10")
    private Integer qualityScore10;

    @Column(name = "sort_amount")
    private Integer sortAmount;

    @Column(name = "home_score")
    private Integer homeScore;

    @Column(name = "investor_score")
    private Integer investorScore;

    @Column(name = "address_id")
    private Long addressId;

    @Column(name = "estimated_value")
    private Integer estimatedValue;

    @Column(name = "avm_low")
    private Integer estimatedLow;

    @Column(name = "avm_high")
    private Integer estimatedHigh;

    @Column(name = "avm_confidence")
    private Double estimatedConfidence;

    @Column(name = "avm_date")
    private Timestamp estimatedValueDate;

    @Column(name = "rent")
    private Integer rent;

    @Column(name = "original_loan_amount")
    private Integer originalLoanAmount;

    @Column(name = "amount")
    private Integer amount;

    @Column(name = "transfer_value")
    private Integer transferValue;

    @Column(name = "estimated_rent")
    private Integer estimatedRent;

    @Column(name = "foundation")
    private String foundation;

    @Column(name = "room_list")
    private String roomList;

    @Column(name = "appliances")
    private String appliances;

    @Column(name = "floor_covering")
    private String floorCovering;

    @Column(name = "roof_type")
    private String roofType;

    @Column(name = "view_type")
    private String viewType;

    @Column(name = "tracking")
    private String tracking;

    @Column(name = "paid_listing")
    private Boolean paidListing;

    @Column(name = "frstlk_exp_date")
    private Date firstLook;

    @Column(name = "100_dwn_pymnt")
    private Boolean hundredDownPayment;

    @Column(name = "hmpth_mrtge")
    private Boolean hmpthMortgage;

    @Column(name = "hmpth_renvtion_mrtge")
    private Boolean hmpthRenovationMortgage;

    @Column(name = "price_reduction")
    private Boolean priceReduction;

    @Column(name = "gnnd_exp_date")
    private Date gnndExpDate;

    @Column(name = "nsp_home")
    private Boolean nspHome;

    @Column(name = "specialty_home")
    private Boolean specialtyHome;

    @Column(name = "first_merge_date")
    private Timestamp firstMergeDate;

    @Column(name = "alt_finance")
    private Boolean altFinance;

    @Column(name = "estimated_monthly_payment")
    private Integer estimatedMonthlyPayment;

    @Column(name = "monthly_amount_RTO_henrys_algo")
    private Integer monthlyAmountRTOHenrysAlgo;

    @Column(name = "monthly_amount_30year_fixed_mortgage")
    private Integer monthlyAmount30yearFixedMortgage;

    @Column(name = "data_sources")
    private String dataSources;

    //Special Options Start
    @Column(name = "exclusive")
    private Boolean exclusive;

    @Column(name = "special_financing")
    private Boolean specialFinancing;

    @Column(name = "bargain_price")
    private Boolean bargainPrice;

    @Column(name = "fixer_upper")
    private Boolean fixerUpper;

    @Column(name = "rto_potential")
    private Boolean rtoPotential;

    @Column(name = "rto_financing")
    private Boolean rtoFinancing;
    //Special Options End


    public Property() {
    }

    public Property(Long propertyId, Integer listPrice, String amountDefinition, String streetAddress, String fullStreetName, String city, String county, String stateOrProvince, String postal_Code, String urlHex, String pictureDataSourceUrl, Boolean pictureRemote, Integer pictureCount, String parcelNumber, String mlsNumber, String mlsDisclaimer, String listingStatus, String listingUrl, String reoDocumentNumber, Date reoRecordingDate, Integer judgmentAmount, String legalDescription, String propertyRecordType, BigDecimal bathsTotal, Integer bedrooms, String stories, Integer livingAreaSquareFeet, String livingArea, String lotSize, Date yearBuilt, Integer totalUnits, String unitNumber, String condition, Integer totalRooms, String den, String kitchen, String livingRoom, String familyRoom, String basement, String heating, String cooling, String parking, String parkingSpaces, String zoning, Date listDate, String features, String pool, String style, String constructionType, String exteriorWalls, String description, String saleType, Date nodDateDefaultedLien, Integer nodDocNumberDefaultedLien, Double nodAmountDefault, Date nodDateDefault, Date nodRecordingDate, Integer nodRecordingYear, String nodDocumentNumber, String taxSaleJurisdictionName, String taxSaleHowOften, String taxSaleRate, String taxSaleRedemtionPeriod, String taxSaleBidMethod, Boolean taxSaleGetPremiumBack, Date auctionDate, Time auctionTime, Double notsOpeningBid, String notsAuctionTitle, String notsAuctionAddress, String notsAuctionCity, String notsAuctionState, Integer notsAuctionZIP, String notsAuctionHouseName, String notsAuctionDescription, String notsAuctionTerms, Date notsLoanDate, String notsLoanNo, Date notsAuctionRecordingDate, String notsAuctionDocumentNumber, String notsTrusteeSaleNumber, String notsIndexNo, Double notsLoanDefaultAmount, String lispendDocketNo, String lispendDocNo, Date lispendRecordingDate, String lispendIndexNo, String lispendLoanNo, Date lispendLoanDate, String lispendCaseNo, String hud203kEligible, String hudBidDeadline, String hudPriority, String hudSaleStatus, Integer hudEscrowAmount, Integer assessedValuationLand, Integer assessedValuationImprovement, Integer assessedValuation, Integer assessedValuationYear, Integer assessedTax, Integer assessedTaxYear, Integer assessedYearDelinquent, Double assessedValuationRate, Double latitude, Double longitude, String fips, String hoaFee, Date closeDate, Integer closePrice, String schoolDistrict, String elementaryschool, String middleSchool, String highSchool, Integer qualityScoreMember, Integer qualityScoreSales, Integer qualityScore1, Integer qualityScore2, Integer qualityScore3, Integer qualityScore4, Integer qualityScore5, Integer qualityScore6, Integer qualityScore7, Integer qualityScore8, Integer qualityScore9, Integer qualityScore10, Integer sortAmount, Integer homeScore, Integer investorScore, Long addressId, Integer estimatedValue, Integer estimatedLow, Integer estimatedHigh, Double estimatedConfidence, Timestamp estimatedValueDate, Integer rent, Integer originalLoanAmount, Integer amount, Integer transferValue, Integer estimatedRent, String foundation, String roomList, String appliances, String floorCovering, String roofType, String viewType, String tracking, Boolean paidListing, Date firstLook, Boolean hundredDownPayment, Boolean hmpthMortgage, Boolean hmpthRenovationMortgage, Boolean priceReduction, Date gnndExpDate, Boolean nspHome, Boolean specialtyHome, Timestamp firstMergeDate, Boolean altFinance, Integer estimatedMonthlyPayment, Integer monthlyAmountRTOHenrysAlgo, Integer monthlyAmount30yearFixedMortgage, String dataSources, Boolean exclusive, Boolean specialFinancing, Boolean bargainPrice, Boolean fixerUpper, Boolean rtoPotential, Boolean rtoFinancing) {
        this.propertyId = propertyId;
        this.listPrice = listPrice;
        this.amountDefinition = amountDefinition;
        this.streetAddress = streetAddress;
        this.fullStreetName = fullStreetName;
        this.city = city;
        this.county = county;
        this.stateOrProvince = stateOrProvince;
        this.postal_Code = postal_Code;
        this.urlHex = urlHex;
        this.pictureDataSourceUrl = pictureDataSourceUrl;
        this.pictureRemote = pictureRemote;
        this.pictureCount = pictureCount;
        this.parcelNumber = parcelNumber;
        this.mlsNumber = mlsNumber;
        this.mlsDisclaimer = mlsDisclaimer;
        this.listingStatus = listingStatus;
        this.listingUrl = listingUrl;
        this.reoDocumentNumber = reoDocumentNumber;
        this.reoRecordingDate = reoRecordingDate;
        this.judgmentAmount = judgmentAmount;
        this.legalDescription = legalDescription;
        this.propertyRecordType = propertyRecordType;
        this.bathsTotal = bathsTotal;
        this.bedrooms = bedrooms;
        this.stories = stories;
        this.livingAreaSquareFeet = livingAreaSquareFeet;
        this.livingArea = livingArea;
        this.lotSize = lotSize;
        YearBuilt = yearBuilt;
        this.totalUnits = totalUnits;
        this.unitNumber = unitNumber;
        this.condition = condition;
        this.totalRooms = totalRooms;
        this.den = den;
        this.kitchen = kitchen;
        this.livingRoom = livingRoom;
        this.familyRoom = familyRoom;
        this.basement = basement;
        this.heating = heating;
        this.cooling = cooling;
        this.parking = parking;
        this.parkingSpaces = parkingSpaces;
        this.zoning = zoning;
        this.listDate = listDate;
        this.features = features;
        this.pool = pool;
        this.style = style;
        this.constructionType = constructionType;
        this.exteriorWalls = exteriorWalls;
        this.description = description;
        this.saleType = saleType;
        this.nodDateDefaultedLien = nodDateDefaultedLien;
        this.nodDocNumberDefaultedLien = nodDocNumberDefaultedLien;
        this.nodAmountDefault = nodAmountDefault;
        this.nodDateDefault = nodDateDefault;
        this.nodRecordingDate = nodRecordingDate;
        this.nodRecordingYear = nodRecordingYear;
        this.nodDocumentNumber = nodDocumentNumber;
        this.taxSaleJurisdictionName = taxSaleJurisdictionName;
        this.taxSaleHowOften = taxSaleHowOften;
        this.taxSaleRate = taxSaleRate;
        this.taxSaleRedemtionPeriod = taxSaleRedemtionPeriod;
        this.taxSaleBidMethod = taxSaleBidMethod;
        this.taxSaleGetPremiumBack = taxSaleGetPremiumBack;
        this.auctionDate = auctionDate;
        this.auctionTime = auctionTime;
        this.notsOpeningBid = notsOpeningBid;
        this.notsAuctionTitle = notsAuctionTitle;
        this.notsAuctionAddress = notsAuctionAddress;
        this.notsAuctionCity = notsAuctionCity;
        this.notsAuctionState = notsAuctionState;
        this.notsAuctionZIP = notsAuctionZIP;
        this.notsAuctionHouseName = notsAuctionHouseName;
        this.notsAuctionDescription = notsAuctionDescription;
        this.notsAuctionTerms = notsAuctionTerms;
        this.notsLoanDate = notsLoanDate;
        this.notsLoanNo = notsLoanNo;
        this.notsAuctionRecordingDate = notsAuctionRecordingDate;
        this.notsAuctionDocumentNumber = notsAuctionDocumentNumber;
        this.notsTrusteeSaleNumber = notsTrusteeSaleNumber;
        this.notsIndexNo = notsIndexNo;
        NotsLoanDefaultAmount = notsLoanDefaultAmount;
        this.lispendDocketNo = lispendDocketNo;
        this.lispendDocNo = lispendDocNo;
        this.lispendRecordingDate = lispendRecordingDate;
        this.lispendIndexNo = lispendIndexNo;
        this.lispendLoanNo = lispendLoanNo;
        this.lispendLoanDate = lispendLoanDate;
        this.lispendCaseNo = lispendCaseNo;
        this.hud203kEligible = hud203kEligible;
        this.hudBidDeadline = hudBidDeadline;
        this.hudPriority = hudPriority;
        this.hudSaleStatus = hudSaleStatus;
        this.hudEscrowAmount = hudEscrowAmount;
        this.assessedValuationLand = assessedValuationLand;
        this.assessedValuationImprovement = assessedValuationImprovement;
        this.assessedValuation = assessedValuation;
        this.assessedValuationYear = assessedValuationYear;
        this.assessedTax = assessedTax;
        this.assessedTaxYear = assessedTaxYear;
        this.assessedYearDelinquent = assessedYearDelinquent;
        this.assessedValuationRate = assessedValuationRate;
        this.latitude = latitude;
        this.longitude = longitude;
        this.fips = fips;
        this.hoaFee = hoaFee;
        this.closeDate = closeDate;
        this.closePrice = closePrice;
        this.schoolDistrict = schoolDistrict;
        this.elementaryschool = elementaryschool;
        this.middleSchool = middleSchool;
        this.highSchool = highSchool;
        this.qualityScoreMember = qualityScoreMember;
        this.qualityScoreSales = qualityScoreSales;
        this.qualityScore1 = qualityScore1;
        this.qualityScore2 = qualityScore2;
        this.qualityScore3 = qualityScore3;
        this.qualityScore4 = qualityScore4;
        this.qualityScore5 = qualityScore5;
        this.qualityScore6 = qualityScore6;
        this.qualityScore7 = qualityScore7;
        this.qualityScore8 = qualityScore8;
        this.qualityScore9 = qualityScore9;
        this.qualityScore10 = qualityScore10;
        this.sortAmount = sortAmount;
        this.homeScore = homeScore;
        this.investorScore = investorScore;
        this.addressId = addressId;
        this.estimatedValue = estimatedValue;
        this.estimatedLow = estimatedLow;
        this.estimatedHigh = estimatedHigh;
        this.estimatedConfidence = estimatedConfidence;
        this.estimatedValueDate = estimatedValueDate;
        this.rent = rent;
        this.originalLoanAmount = originalLoanAmount;
        this.amount = amount;
        this.transferValue = transferValue;
        this.estimatedRent = estimatedRent;
        this.foundation = foundation;
        this.roomList = roomList;
        this.appliances = appliances;
        this.floorCovering = floorCovering;
        this.roofType = roofType;
        this.viewType = viewType;
        this.tracking = tracking;
        this.paidListing = paidListing;
        this.firstLook = firstLook;
        this.hundredDownPayment = hundredDownPayment;
        this.hmpthMortgage = hmpthMortgage;
        this.hmpthRenovationMortgage = hmpthRenovationMortgage;
        this.priceReduction = priceReduction;
        this.gnndExpDate = gnndExpDate;
        this.nspHome = nspHome;
        this.specialtyHome = specialtyHome;
        this.firstMergeDate = firstMergeDate;
        this.altFinance = altFinance;
        this.estimatedMonthlyPayment = estimatedMonthlyPayment;
        this.monthlyAmountRTOHenrysAlgo = monthlyAmountRTOHenrysAlgo;
        this.monthlyAmount30yearFixedMortgage = monthlyAmount30yearFixedMortgage;
        this.dataSources = dataSources;
        this.exclusive = exclusive;
        this.specialFinancing = specialFinancing;
        this.bargainPrice = bargainPrice;
        this.fixerUpper = fixerUpper;
        this.rtoPotential = rtoPotential;
        this.rtoFinancing = rtoFinancing;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    public Integer getListPrice() {
        return listPrice;
    }

    public void setListPrice(Integer listPrice) {
        this.listPrice = listPrice;
    }

    public String getAmountDefinition() {
        return amountDefinition;
    }

    public void setAmountDefinition(String amountDefinition) {
        this.amountDefinition = amountDefinition;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getFullStreetName() {
        return fullStreetName;
    }

    public void setFullStreetName(String fullStreetName) {
        this.fullStreetName = fullStreetName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getStateOrProvince() {
        return stateOrProvince;
    }

    public void setStateOrProvince(String stateOrProvince) {
        this.stateOrProvince = stateOrProvince;
    }

    public String getPostal_Code() {
        return postal_Code;
    }

    public void setPostal_Code(String postal_Code) {
        this.postal_Code = postal_Code;
    }

    public String getUrlHex() {
        return urlHex;
    }

    public void setUrlHex(String urlHex) {
        this.urlHex = urlHex;
    }

    public String getPictureDataSourceUrl() {
        return pictureDataSourceUrl;
    }

    public void setPictureDataSourceUrl(String pictureDataSourceUrl) {
        this.pictureDataSourceUrl = pictureDataSourceUrl;
    }

    public Boolean getPictureRemote() {
        return pictureRemote;
    }

    public void setPictureRemote(Boolean pictureRemote) {
        this.pictureRemote = pictureRemote;
    }

    public Integer getPictureCount() {
        return pictureCount;
    }

    public void setPictureCount(Integer pictureCount) {
        this.pictureCount = pictureCount;
    }

    public String getParcelNumber() {
        return parcelNumber;
    }

    public void setParcelNumber(String parcelNumber) {
        this.parcelNumber = parcelNumber;
    }

    public String getMlsNumber() {
        return mlsNumber;
    }

    public void setMlsNumber(String mlsNumber) {
        this.mlsNumber = mlsNumber;
    }

    public String getMlsDisclaimer() {
        return mlsDisclaimer;
    }

    public void setMlsDisclaimer(String mlsDisclaimer) {
        this.mlsDisclaimer = mlsDisclaimer;
    }

    public String getListingStatus() {
        return listingStatus;
    }

    public void setListingStatus(String listingStatus) {
        this.listingStatus = listingStatus;
    }

    public String getListingUrl() {
        return listingUrl;
    }

    public void setListingUrl(String listingUrl) {
        this.listingUrl = listingUrl;
    }

    public String getReoDocumentNumber() {
        return reoDocumentNumber;
    }

    public void setReoDocumentNumber(String reoDocumentNumber) {
        this.reoDocumentNumber = reoDocumentNumber;
    }

    public Date getReoRecordingDate() {
        return reoRecordingDate;
    }

    public void setReoRecordingDate(Date reoRecordingDate) {
        this.reoRecordingDate = reoRecordingDate;
    }

    public Integer getJudgmentAmount() {
        return judgmentAmount;
    }

    public void setJudgmentAmount(Integer judgmentAmount) {
        this.judgmentAmount = judgmentAmount;
    }

    public String getLegalDescription() {
        return legalDescription;
    }

    public void setLegalDescription(String legalDescription) {
        this.legalDescription = legalDescription;
    }

    public String getPropertyRecordType() {
        return propertyRecordType;
    }

    public void setPropertyRecordType(String propertyRecordType) {
        this.propertyRecordType = propertyRecordType;
    }

    public BigDecimal getBathsTotal() {
        return bathsTotal;
    }

    public void setBathsTotal(BigDecimal bathsTotal) {
        this.bathsTotal = bathsTotal;
    }

    public Integer getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(Integer bedrooms) {
        this.bedrooms = bedrooms;
    }

    public String getStories() {
        return stories;
    }

    public void setStories(String stories) {
        this.stories = stories;
    }

    public Integer getLivingAreaSquareFeet() {
        return livingAreaSquareFeet;
    }

    public void setLivingAreaSquareFeet(Integer livingAreaSquareFeet) {
        this.livingAreaSquareFeet = livingAreaSquareFeet;
    }

    public String getLivingArea() {
        return livingArea;
    }

    public void setLivingArea(String livingArea) {
        this.livingArea = livingArea;
    }

    public String getLotSize() {
        return lotSize;
    }

    public void setLotSize(String lotSize) {
        this.lotSize = lotSize;
    }

    public Date getYearBuilt() {
        return YearBuilt;
    }

    public void setYearBuilt(Date yearBuilt) {
        YearBuilt = yearBuilt;
    }

    public Integer getTotalUnits() {
        return totalUnits;
    }

    public void setTotalUnits(Integer totalUnits) {
        this.totalUnits = totalUnits;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public Integer getTotalRooms() {
        return totalRooms;
    }

    public void setTotalRooms(Integer totalRooms) {
        this.totalRooms = totalRooms;
    }

    public String getDen() {
        return den;
    }

    public void setDen(String den) {
        this.den = den;
    }

    public String getKitchen() {
        return kitchen;
    }

    public void setKitchen(String kitchen) {
        this.kitchen = kitchen;
    }

    public String getLivingRoom() {
        return livingRoom;
    }

    public void setLivingRoom(String livingRoom) {
        this.livingRoom = livingRoom;
    }

    public String getFamilyRoom() {
        return familyRoom;
    }

    public void setFamilyRoom(String familyRoom) {
        this.familyRoom = familyRoom;
    }

    public String getBasement() {
        return basement;
    }

    public void setBasement(String basement) {
        this.basement = basement;
    }

    public String getHeating() {
        return heating;
    }

    public void setHeating(String heating) {
        this.heating = heating;
    }

    public String getCooling() {
        return cooling;
    }

    public void setCooling(String cooling) {
        this.cooling = cooling;
    }

    public String getParking() {
        return parking;
    }

    public void setParking(String parking) {
        this.parking = parking;
    }

    public String getParkingSpaces() {
        return parkingSpaces;
    }

    public void setParkingSpaces(String parkingSpaces) {
        this.parkingSpaces = parkingSpaces;
    }

    public String getZoning() {
        return zoning;
    }

    public void setZoning(String zoning) {
        this.zoning = zoning;
    }

    public Date getListDate() {
        return listDate;
    }

    public void setListDate(Date listDate) {
        this.listDate = listDate;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getPool() {
        return pool;
    }

    public void setPool(String pool) {
        this.pool = pool;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getConstructionType() {
        return constructionType;
    }

    public void setConstructionType(String constructionType) {
        this.constructionType = constructionType;
    }

    public String getExteriorWalls() {
        return exteriorWalls;
    }

    public void setExteriorWalls(String exteriorWalls) {
        this.exteriorWalls = exteriorWalls;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }

    public Date getNodDateDefaultedLien() {
        return nodDateDefaultedLien;
    }

    public void setNodDateDefaultedLien(Date nodDateDefaultedLien) {
        this.nodDateDefaultedLien = nodDateDefaultedLien;
    }

    public Integer getNodDocNumberDefaultedLien() {
        return nodDocNumberDefaultedLien;
    }

    public void setNodDocNumberDefaultedLien(Integer nodDocNumberDefaultedLien) {
        this.nodDocNumberDefaultedLien = nodDocNumberDefaultedLien;
    }

    public Double getNodAmountDefault() {
        return nodAmountDefault;
    }

    public void setNodAmountDefault(Double nodAmountDefault) {
        this.nodAmountDefault = nodAmountDefault;
    }

    public Date getNodDateDefault() {
        return nodDateDefault;
    }

    public void setNodDateDefault(Date nodDateDefault) {
        this.nodDateDefault = nodDateDefault;
    }

    public Date getNodRecordingDate() {
        return nodRecordingDate;
    }

    public void setNodRecordingDate(Date nodRecordingDate) {
        this.nodRecordingDate = nodRecordingDate;
    }

    public Integer getNodRecordingYear() {
        return nodRecordingYear;
    }

    public void setNodRecordingYear(Integer nodRecordingYear) {
        this.nodRecordingYear = nodRecordingYear;
    }

    public String getNodDocumentNumber() {
        return nodDocumentNumber;
    }

    public void setNodDocumentNumber(String nodDocumentNumber) {
        this.nodDocumentNumber = nodDocumentNumber;
    }

    public String getTaxSaleJurisdictionName() {
        return taxSaleJurisdictionName;
    }

    public void setTaxSaleJurisdictionName(String taxSaleJurisdictionName) {
        this.taxSaleJurisdictionName = taxSaleJurisdictionName;
    }

    public String getTaxSaleHowOften() {
        return taxSaleHowOften;
    }

    public void setTaxSaleHowOften(String taxSaleHowOften) {
        this.taxSaleHowOften = taxSaleHowOften;
    }

    public String getTaxSaleRate() {
        return taxSaleRate;
    }

    public void setTaxSaleRate(String taxSaleRate) {
        this.taxSaleRate = taxSaleRate;
    }

    public String getTaxSaleRedemtionPeriod() {
        return taxSaleRedemtionPeriod;
    }

    public void setTaxSaleRedemtionPeriod(String taxSaleRedemtionPeriod) {
        this.taxSaleRedemtionPeriod = taxSaleRedemtionPeriod;
    }

    public String getTaxSaleBidMethod() {
        return taxSaleBidMethod;
    }

    public void setTaxSaleBidMethod(String taxSaleBidMethod) {
        this.taxSaleBidMethod = taxSaleBidMethod;
    }

    public Boolean getTaxSaleGetPremiumBack() {
        return taxSaleGetPremiumBack;
    }

    public void setTaxSaleGetPremiumBack(Boolean taxSaleGetPremiumBack) {
        this.taxSaleGetPremiumBack = taxSaleGetPremiumBack;
    }

    public Date getAuctionDate() {
        return auctionDate;
    }

    public void setAuctionDate(Date auctionDate) {
        this.auctionDate = auctionDate;
    }

    public Time getAuctionTime() {
        return auctionTime;
    }

    public void setAuctionTime(Time auctionTime) {
        this.auctionTime = auctionTime;
    }

    public Double getNotsOpeningBid() {
        return notsOpeningBid;
    }

    public void setNotsOpeningBid(Double notsOpeningBid) {
        this.notsOpeningBid = notsOpeningBid;
    }

    public String getNotsAuctionTitle() {
        return notsAuctionTitle;
    }

    public void setNotsAuctionTitle(String notsAuctionTitle) {
        this.notsAuctionTitle = notsAuctionTitle;
    }

    public String getNotsAuctionAddress() {
        return notsAuctionAddress;
    }

    public void setNotsAuctionAddress(String notsAuctionAddress) {
        this.notsAuctionAddress = notsAuctionAddress;
    }

    public String getNotsAuctionCity() {
        return notsAuctionCity;
    }

    public void setNotsAuctionCity(String notsAuctionCity) {
        this.notsAuctionCity = notsAuctionCity;
    }

    public String getNotsAuctionState() {
        return notsAuctionState;
    }

    public void setNotsAuctionState(String notsAuctionState) {
        this.notsAuctionState = notsAuctionState;
    }

    public Integer getNotsAuctionZIP() {
        return notsAuctionZIP;
    }

    public void setNotsAuctionZIP(Integer notsAuctionZIP) {
        this.notsAuctionZIP = notsAuctionZIP;
    }

    public String getNotsAuctionHouseName() {
        return notsAuctionHouseName;
    }

    public void setNotsAuctionHouseName(String notsAuctionHouseName) {
        this.notsAuctionHouseName = notsAuctionHouseName;
    }

    public String getNotsAuctionDescription() {
        return notsAuctionDescription;
    }

    public void setNotsAuctionDescription(String notsAuctionDescription) {
        this.notsAuctionDescription = notsAuctionDescription;
    }

    public String getNotsAuctionTerms() {
        return notsAuctionTerms;
    }

    public void setNotsAuctionTerms(String notsAuctionTerms) {
        this.notsAuctionTerms = notsAuctionTerms;
    }

    public Date getNotsLoanDate() {
        return notsLoanDate;
    }

    public void setNotsLoanDate(Date notsLoanDate) {
        this.notsLoanDate = notsLoanDate;
    }

    public String getNotsLoanNo() {
        return notsLoanNo;
    }

    public void setNotsLoanNo(String notsLoanNo) {
        this.notsLoanNo = notsLoanNo;
    }

    public Date getNotsAuctionRecordingDate() {
        return notsAuctionRecordingDate;
    }

    public void setNotsAuctionRecordingDate(Date notsAuctionRecordingDate) {
        this.notsAuctionRecordingDate = notsAuctionRecordingDate;
    }

    public String getNotsAuctionDocumentNumber() {
        return notsAuctionDocumentNumber;
    }

    public void setNotsAuctionDocumentNumber(String notsAuctionDocumentNumber) {
        this.notsAuctionDocumentNumber = notsAuctionDocumentNumber;
    }

    public String getNotsTrusteeSaleNumber() {
        return notsTrusteeSaleNumber;
    }

    public void setNotsTrusteeSaleNumber(String notsTrusteeSaleNumber) {
        this.notsTrusteeSaleNumber = notsTrusteeSaleNumber;
    }

    public String getNotsIndexNo() {
        return notsIndexNo;
    }

    public void setNotsIndexNo(String notsIndexNo) {
        this.notsIndexNo = notsIndexNo;
    }

    public Double getNotsLoanDefaultAmount() {
        return NotsLoanDefaultAmount;
    }

    public void setNotsLoanDefaultAmount(Double notsLoanDefaultAmount) {
        NotsLoanDefaultAmount = notsLoanDefaultAmount;
    }

    public String getLispendDocketNo() {
        return lispendDocketNo;
    }

    public void setLispendDocketNo(String lispendDocketNo) {
        this.lispendDocketNo = lispendDocketNo;
    }

    public String getLispendDocNo() {
        return lispendDocNo;
    }

    public void setLispendDocNo(String lispendDocNo) {
        this.lispendDocNo = lispendDocNo;
    }

    public Date getLispendRecordingDate() {
        return lispendRecordingDate;
    }

    public void setLispendRecordingDate(Date lispendRecordingDate) {
        this.lispendRecordingDate = lispendRecordingDate;
    }

    public String getLispendIndexNo() {
        return lispendIndexNo;
    }

    public void setLispendIndexNo(String lispendIndexNo) {
        this.lispendIndexNo = lispendIndexNo;
    }

    public String getLispendLoanNo() {
        return lispendLoanNo;
    }

    public void setLispendLoanNo(String lispendLoanNo) {
        this.lispendLoanNo = lispendLoanNo;
    }

    public Date getLispendLoanDate() {
        return lispendLoanDate;
    }

    public void setLispendLoanDate(Date lispendLoanDate) {
        this.lispendLoanDate = lispendLoanDate;
    }

    public String getLispendCaseNo() {
        return lispendCaseNo;
    }

    public void setLispendCaseNo(String lispendCaseNo) {
        this.lispendCaseNo = lispendCaseNo;
    }

    public String getHud203kEligible() {
        return hud203kEligible;
    }

    public void setHud203kEligible(String hud203kEligible) {
        this.hud203kEligible = hud203kEligible;
    }

    public String getHudBidDeadline() {
        return hudBidDeadline;
    }

    public void setHudBidDeadline(String hudBidDeadline) {
        this.hudBidDeadline = hudBidDeadline;
    }

    public String getHudPriority() {
        return hudPriority;
    }

    public void setHudPriority(String hudPriority) {
        this.hudPriority = hudPriority;
    }

    public String getHudSaleStatus() {
        return hudSaleStatus;
    }

    public void setHudSaleStatus(String hudSaleStatus) {
        this.hudSaleStatus = hudSaleStatus;
    }

    public Integer getHudEscrowAmount() {
        return hudEscrowAmount;
    }

    public void setHudEscrowAmount(Integer hudEscrowAmount) {
        this.hudEscrowAmount = hudEscrowAmount;
    }

    public Integer getAssessedValuationLand() {
        return assessedValuationLand;
    }

    public void setAssessedValuationLand(Integer assessedValuationLand) {
        this.assessedValuationLand = assessedValuationLand;
    }

    public Integer getAssessedValuationImprovement() {
        return assessedValuationImprovement;
    }

    public void setAssessedValuationImprovement(Integer assessedValuationImprovement) {
        this.assessedValuationImprovement = assessedValuationImprovement;
    }

    public Integer getAssessedValuation() {
        return assessedValuation;
    }

    public void setAssessedValuation(Integer assessedValuation) {
        this.assessedValuation = assessedValuation;
    }

    public Integer getAssessedValuationYear() {
        return assessedValuationYear;
    }

    public void setAssessedValuationYear(Integer assessedValuationYear) {
        this.assessedValuationYear = assessedValuationYear;
    }

    public Integer getAssessedTax() {
        return assessedTax;
    }

    public void setAssessedTax(Integer assessedTax) {
        this.assessedTax = assessedTax;
    }

    public Integer getAssessedTaxYear() {
        return assessedTaxYear;
    }

    public void setAssessedTaxYear(Integer assessedTaxYear) {
        this.assessedTaxYear = assessedTaxYear;
    }

    public Integer getAssessedYearDelinquent() {
        return assessedYearDelinquent;
    }

    public void setAssessedYearDelinquent(Integer assessedYearDelinquent) {
        this.assessedYearDelinquent = assessedYearDelinquent;
    }

    public Double getAssessedValuationRate() {
        return assessedValuationRate;
    }

    public void setAssessedValuationRate(Double assessedValuationRate) {
        this.assessedValuationRate = assessedValuationRate;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getFips() {
        return fips;
    }

    public void setFips(String fips) {
        this.fips = fips;
    }

    public String getHoaFee() {
        return hoaFee;
    }

    public void setHoaFee(String hoaFee) {
        this.hoaFee = hoaFee;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public Integer getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(Integer closePrice) {
        this.closePrice = closePrice;
    }

    public String getSchoolDistrict() {
        return schoolDistrict;
    }

    public void setSchoolDistrict(String schoolDistrict) {
        this.schoolDistrict = schoolDistrict;
    }

    public String getElementaryschool() {
        return elementaryschool;
    }

    public void setElementaryschool(String elementaryschool) {
        this.elementaryschool = elementaryschool;
    }

    public String getMiddleSchool() {
        return middleSchool;
    }

    public void setMiddleSchool(String middleSchool) {
        this.middleSchool = middleSchool;
    }

    public String getHighSchool() {
        return highSchool;
    }

    public void setHighSchool(String highSchool) {
        this.highSchool = highSchool;
    }

    public Integer getQualityScoreMember() {
        return qualityScoreMember;
    }

    public void setQualityScoreMember(Integer qualityScoreMember) {
        this.qualityScoreMember = qualityScoreMember;
    }

    public Integer getQualityScoreSales() {
        return qualityScoreSales;
    }

    public void setQualityScoreSales(Integer qualityScoreSales) {
        this.qualityScoreSales = qualityScoreSales;
    }

    public Integer getQualityScore1() {
        return qualityScore1;
    }

    public void setQualityScore1(Integer qualityScore1) {
        this.qualityScore1 = qualityScore1;
    }

    public Integer getQualityScore2() {
        return qualityScore2;
    }

    public void setQualityScore2(Integer qualityScore2) {
        this.qualityScore2 = qualityScore2;
    }

    public Integer getQualityScore3() {
        return qualityScore3;
    }

    public void setQualityScore3(Integer qualityScore3) {
        this.qualityScore3 = qualityScore3;
    }

    public Integer getQualityScore4() {
        return qualityScore4;
    }

    public void setQualityScore4(Integer qualityScore4) {
        this.qualityScore4 = qualityScore4;
    }

    public Integer getQualityScore5() {
        return qualityScore5;
    }

    public void setQualityScore5(Integer qualityScore5) {
        this.qualityScore5 = qualityScore5;
    }

    public Integer getQualityScore6() {
        return qualityScore6;
    }

    public void setQualityScore6(Integer qualityScore6) {
        this.qualityScore6 = qualityScore6;
    }

    public Integer getQualityScore7() {
        return qualityScore7;
    }

    public void setQualityScore7(Integer qualityScore7) {
        this.qualityScore7 = qualityScore7;
    }

    public Integer getQualityScore8() {
        return qualityScore8;
    }

    public void setQualityScore8(Integer qualityScore8) {
        this.qualityScore8 = qualityScore8;
    }

    public Integer getQualityScore9() {
        return qualityScore9;
    }

    public void setQualityScore9(Integer qualityScore9) {
        this.qualityScore9 = qualityScore9;
    }

    public Integer getQualityScore10() {
        return qualityScore10;
    }

    public void setQualityScore10(Integer qualityScore10) {
        this.qualityScore10 = qualityScore10;
    }

    public Integer getSortAmount() {
        return sortAmount;
    }

    public void setSortAmount(Integer sortAmount) {
        this.sortAmount = sortAmount;
    }

    public Integer getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(Integer homeScore) {
        this.homeScore = homeScore;
    }

    public Integer getInvestorScore() {
        return investorScore;
    }

    public void setInvestorScore(Integer investorScore) {
        this.investorScore = investorScore;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public Integer getEstimatedValue() {
        return estimatedValue;
    }

    public void setEstimatedValue(Integer estimatedValue) {
        this.estimatedValue = estimatedValue;
    }

    public Integer getEstimatedLow() {
        return estimatedLow;
    }

    public void setEstimatedLow(Integer estimatedLow) {
        this.estimatedLow = estimatedLow;
    }

    public Integer getEstimatedHigh() {
        return estimatedHigh;
    }

    public void setEstimatedHigh(Integer estimatedHigh) {
        this.estimatedHigh = estimatedHigh;
    }

    public Double getEstimatedConfidence() {
        return estimatedConfidence;
    }

    public void setEstimatedConfidence(Double estimatedConfidence) {
        this.estimatedConfidence = estimatedConfidence;
    }

    public Timestamp getEstimatedValueDate() {
        return estimatedValueDate;
    }

    public void setEstimatedValueDate(Timestamp estimatedValueDate) {
        this.estimatedValueDate = estimatedValueDate;
    }

    public Integer getRent() {
        return rent;
    }

    public void setRent(Integer rent) {
        this.rent = rent;
    }

    public Integer getOriginalLoanAmount() {
        return originalLoanAmount;
    }

    public void setOriginalLoanAmount(Integer originalLoanAmount) {
        this.originalLoanAmount = originalLoanAmount;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getTransferValue() {
        return transferValue;
    }

    public void setTransferValue(Integer transferValue) {
        this.transferValue = transferValue;
    }

    public Integer getEstimatedRent() {
        return estimatedRent;
    }

    public void setEstimatedRent(Integer estimatedRent) {
        this.estimatedRent = estimatedRent;
    }

    public String getFoundation() {
        return foundation;
    }

    public void setFoundation(String foundation) {
        this.foundation = foundation;
    }

    public String getRoomList() {
        return roomList;
    }

    public void setRoomList(String roomList) {
        this.roomList = roomList;
    }

    public String getAppliances() {
        return appliances;
    }

    public void setAppliances(String appliances) {
        this.appliances = appliances;
    }

    public String getFloorCovering() {
        return floorCovering;
    }

    public void setFloorCovering(String floorCovering) {
        this.floorCovering = floorCovering;
    }

    public String getRoofType() {
        return roofType;
    }

    public void setRoofType(String roofType) {
        this.roofType = roofType;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public String getTracking() {
        return tracking;
    }

    public void setTracking(String tracking) {
        this.tracking = tracking;
    }

    public Boolean getPaidListing() {
        return paidListing;
    }

    public void setPaidListing(Boolean paidListing) {
        this.paidListing = paidListing;
    }

    public Date getFirstLook() {
        return firstLook;
    }

    public void setFirstLook(Date firstLook) {
        this.firstLook = firstLook;
    }

    public Boolean getHundredDownPayment() {
        return hundredDownPayment;
    }

    public void setHundredDownPayment(Boolean hundredDownPayment) {
        this.hundredDownPayment = hundredDownPayment;
    }

    public Boolean getHmpthMortgage() {
        return hmpthMortgage;
    }

    public void setHmpthMortgage(Boolean hmpthMortgage) {
        this.hmpthMortgage = hmpthMortgage;
    }

    public Boolean getHmpthRenovationMortgage() {
        return hmpthRenovationMortgage;
    }

    public void setHmpthRenovationMortgage(Boolean hmpthRenovationMortgage) {
        this.hmpthRenovationMortgage = hmpthRenovationMortgage;
    }

    public Boolean getPriceReduction() {
        return priceReduction;
    }

    public void setPriceReduction(Boolean priceReduction) {
        this.priceReduction = priceReduction;
    }

    public Date getGnndExpDate() {
        return gnndExpDate;
    }

    public void setGnndExpDate(Date gnndExpDate) {
        this.gnndExpDate = gnndExpDate;
    }

    public Boolean getNspHome() {
        return nspHome;
    }

    public void setNspHome(Boolean nspHome) {
        this.nspHome = nspHome;
    }

    public Boolean getSpecialtyHome() {
        return specialtyHome;
    }

    public void setSpecialtyHome(Boolean specialtyHome) {
        this.specialtyHome = specialtyHome;
    }

    public Timestamp getFirstMergeDate() {
        return firstMergeDate;
    }

    public void setFirstMergeDate(Timestamp firstMergeDate) {
        this.firstMergeDate = firstMergeDate;
    }

    public Boolean getAltFinance() {
        return altFinance;
    }

    public void setAltFinance(Boolean altFinance) {
        this.altFinance = altFinance;
    }

    public Integer getEstimatedMonthlyPayment() {
        return estimatedMonthlyPayment;
    }

    public void setEstimatedMonthlyPayment(Integer estimatedMonthlyPayment) {
        this.estimatedMonthlyPayment = estimatedMonthlyPayment;
    }

    public Integer getMonthlyAmountRTOHenrysAlgo() {
        return monthlyAmountRTOHenrysAlgo;
    }

    public void setMonthlyAmountRTOHenrysAlgo(Integer monthlyAmountRTOHenrysAlgo) {
        this.monthlyAmountRTOHenrysAlgo = monthlyAmountRTOHenrysAlgo;
    }

    public Integer getMonthlyAmount30yearFixedMortgage() {
        return monthlyAmount30yearFixedMortgage;
    }

    public void setMonthlyAmount30yearFixedMortgage(Integer monthlyAmount30yearFixedMortgage) {
        this.monthlyAmount30yearFixedMortgage = monthlyAmount30yearFixedMortgage;
    }

    public String getDataSources() {
        return dataSources;
    }

    public void setDataSources(String dataSources) {
        this.dataSources = dataSources;
    }

    public Boolean getExclusive() {
        return exclusive;
    }

    public void setExclusive(Boolean exclusive) {
        this.exclusive = exclusive;
    }

    public Boolean getSpecialFinancing() {
        return specialFinancing;
    }

    public void setSpecialFinancing(Boolean specialFinancing) {
        this.specialFinancing = specialFinancing;
    }

    public Boolean getBargainPrice() {
        return bargainPrice;
    }

    public void setBargainPrice(Boolean bargainPrice) {
        this.bargainPrice = bargainPrice;
    }

    public Boolean getFixerUpper() {
        return fixerUpper;
    }

    public void setFixerUpper(Boolean fixerUpper) {
        this.fixerUpper = fixerUpper;
    }

    public Boolean getRtoPotential() {
        return rtoPotential;
    }

    public void setRtoPotential(Boolean rtoPotential) {
        this.rtoPotential = rtoPotential;
    }

    public Boolean getRtoFinancing() {
        return rtoFinancing;
    }

    public void setRtoFinancing(Boolean rtoFinancing) {
        this.rtoFinancing = rtoFinancing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Property)) return false;

        Property property = (Property) o;

        if (!getPropertyId().equals(property.getPropertyId())) return false;
        if (getListPrice() != null ? !getListPrice().equals(property.getListPrice()) : property.getListPrice() != null)
            return false;
        if (!getAmountDefinition().equals(property.getAmountDefinition())) return false;
        if (getStreetAddress() != null ? !getStreetAddress().equals(property.getStreetAddress()) : property.getStreetAddress() != null)
            return false;
        if (getFullStreetName() != null ? !getFullStreetName().equals(property.getFullStreetName()) : property.getFullStreetName() != null)
            return false;
        if (getCity() != null ? !getCity().equals(property.getCity()) : property.getCity() != null) return false;
        if (!getCounty().equals(property.getCounty())) return false;
        if (!getStateOrProvince().equals(property.getStateOrProvince())) return false;
        if (getPostal_Code() != null ? !getPostal_Code().equals(property.getPostal_Code()) : property.getPostal_Code() != null)
            return false;
        if (getUrlHex() != null ? !getUrlHex().equals(property.getUrlHex()) : property.getUrlHex() != null)
            return false;
        if (getPictureDataSourceUrl() != null ? !getPictureDataSourceUrl().equals(property.getPictureDataSourceUrl()) : property.getPictureDataSourceUrl() != null)
            return false;
        if (!getPictureRemote().equals(property.getPictureRemote())) return false;
        if (getPictureCount() != null ? !getPictureCount().equals(property.getPictureCount()) : property.getPictureCount() != null)
            return false;
        if (getParcelNumber() != null ? !getParcelNumber().equals(property.getParcelNumber()) : property.getParcelNumber() != null)
            return false;
        if (getMlsNumber() != null ? !getMlsNumber().equals(property.getMlsNumber()) : property.getMlsNumber() != null)
            return false;
        if (getMlsDisclaimer() != null ? !getMlsDisclaimer().equals(property.getMlsDisclaimer()) : property.getMlsDisclaimer() != null)
            return false;
        if (getListingStatus() != null ? !getListingStatus().equals(property.getListingStatus()) : property.getListingStatus() != null)
            return false;
        if (getListingUrl() != null ? !getListingUrl().equals(property.getListingUrl()) : property.getListingUrl() != null)
            return false;
        if (getReoDocumentNumber() != null ? !getReoDocumentNumber().equals(property.getReoDocumentNumber()) : property.getReoDocumentNumber() != null)
            return false;
        if (getReoRecordingDate() != null ? !getReoRecordingDate().equals(property.getReoRecordingDate()) : property.getReoRecordingDate() != null)
            return false;
        if (getJudgmentAmount() != null ? !getJudgmentAmount().equals(property.getJudgmentAmount()) : property.getJudgmentAmount() != null)
            return false;
        if (getLegalDescription() != null ? !getLegalDescription().equals(property.getLegalDescription()) : property.getLegalDescription() != null)
            return false;
        if (getPropertyRecordType() != null ? !getPropertyRecordType().equals(property.getPropertyRecordType()) : property.getPropertyRecordType() != null)
            return false;
        if (getBathsTotal() != null ? !getBathsTotal().equals(property.getBathsTotal()) : property.getBathsTotal() != null)
            return false;
        if (getBedrooms() != null ? !getBedrooms().equals(property.getBedrooms()) : property.getBedrooms() != null)
            return false;
        if (getStories() != null ? !getStories().equals(property.getStories()) : property.getStories() != null)
            return false;
        if (getLivingAreaSquareFeet() != null ? !getLivingAreaSquareFeet().equals(property.getLivingAreaSquareFeet()) : property.getLivingAreaSquareFeet() != null)
            return false;
        if (getLivingArea() != null ? !getLivingArea().equals(property.getLivingArea()) : property.getLivingArea() != null)
            return false;
        if (getLotSize() != null ? !getLotSize().equals(property.getLotSize()) : property.getLotSize() != null)
            return false;
        if (getYearBuilt() != null ? !getYearBuilt().equals(property.getYearBuilt()) : property.getYearBuilt() != null)
            return false;
        if (getTotalUnits() != null ? !getTotalUnits().equals(property.getTotalUnits()) : property.getTotalUnits() != null)
            return false;
        if (getUnitNumber() != null ? !getUnitNumber().equals(property.getUnitNumber()) : property.getUnitNumber() != null)
            return false;
        if (getCondition() != null ? !getCondition().equals(property.getCondition()) : property.getCondition() != null)
            return false;
        if (getTotalRooms() != null ? !getTotalRooms().equals(property.getTotalRooms()) : property.getTotalRooms() != null)
            return false;
        if (getDen() != null ? !getDen().equals(property.getDen()) : property.getDen() != null) return false;
        if (getKitchen() != null ? !getKitchen().equals(property.getKitchen()) : property.getKitchen() != null)
            return false;
        if (getLivingRoom() != null ? !getLivingRoom().equals(property.getLivingRoom()) : property.getLivingRoom() != null)
            return false;
        if (getFamilyRoom() != null ? !getFamilyRoom().equals(property.getFamilyRoom()) : property.getFamilyRoom() != null)
            return false;
        if (getBasement() != null ? !getBasement().equals(property.getBasement()) : property.getBasement() != null)
            return false;
        if (getHeating() != null ? !getHeating().equals(property.getHeating()) : property.getHeating() != null)
            return false;
        if (getCooling() != null ? !getCooling().equals(property.getCooling()) : property.getCooling() != null)
            return false;
        if (getParking() != null ? !getParking().equals(property.getParking()) : property.getParking() != null)
            return false;
        if (getParkingSpaces() != null ? !getParkingSpaces().equals(property.getParkingSpaces()) : property.getParkingSpaces() != null)
            return false;
        if (getZoning() != null ? !getZoning().equals(property.getZoning()) : property.getZoning() != null)
            return false;
        if (getListDate() != null ? !getListDate().equals(property.getListDate()) : property.getListDate() != null)
            return false;
        if (getFeatures() != null ? !getFeatures().equals(property.getFeatures()) : property.getFeatures() != null)
            return false;
        if (getPool() != null ? !getPool().equals(property.getPool()) : property.getPool() != null) return false;
        if (getStyle() != null ? !getStyle().equals(property.getStyle()) : property.getStyle() != null) return false;
        if (getConstructionType() != null ? !getConstructionType().equals(property.getConstructionType()) : property.getConstructionType() != null)
            return false;
        if (getExteriorWalls() != null ? !getExteriorWalls().equals(property.getExteriorWalls()) : property.getExteriorWalls() != null)
            return false;
        if (getDescription() != null ? !getDescription().equals(property.getDescription()) : property.getDescription() != null)
            return false;
        if (getSaleType() != null ? !getSaleType().equals(property.getSaleType()) : property.getSaleType() != null)
            return false;
        if (getNodDateDefaultedLien() != null ? !getNodDateDefaultedLien().equals(property.getNodDateDefaultedLien()) : property.getNodDateDefaultedLien() != null)
            return false;
        if (getNodDocNumberDefaultedLien() != null ? !getNodDocNumberDefaultedLien().equals(property.getNodDocNumberDefaultedLien()) : property.getNodDocNumberDefaultedLien() != null)
            return false;
        if (getNodAmountDefault() != null ? !getNodAmountDefault().equals(property.getNodAmountDefault()) : property.getNodAmountDefault() != null)
            return false;
        if (getNodDateDefault() != null ? !getNodDateDefault().equals(property.getNodDateDefault()) : property.getNodDateDefault() != null)
            return false;
        if (getNodRecordingDate() != null ? !getNodRecordingDate().equals(property.getNodRecordingDate()) : property.getNodRecordingDate() != null)
            return false;
        if (getNodRecordingYear() != null ? !getNodRecordingYear().equals(property.getNodRecordingYear()) : property.getNodRecordingYear() != null)
            return false;
        if (getNodDocumentNumber() != null ? !getNodDocumentNumber().equals(property.getNodDocumentNumber()) : property.getNodDocumentNumber() != null)
            return false;
        if (getTaxSaleJurisdictionName() != null ? !getTaxSaleJurisdictionName().equals(property.getTaxSaleJurisdictionName()) : property.getTaxSaleJurisdictionName() != null)
            return false;
        if (getTaxSaleHowOften() != null ? !getTaxSaleHowOften().equals(property.getTaxSaleHowOften()) : property.getTaxSaleHowOften() != null)
            return false;
        if (getTaxSaleRate() != null ? !getTaxSaleRate().equals(property.getTaxSaleRate()) : property.getTaxSaleRate() != null)
            return false;
        if (getTaxSaleRedemtionPeriod() != null ? !getTaxSaleRedemtionPeriod().equals(property.getTaxSaleRedemtionPeriod()) : property.getTaxSaleRedemtionPeriod() != null)
            return false;
        if (getTaxSaleBidMethod() != null ? !getTaxSaleBidMethod().equals(property.getTaxSaleBidMethod()) : property.getTaxSaleBidMethod() != null)
            return false;
        if (getTaxSaleGetPremiumBack() != null ? !getTaxSaleGetPremiumBack().equals(property.getTaxSaleGetPremiumBack()) : property.getTaxSaleGetPremiumBack() != null)
            return false;
        if (getAuctionDate() != null ? !getAuctionDate().equals(property.getAuctionDate()) : property.getAuctionDate() != null)
            return false;
        if (getAuctionTime() != null ? !getAuctionTime().equals(property.getAuctionTime()) : property.getAuctionTime() != null)
            return false;
        if (getNotsOpeningBid() != null ? !getNotsOpeningBid().equals(property.getNotsOpeningBid()) : property.getNotsOpeningBid() != null)
            return false;
        if (getNotsAuctionTitle() != null ? !getNotsAuctionTitle().equals(property.getNotsAuctionTitle()) : property.getNotsAuctionTitle() != null)
            return false;
        if (getNotsAuctionAddress() != null ? !getNotsAuctionAddress().equals(property.getNotsAuctionAddress()) : property.getNotsAuctionAddress() != null)
            return false;
        if (getNotsAuctionCity() != null ? !getNotsAuctionCity().equals(property.getNotsAuctionCity()) : property.getNotsAuctionCity() != null)
            return false;
        if (getNotsAuctionState() != null ? !getNotsAuctionState().equals(property.getNotsAuctionState()) : property.getNotsAuctionState() != null)
            return false;
        if (getNotsAuctionZIP() != null ? !getNotsAuctionZIP().equals(property.getNotsAuctionZIP()) : property.getNotsAuctionZIP() != null)
            return false;
        if (getNotsAuctionHouseName() != null ? !getNotsAuctionHouseName().equals(property.getNotsAuctionHouseName()) : property.getNotsAuctionHouseName() != null)
            return false;
        if (getNotsAuctionDescription() != null ? !getNotsAuctionDescription().equals(property.getNotsAuctionDescription()) : property.getNotsAuctionDescription() != null)
            return false;
        if (getNotsAuctionTerms() != null ? !getNotsAuctionTerms().equals(property.getNotsAuctionTerms()) : property.getNotsAuctionTerms() != null)
            return false;
        if (getNotsLoanDate() != null ? !getNotsLoanDate().equals(property.getNotsLoanDate()) : property.getNotsLoanDate() != null)
            return false;
        if (getNotsLoanNo() != null ? !getNotsLoanNo().equals(property.getNotsLoanNo()) : property.getNotsLoanNo() != null)
            return false;
        if (getNotsAuctionRecordingDate() != null ? !getNotsAuctionRecordingDate().equals(property.getNotsAuctionRecordingDate()) : property.getNotsAuctionRecordingDate() != null)
            return false;
        if (getNotsAuctionDocumentNumber() != null ? !getNotsAuctionDocumentNumber().equals(property.getNotsAuctionDocumentNumber()) : property.getNotsAuctionDocumentNumber() != null)
            return false;
        if (getNotsTrusteeSaleNumber() != null ? !getNotsTrusteeSaleNumber().equals(property.getNotsTrusteeSaleNumber()) : property.getNotsTrusteeSaleNumber() != null)
            return false;
        if (getNotsIndexNo() != null ? !getNotsIndexNo().equals(property.getNotsIndexNo()) : property.getNotsIndexNo() != null)
            return false;
        if (getNotsLoanDefaultAmount() != null ? !getNotsLoanDefaultAmount().equals(property.getNotsLoanDefaultAmount()) : property.getNotsLoanDefaultAmount() != null)
            return false;
        if (getLispendDocketNo() != null ? !getLispendDocketNo().equals(property.getLispendDocketNo()) : property.getLispendDocketNo() != null)
            return false;
        if (getLispendDocNo() != null ? !getLispendDocNo().equals(property.getLispendDocNo()) : property.getLispendDocNo() != null)
            return false;
        if (getLispendRecordingDate() != null ? !getLispendRecordingDate().equals(property.getLispendRecordingDate()) : property.getLispendRecordingDate() != null)
            return false;
        if (getLispendIndexNo() != null ? !getLispendIndexNo().equals(property.getLispendIndexNo()) : property.getLispendIndexNo() != null)
            return false;
        if (getLispendLoanNo() != null ? !getLispendLoanNo().equals(property.getLispendLoanNo()) : property.getLispendLoanNo() != null)
            return false;
        if (getLispendLoanDate() != null ? !getLispendLoanDate().equals(property.getLispendLoanDate()) : property.getLispendLoanDate() != null)
            return false;
        if (getLispendCaseNo() != null ? !getLispendCaseNo().equals(property.getLispendCaseNo()) : property.getLispendCaseNo() != null)
            return false;
        if (getHud203kEligible() != null ? !getHud203kEligible().equals(property.getHud203kEligible()) : property.getHud203kEligible() != null)
            return false;
        if (getHudBidDeadline() != null ? !getHudBidDeadline().equals(property.getHudBidDeadline()) : property.getHudBidDeadline() != null)
            return false;
        if (getHudPriority() != null ? !getHudPriority().equals(property.getHudPriority()) : property.getHudPriority() != null)
            return false;
        if (getHudSaleStatus() != null ? !getHudSaleStatus().equals(property.getHudSaleStatus()) : property.getHudSaleStatus() != null)
            return false;
        if (getHudEscrowAmount() != null ? !getHudEscrowAmount().equals(property.getHudEscrowAmount()) : property.getHudEscrowAmount() != null)
            return false;
        if (getAssessedValuationLand() != null ? !getAssessedValuationLand().equals(property.getAssessedValuationLand()) : property.getAssessedValuationLand() != null)
            return false;
        if (getAssessedValuationImprovement() != null ? !getAssessedValuationImprovement().equals(property.getAssessedValuationImprovement()) : property.getAssessedValuationImprovement() != null)
            return false;
        if (getAssessedValuation() != null ? !getAssessedValuation().equals(property.getAssessedValuation()) : property.getAssessedValuation() != null)
            return false;
        if (getAssessedValuationYear() != null ? !getAssessedValuationYear().equals(property.getAssessedValuationYear()) : property.getAssessedValuationYear() != null)
            return false;
        if (getAssessedTax() != null ? !getAssessedTax().equals(property.getAssessedTax()) : property.getAssessedTax() != null)
            return false;
        if (getAssessedTaxYear() != null ? !getAssessedTaxYear().equals(property.getAssessedTaxYear()) : property.getAssessedTaxYear() != null)
            return false;
        if (getAssessedYearDelinquent() != null ? !getAssessedYearDelinquent().equals(property.getAssessedYearDelinquent()) : property.getAssessedYearDelinquent() != null)
            return false;
        if (getAssessedValuationRate() != null ? !getAssessedValuationRate().equals(property.getAssessedValuationRate()) : property.getAssessedValuationRate() != null)
            return false;
        if (getLatitude() != null ? !getLatitude().equals(property.getLatitude()) : property.getLatitude() != null)
            return false;
        if (getLongitude() != null ? !getLongitude().equals(property.getLongitude()) : property.getLongitude() != null)
            return false;
        if (!getFips().equals(property.getFips())) return false;
        if (getHoaFee() != null ? !getHoaFee().equals(property.getHoaFee()) : property.getHoaFee() != null)
            return false;
        if (getCloseDate() != null ? !getCloseDate().equals(property.getCloseDate()) : property.getCloseDate() != null)
            return false;
        if (getClosePrice() != null ? !getClosePrice().equals(property.getClosePrice()) : property.getClosePrice() != null)
            return false;
        if (getSchoolDistrict() != null ? !getSchoolDistrict().equals(property.getSchoolDistrict()) : property.getSchoolDistrict() != null)
            return false;
        if (getElementaryschool() != null ? !getElementaryschool().equals(property.getElementaryschool()) : property.getElementaryschool() != null)
            return false;
        if (getMiddleSchool() != null ? !getMiddleSchool().equals(property.getMiddleSchool()) : property.getMiddleSchool() != null)
            return false;
        if (getHighSchool() != null ? !getHighSchool().equals(property.getHighSchool()) : property.getHighSchool() != null)
            return false;
        if (getQualityScoreMember() != null ? !getQualityScoreMember().equals(property.getQualityScoreMember()) : property.getQualityScoreMember() != null)
            return false;
        if (getQualityScoreSales() != null ? !getQualityScoreSales().equals(property.getQualityScoreSales()) : property.getQualityScoreSales() != null)
            return false;
        if (getQualityScore1() != null ? !getQualityScore1().equals(property.getQualityScore1()) : property.getQualityScore1() != null)
            return false;
        if (getQualityScore2() != null ? !getQualityScore2().equals(property.getQualityScore2()) : property.getQualityScore2() != null)
            return false;
        if (getQualityScore3() != null ? !getQualityScore3().equals(property.getQualityScore3()) : property.getQualityScore3() != null)
            return false;
        if (getQualityScore4() != null ? !getQualityScore4().equals(property.getQualityScore4()) : property.getQualityScore4() != null)
            return false;
        if (getQualityScore5() != null ? !getQualityScore5().equals(property.getQualityScore5()) : property.getQualityScore5() != null)
            return false;
        if (getQualityScore6() != null ? !getQualityScore6().equals(property.getQualityScore6()) : property.getQualityScore6() != null)
            return false;
        if (getQualityScore7() != null ? !getQualityScore7().equals(property.getQualityScore7()) : property.getQualityScore7() != null)
            return false;
        if (getQualityScore8() != null ? !getQualityScore8().equals(property.getQualityScore8()) : property.getQualityScore8() != null)
            return false;
        if (getQualityScore9() != null ? !getQualityScore9().equals(property.getQualityScore9()) : property.getQualityScore9() != null)
            return false;
        if (getQualityScore10() != null ? !getQualityScore10().equals(property.getQualityScore10()) : property.getQualityScore10() != null)
            return false;
        if (getSortAmount() != null ? !getSortAmount().equals(property.getSortAmount()) : property.getSortAmount() != null)
            return false;
        if (getHomeScore() != null ? !getHomeScore().equals(property.getHomeScore()) : property.getHomeScore() != null)
            return false;
        if (getInvestorScore() != null ? !getInvestorScore().equals(property.getInvestorScore()) : property.getInvestorScore() != null)
            return false;
        if (getAddressId() != null ? !getAddressId().equals(property.getAddressId()) : property.getAddressId() != null)
            return false;
        if (getEstimatedValue() != null ? !getEstimatedValue().equals(property.getEstimatedValue()) : property.getEstimatedValue() != null)
            return false;
        if (getEstimatedLow() != null ? !getEstimatedLow().equals(property.getEstimatedLow()) : property.getEstimatedLow() != null)
            return false;
        if (getEstimatedHigh() != null ? !getEstimatedHigh().equals(property.getEstimatedHigh()) : property.getEstimatedHigh() != null)
            return false;
        if (getEstimatedConfidence() != null ? !getEstimatedConfidence().equals(property.getEstimatedConfidence()) : property.getEstimatedConfidence() != null)
            return false;
        if (getEstimatedValueDate() != null ? !getEstimatedValueDate().equals(property.getEstimatedValueDate()) : property.getEstimatedValueDate() != null)
            return false;
        if (getRent() != null ? !getRent().equals(property.getRent()) : property.getRent() != null) return false;
        if (getOriginalLoanAmount() != null ? !getOriginalLoanAmount().equals(property.getOriginalLoanAmount()) : property.getOriginalLoanAmount() != null)
            return false;
        if (getAmount() != null ? !getAmount().equals(property.getAmount()) : property.getAmount() != null)
            return false;
        if (getTransferValue() != null ? !getTransferValue().equals(property.getTransferValue()) : property.getTransferValue() != null)
            return false;
        if (getEstimatedRent() != null ? !getEstimatedRent().equals(property.getEstimatedRent()) : property.getEstimatedRent() != null)
            return false;
        if (getFoundation() != null ? !getFoundation().equals(property.getFoundation()) : property.getFoundation() != null)
            return false;
        if (getRoomList() != null ? !getRoomList().equals(property.getRoomList()) : property.getRoomList() != null)
            return false;
        if (getAppliances() != null ? !getAppliances().equals(property.getAppliances()) : property.getAppliances() != null)
            return false;
        if (getFloorCovering() != null ? !getFloorCovering().equals(property.getFloorCovering()) : property.getFloorCovering() != null)
            return false;
        if (getRoofType() != null ? !getRoofType().equals(property.getRoofType()) : property.getRoofType() != null)
            return false;
        if (getViewType() != null ? !getViewType().equals(property.getViewType()) : property.getViewType() != null)
            return false;
        if (getTracking() != null ? !getTracking().equals(property.getTracking()) : property.getTracking() != null)
            return false;
        if (getPaidListing() != null ? !getPaidListing().equals(property.getPaidListing()) : property.getPaidListing() != null)
            return false;
        if (getFirstLook() != null ? !getFirstLook().equals(property.getFirstLook()) : property.getFirstLook() != null)
            return false;
        if (getHundredDownPayment() != null ? !getHundredDownPayment().equals(property.getHundredDownPayment()) : property.getHundredDownPayment() != null)
            return false;
        if (getHmpthMortgage() != null ? !getHmpthMortgage().equals(property.getHmpthMortgage()) : property.getHmpthMortgage() != null)
            return false;
        if (getHmpthRenovationMortgage() != null ? !getHmpthRenovationMortgage().equals(property.getHmpthRenovationMortgage()) : property.getHmpthRenovationMortgage() != null)
            return false;
        if (getPriceReduction() != null ? !getPriceReduction().equals(property.getPriceReduction()) : property.getPriceReduction() != null)
            return false;
        if (getGnndExpDate() != null ? !getGnndExpDate().equals(property.getGnndExpDate()) : property.getGnndExpDate() != null)
            return false;
        if (getNspHome() != null ? !getNspHome().equals(property.getNspHome()) : property.getNspHome() != null)
            return false;
        if (getSpecialtyHome() != null ? !getSpecialtyHome().equals(property.getSpecialtyHome()) : property.getSpecialtyHome() != null)
            return false;
        if (getFirstMergeDate() != null ? !getFirstMergeDate().equals(property.getFirstMergeDate()) : property.getFirstMergeDate() != null)
            return false;
        if (getAltFinance() != null ? !getAltFinance().equals(property.getAltFinance()) : property.getAltFinance() != null)
            return false;
        if (getEstimatedMonthlyPayment() != null ? !getEstimatedMonthlyPayment().equals(property.getEstimatedMonthlyPayment()) : property.getEstimatedMonthlyPayment() != null)
            return false;
        if (getMonthlyAmountRTOHenrysAlgo() != null ? !getMonthlyAmountRTOHenrysAlgo().equals(property.getMonthlyAmountRTOHenrysAlgo()) : property.getMonthlyAmountRTOHenrysAlgo() != null)
            return false;
        if (getMonthlyAmount30yearFixedMortgage() != null ? !getMonthlyAmount30yearFixedMortgage().equals(property.getMonthlyAmount30yearFixedMortgage()) : property.getMonthlyAmount30yearFixedMortgage() != null)
            return false;
        if (getDataSources() != null ? !getDataSources().equals(property.getDataSources()) : property.getDataSources() != null)
            return false;
        if (getExclusive() != null ? !getExclusive().equals(property.getExclusive()) : property.getExclusive() != null)
            return false;
        if (getSpecialFinancing() != null ? !getSpecialFinancing().equals(property.getSpecialFinancing()) : property.getSpecialFinancing() != null)
            return false;
        if (getBargainPrice() != null ? !getBargainPrice().equals(property.getBargainPrice()) : property.getBargainPrice() != null)
            return false;
        if (getFixerUpper() != null ? !getFixerUpper().equals(property.getFixerUpper()) : property.getFixerUpper() != null)
            return false;
        if (getRtoPotential() != null ? !getRtoPotential().equals(property.getRtoPotential()) : property.getRtoPotential() != null)
            return false;
        return getRtoFinancing() != null ? getRtoFinancing().equals(property.getRtoFinancing()) : property.getRtoFinancing() == null;
    }

    @Override
    public int hashCode() {
        int result = getPropertyId().hashCode();
        result = 31 * result + (getListPrice() != null ? getListPrice().hashCode() : 0);
        result = 31 * result + getAmountDefinition().hashCode();
        result = 31 * result + (getStreetAddress() != null ? getStreetAddress().hashCode() : 0);
        result = 31 * result + (getFullStreetName() != null ? getFullStreetName().hashCode() : 0);
        result = 31 * result + (getCity() != null ? getCity().hashCode() : 0);
        result = 31 * result + getCounty().hashCode();
        result = 31 * result + getStateOrProvince().hashCode();
        result = 31 * result + (getPostal_Code() != null ? getPostal_Code().hashCode() : 0);
        result = 31 * result + (getUrlHex() != null ? getUrlHex().hashCode() : 0);
        result = 31 * result + (getPictureDataSourceUrl() != null ? getPictureDataSourceUrl().hashCode() : 0);
        result = 31 * result + getPictureRemote().hashCode();
        result = 31 * result + (getPictureCount() != null ? getPictureCount().hashCode() : 0);
        result = 31 * result + (getParcelNumber() != null ? getParcelNumber().hashCode() : 0);
        result = 31 * result + (getMlsNumber() != null ? getMlsNumber().hashCode() : 0);
        result = 31 * result + (getMlsDisclaimer() != null ? getMlsDisclaimer().hashCode() : 0);
        result = 31 * result + (getListingStatus() != null ? getListingStatus().hashCode() : 0);
        result = 31 * result + (getListingUrl() != null ? getListingUrl().hashCode() : 0);
        result = 31 * result + (getReoDocumentNumber() != null ? getReoDocumentNumber().hashCode() : 0);
        result = 31 * result + (getReoRecordingDate() != null ? getReoRecordingDate().hashCode() : 0);
        result = 31 * result + (getJudgmentAmount() != null ? getJudgmentAmount().hashCode() : 0);
        result = 31 * result + (getLegalDescription() != null ? getLegalDescription().hashCode() : 0);
        result = 31 * result + (getPropertyRecordType() != null ? getPropertyRecordType().hashCode() : 0);
        result = 31 * result + (getBathsTotal() != null ? getBathsTotal().hashCode() : 0);
        result = 31 * result + (getBedrooms() != null ? getBedrooms().hashCode() : 0);
        result = 31 * result + (getStories() != null ? getStories().hashCode() : 0);
        result = 31 * result + (getLivingAreaSquareFeet() != null ? getLivingAreaSquareFeet().hashCode() : 0);
        result = 31 * result + (getLivingArea() != null ? getLivingArea().hashCode() : 0);
        result = 31 * result + (getLotSize() != null ? getLotSize().hashCode() : 0);
        result = 31 * result + (getYearBuilt() != null ? getYearBuilt().hashCode() : 0);
        result = 31 * result + (getTotalUnits() != null ? getTotalUnits().hashCode() : 0);
        result = 31 * result + (getUnitNumber() != null ? getUnitNumber().hashCode() : 0);
        result = 31 * result + (getCondition() != null ? getCondition().hashCode() : 0);
        result = 31 * result + (getTotalRooms() != null ? getTotalRooms().hashCode() : 0);
        result = 31 * result + (getDen() != null ? getDen().hashCode() : 0);
        result = 31 * result + (getKitchen() != null ? getKitchen().hashCode() : 0);
        result = 31 * result + (getLivingRoom() != null ? getLivingRoom().hashCode() : 0);
        result = 31 * result + (getFamilyRoom() != null ? getFamilyRoom().hashCode() : 0);
        result = 31 * result + (getBasement() != null ? getBasement().hashCode() : 0);
        result = 31 * result + (getHeating() != null ? getHeating().hashCode() : 0);
        result = 31 * result + (getCooling() != null ? getCooling().hashCode() : 0);
        result = 31 * result + (getParking() != null ? getParking().hashCode() : 0);
        result = 31 * result + (getParkingSpaces() != null ? getParkingSpaces().hashCode() : 0);
        result = 31 * result + (getZoning() != null ? getZoning().hashCode() : 0);
        result = 31 * result + (getListDate() != null ? getListDate().hashCode() : 0);
        result = 31 * result + (getFeatures() != null ? getFeatures().hashCode() : 0);
        result = 31 * result + (getPool() != null ? getPool().hashCode() : 0);
        result = 31 * result + (getStyle() != null ? getStyle().hashCode() : 0);
        result = 31 * result + (getConstructionType() != null ? getConstructionType().hashCode() : 0);
        result = 31 * result + (getExteriorWalls() != null ? getExteriorWalls().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getSaleType() != null ? getSaleType().hashCode() : 0);
        result = 31 * result + (getNodDateDefaultedLien() != null ? getNodDateDefaultedLien().hashCode() : 0);
        result = 31 * result + (getNodDocNumberDefaultedLien() != null ? getNodDocNumberDefaultedLien().hashCode() : 0);
        result = 31 * result + (getNodAmountDefault() != null ? getNodAmountDefault().hashCode() : 0);
        result = 31 * result + (getNodDateDefault() != null ? getNodDateDefault().hashCode() : 0);
        result = 31 * result + (getNodRecordingDate() != null ? getNodRecordingDate().hashCode() : 0);
        result = 31 * result + (getNodRecordingYear() != null ? getNodRecordingYear().hashCode() : 0);
        result = 31 * result + (getNodDocumentNumber() != null ? getNodDocumentNumber().hashCode() : 0);
        result = 31 * result + (getTaxSaleJurisdictionName() != null ? getTaxSaleJurisdictionName().hashCode() : 0);
        result = 31 * result + (getTaxSaleHowOften() != null ? getTaxSaleHowOften().hashCode() : 0);
        result = 31 * result + (getTaxSaleRate() != null ? getTaxSaleRate().hashCode() : 0);
        result = 31 * result + (getTaxSaleRedemtionPeriod() != null ? getTaxSaleRedemtionPeriod().hashCode() : 0);
        result = 31 * result + (getTaxSaleBidMethod() != null ? getTaxSaleBidMethod().hashCode() : 0);
        result = 31 * result + (getTaxSaleGetPremiumBack() != null ? getTaxSaleGetPremiumBack().hashCode() : 0);
        result = 31 * result + (getAuctionDate() != null ? getAuctionDate().hashCode() : 0);
        result = 31 * result + (getAuctionTime() != null ? getAuctionTime().hashCode() : 0);
        result = 31 * result + (getNotsOpeningBid() != null ? getNotsOpeningBid().hashCode() : 0);
        result = 31 * result + (getNotsAuctionTitle() != null ? getNotsAuctionTitle().hashCode() : 0);
        result = 31 * result + (getNotsAuctionAddress() != null ? getNotsAuctionAddress().hashCode() : 0);
        result = 31 * result + (getNotsAuctionCity() != null ? getNotsAuctionCity().hashCode() : 0);
        result = 31 * result + (getNotsAuctionState() != null ? getNotsAuctionState().hashCode() : 0);
        result = 31 * result + (getNotsAuctionZIP() != null ? getNotsAuctionZIP().hashCode() : 0);
        result = 31 * result + (getNotsAuctionHouseName() != null ? getNotsAuctionHouseName().hashCode() : 0);
        result = 31 * result + (getNotsAuctionDescription() != null ? getNotsAuctionDescription().hashCode() : 0);
        result = 31 * result + (getNotsAuctionTerms() != null ? getNotsAuctionTerms().hashCode() : 0);
        result = 31 * result + (getNotsLoanDate() != null ? getNotsLoanDate().hashCode() : 0);
        result = 31 * result + (getNotsLoanNo() != null ? getNotsLoanNo().hashCode() : 0);
        result = 31 * result + (getNotsAuctionRecordingDate() != null ? getNotsAuctionRecordingDate().hashCode() : 0);
        result = 31 * result + (getNotsAuctionDocumentNumber() != null ? getNotsAuctionDocumentNumber().hashCode() : 0);
        result = 31 * result + (getNotsTrusteeSaleNumber() != null ? getNotsTrusteeSaleNumber().hashCode() : 0);
        result = 31 * result + (getNotsIndexNo() != null ? getNotsIndexNo().hashCode() : 0);
        result = 31 * result + (getNotsLoanDefaultAmount() != null ? getNotsLoanDefaultAmount().hashCode() : 0);
        result = 31 * result + (getLispendDocketNo() != null ? getLispendDocketNo().hashCode() : 0);
        result = 31 * result + (getLispendDocNo() != null ? getLispendDocNo().hashCode() : 0);
        result = 31 * result + (getLispendRecordingDate() != null ? getLispendRecordingDate().hashCode() : 0);
        result = 31 * result + (getLispendIndexNo() != null ? getLispendIndexNo().hashCode() : 0);
        result = 31 * result + (getLispendLoanNo() != null ? getLispendLoanNo().hashCode() : 0);
        result = 31 * result + (getLispendLoanDate() != null ? getLispendLoanDate().hashCode() : 0);
        result = 31 * result + (getLispendCaseNo() != null ? getLispendCaseNo().hashCode() : 0);
        result = 31 * result + (getHud203kEligible() != null ? getHud203kEligible().hashCode() : 0);
        result = 31 * result + (getHudBidDeadline() != null ? getHudBidDeadline().hashCode() : 0);
        result = 31 * result + (getHudPriority() != null ? getHudPriority().hashCode() : 0);
        result = 31 * result + (getHudSaleStatus() != null ? getHudSaleStatus().hashCode() : 0);
        result = 31 * result + (getHudEscrowAmount() != null ? getHudEscrowAmount().hashCode() : 0);
        result = 31 * result + (getAssessedValuationLand() != null ? getAssessedValuationLand().hashCode() : 0);
        result = 31 * result + (getAssessedValuationImprovement() != null ? getAssessedValuationImprovement().hashCode() : 0);
        result = 31 * result + (getAssessedValuation() != null ? getAssessedValuation().hashCode() : 0);
        result = 31 * result + (getAssessedValuationYear() != null ? getAssessedValuationYear().hashCode() : 0);
        result = 31 * result + (getAssessedTax() != null ? getAssessedTax().hashCode() : 0);
        result = 31 * result + (getAssessedTaxYear() != null ? getAssessedTaxYear().hashCode() : 0);
        result = 31 * result + (getAssessedYearDelinquent() != null ? getAssessedYearDelinquent().hashCode() : 0);
        result = 31 * result + (getAssessedValuationRate() != null ? getAssessedValuationRate().hashCode() : 0);
        result = 31 * result + (getLatitude() != null ? getLatitude().hashCode() : 0);
        result = 31 * result + (getLongitude() != null ? getLongitude().hashCode() : 0);
        result = 31 * result + getFips().hashCode();
        result = 31 * result + (getHoaFee() != null ? getHoaFee().hashCode() : 0);
        result = 31 * result + (getCloseDate() != null ? getCloseDate().hashCode() : 0);
        result = 31 * result + (getClosePrice() != null ? getClosePrice().hashCode() : 0);
        result = 31 * result + (getSchoolDistrict() != null ? getSchoolDistrict().hashCode() : 0);
        result = 31 * result + (getElementaryschool() != null ? getElementaryschool().hashCode() : 0);
        result = 31 * result + (getMiddleSchool() != null ? getMiddleSchool().hashCode() : 0);
        result = 31 * result + (getHighSchool() != null ? getHighSchool().hashCode() : 0);
        result = 31 * result + (getQualityScoreMember() != null ? getQualityScoreMember().hashCode() : 0);
        result = 31 * result + (getQualityScoreSales() != null ? getQualityScoreSales().hashCode() : 0);
        result = 31 * result + (getQualityScore1() != null ? getQualityScore1().hashCode() : 0);
        result = 31 * result + (getQualityScore2() != null ? getQualityScore2().hashCode() : 0);
        result = 31 * result + (getQualityScore3() != null ? getQualityScore3().hashCode() : 0);
        result = 31 * result + (getQualityScore4() != null ? getQualityScore4().hashCode() : 0);
        result = 31 * result + (getQualityScore5() != null ? getQualityScore5().hashCode() : 0);
        result = 31 * result + (getQualityScore6() != null ? getQualityScore6().hashCode() : 0);
        result = 31 * result + (getQualityScore7() != null ? getQualityScore7().hashCode() : 0);
        result = 31 * result + (getQualityScore8() != null ? getQualityScore8().hashCode() : 0);
        result = 31 * result + (getQualityScore9() != null ? getQualityScore9().hashCode() : 0);
        result = 31 * result + (getQualityScore10() != null ? getQualityScore10().hashCode() : 0);
        result = 31 * result + (getSortAmount() != null ? getSortAmount().hashCode() : 0);
        result = 31 * result + (getHomeScore() != null ? getHomeScore().hashCode() : 0);
        result = 31 * result + (getInvestorScore() != null ? getInvestorScore().hashCode() : 0);
        result = 31 * result + (getAddressId() != null ? getAddressId().hashCode() : 0);
        result = 31 * result + (getEstimatedValue() != null ? getEstimatedValue().hashCode() : 0);
        result = 31 * result + (getEstimatedLow() != null ? getEstimatedLow().hashCode() : 0);
        result = 31 * result + (getEstimatedHigh() != null ? getEstimatedHigh().hashCode() : 0);
        result = 31 * result + (getEstimatedConfidence() != null ? getEstimatedConfidence().hashCode() : 0);
        result = 31 * result + (getEstimatedValueDate() != null ? getEstimatedValueDate().hashCode() : 0);
        result = 31 * result + (getRent() != null ? getRent().hashCode() : 0);
        result = 31 * result + (getOriginalLoanAmount() != null ? getOriginalLoanAmount().hashCode() : 0);
        result = 31 * result + (getAmount() != null ? getAmount().hashCode() : 0);
        result = 31 * result + (getTransferValue() != null ? getTransferValue().hashCode() : 0);
        result = 31 * result + (getEstimatedRent() != null ? getEstimatedRent().hashCode() : 0);
        result = 31 * result + (getFoundation() != null ? getFoundation().hashCode() : 0);
        result = 31 * result + (getRoomList() != null ? getRoomList().hashCode() : 0);
        result = 31 * result + (getAppliances() != null ? getAppliances().hashCode() : 0);
        result = 31 * result + (getFloorCovering() != null ? getFloorCovering().hashCode() : 0);
        result = 31 * result + (getRoofType() != null ? getRoofType().hashCode() : 0);
        result = 31 * result + (getViewType() != null ? getViewType().hashCode() : 0);
        result = 31 * result + (getTracking() != null ? getTracking().hashCode() : 0);
        result = 31 * result + (getPaidListing() != null ? getPaidListing().hashCode() : 0);
        result = 31 * result + (getFirstLook() != null ? getFirstLook().hashCode() : 0);
        result = 31 * result + (getHundredDownPayment() != null ? getHundredDownPayment().hashCode() : 0);
        result = 31 * result + (getHmpthMortgage() != null ? getHmpthMortgage().hashCode() : 0);
        result = 31 * result + (getHmpthRenovationMortgage() != null ? getHmpthRenovationMortgage().hashCode() : 0);
        result = 31 * result + (getPriceReduction() != null ? getPriceReduction().hashCode() : 0);
        result = 31 * result + (getGnndExpDate() != null ? getGnndExpDate().hashCode() : 0);
        result = 31 * result + (getNspHome() != null ? getNspHome().hashCode() : 0);
        result = 31 * result + (getSpecialtyHome() != null ? getSpecialtyHome().hashCode() : 0);
        result = 31 * result + (getFirstMergeDate() != null ? getFirstMergeDate().hashCode() : 0);
        result = 31 * result + (getAltFinance() != null ? getAltFinance().hashCode() : 0);
        result = 31 * result + (getEstimatedMonthlyPayment() != null ? getEstimatedMonthlyPayment().hashCode() : 0);
        result = 31 * result + (getMonthlyAmountRTOHenrysAlgo() != null ? getMonthlyAmountRTOHenrysAlgo().hashCode() : 0);
        result = 31 * result + (getMonthlyAmount30yearFixedMortgage() != null ? getMonthlyAmount30yearFixedMortgage().hashCode() : 0);
        result = 31 * result + (getDataSources() != null ? getDataSources().hashCode() : 0);
        result = 31 * result + (getExclusive() != null ? getExclusive().hashCode() : 0);
        result = 31 * result + (getSpecialFinancing() != null ? getSpecialFinancing().hashCode() : 0);
        result = 31 * result + (getBargainPrice() != null ? getBargainPrice().hashCode() : 0);
        result = 31 * result + (getFixerUpper() != null ? getFixerUpper().hashCode() : 0);
        result = 31 * result + (getRtoPotential() != null ? getRtoPotential().hashCode() : 0);
        result = 31 * result + (getRtoFinancing() != null ? getRtoFinancing().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Property{" +
                "propertyId=" + propertyId +
                ", listPrice=" + listPrice +
                ", amountDefinition='" + amountDefinition + '\'' +
                ", streetAddress='" + streetAddress + '\'' +
                ", fullStreetName='" + fullStreetName + '\'' +
                ", city='" + city + '\'' +
                ", county='" + county + '\'' +
                ", stateOrProvince='" + stateOrProvince + '\'' +
                ", postal_Code='" + postal_Code + '\'' +
                ", urlHex='" + urlHex + '\'' +
                ", pictureDataSourceUrl='" + pictureDataSourceUrl + '\'' +
                ", pictureRemote=" + pictureRemote +
                ", pictureCount=" + pictureCount +
                ", parcelNumber='" + parcelNumber + '\'' +
                ", mlsNumber='" + mlsNumber + '\'' +
                ", mlsDisclaimer='" + mlsDisclaimer + '\'' +
                ", listingStatus='" + listingStatus + '\'' +
                ", listingUrl='" + listingUrl + '\'' +
                ", reoDocumentNumber='" + reoDocumentNumber + '\'' +
                ", reoRecordingDate=" + reoRecordingDate +
                ", judgmentAmount=" + judgmentAmount +
                ", legalDescription='" + legalDescription + '\'' +
                ", propertyRecordType='" + propertyRecordType + '\'' +
                ", bathsTotal=" + bathsTotal +
                ", bedrooms=" + bedrooms +
                ", stories='" + stories + '\'' +
                ", livingAreaSquareFeet=" + livingAreaSquareFeet +
                ", livingArea='" + livingArea + '\'' +
                ", lotSize='" + lotSize + '\'' +
                ", YearBuilt=" + YearBuilt +
                ", totalUnits=" + totalUnits +
                ", unitNumber='" + unitNumber + '\'' +
                ", condition='" + condition + '\'' +
                ", totalRooms=" + totalRooms +
                ", den='" + den + '\'' +
                ", kitchen='" + kitchen + '\'' +
                ", livingRoom='" + livingRoom + '\'' +
                ", familyRoom='" + familyRoom + '\'' +
                ", basement='" + basement + '\'' +
                ", heating='" + heating + '\'' +
                ", cooling='" + cooling + '\'' +
                ", parking='" + parking + '\'' +
                ", parkingSpaces='" + parkingSpaces + '\'' +
                ", zoning='" + zoning + '\'' +
                ", listDate=" + listDate +
                ", features='" + features + '\'' +
                ", pool='" + pool + '\'' +
                ", style='" + style + '\'' +
                ", constructionType='" + constructionType + '\'' +
                ", exteriorWalls='" + exteriorWalls + '\'' +
                ", description='" + description + '\'' +
                ", saleType='" + saleType + '\'' +
                ", nodDateDefaultedLien=" + nodDateDefaultedLien +
                ", nodDocNumberDefaultedLien=" + nodDocNumberDefaultedLien +
                ", nodAmountDefault=" + nodAmountDefault +
                ", nodDateDefault=" + nodDateDefault +
                ", nodRecordingDate=" + nodRecordingDate +
                ", nodRecordingYear=" + nodRecordingYear +
                ", nodDocumentNumber='" + nodDocumentNumber + '\'' +
                ", taxSaleJurisdictionName='" + taxSaleJurisdictionName + '\'' +
                ", taxSaleHowOften='" + taxSaleHowOften + '\'' +
                ", taxSaleRate='" + taxSaleRate + '\'' +
                ", taxSaleRedemtionPeriod='" + taxSaleRedemtionPeriod + '\'' +
                ", taxSaleBidMethod='" + taxSaleBidMethod + '\'' +
                ", taxSaleGetPremiumBack=" + taxSaleGetPremiumBack +
                ", auctionDate=" + auctionDate +
                ", auctionTime=" + auctionTime +
                ", notsOpeningBid=" + notsOpeningBid +
                ", notsAuctionTitle='" + notsAuctionTitle + '\'' +
                ", notsAuctionAddress='" + notsAuctionAddress + '\'' +
                ", notsAuctionCity='" + notsAuctionCity + '\'' +
                ", notsAuctionState='" + notsAuctionState + '\'' +
                ", notsAuctionZIP=" + notsAuctionZIP +
                ", notsAuctionHouseName='" + notsAuctionHouseName + '\'' +
                ", notsAuctionDescription='" + notsAuctionDescription + '\'' +
                ", notsAuctionTerms='" + notsAuctionTerms + '\'' +
                ", notsLoanDate=" + notsLoanDate +
                ", notsLoanNo='" + notsLoanNo + '\'' +
                ", notsAuctionRecordingDate=" + notsAuctionRecordingDate +
                ", notsAuctionDocumentNumber='" + notsAuctionDocumentNumber + '\'' +
                ", notsTrusteeSaleNumber='" + notsTrusteeSaleNumber + '\'' +
                ", notsIndexNo='" + notsIndexNo + '\'' +
                ", NotsLoanDefaultAmount=" + NotsLoanDefaultAmount +
                ", lispendDocketNo='" + lispendDocketNo + '\'' +
                ", lispendDocNo='" + lispendDocNo + '\'' +
                ", lispendRecordingDate=" + lispendRecordingDate +
                ", lispendIndexNo='" + lispendIndexNo + '\'' +
                ", lispendLoanNo='" + lispendLoanNo + '\'' +
                ", lispendLoanDate=" + lispendLoanDate +
                ", lispendCaseNo='" + lispendCaseNo + '\'' +
                ", hud203kEligible='" + hud203kEligible + '\'' +
                ", hudBidDeadline='" + hudBidDeadline + '\'' +
                ", hudPriority='" + hudPriority + '\'' +
                ", hudSaleStatus='" + hudSaleStatus + '\'' +
                ", hudEscrowAmount=" + hudEscrowAmount +
                ", assessedValuationLand=" + assessedValuationLand +
                ", assessedValuationImprovement=" + assessedValuationImprovement +
                ", assessedValuation=" + assessedValuation +
                ", assessedValuationYear=" + assessedValuationYear +
                ", assessedTax=" + assessedTax +
                ", assessedTaxYear=" + assessedTaxYear +
                ", assessedYearDelinquent=" + assessedYearDelinquent +
                ", assessedValuationRate=" + assessedValuationRate +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", fips='" + fips + '\'' +
                ", hoaFee='" + hoaFee + '\'' +
                ", closeDate=" + closeDate +
                ", closePrice=" + closePrice +
                ", schoolDistrict='" + schoolDistrict + '\'' +
                ", elementaryschool='" + elementaryschool + '\'' +
                ", middleSchool='" + middleSchool + '\'' +
                ", highSchool='" + highSchool + '\'' +
                ", qualityScoreMember=" + qualityScoreMember +
                ", qualityScoreSales=" + qualityScoreSales +
                ", qualityScore1=" + qualityScore1 +
                ", qualityScore2=" + qualityScore2 +
                ", qualityScore3=" + qualityScore3 +
                ", qualityScore4=" + qualityScore4 +
                ", qualityScore5=" + qualityScore5 +
                ", qualityScore6=" + qualityScore6 +
                ", qualityScore7=" + qualityScore7 +
                ", qualityScore8=" + qualityScore8 +
                ", qualityScore9=" + qualityScore9 +
                ", qualityScore10=" + qualityScore10 +
                ", sortAmount=" + sortAmount +
                ", homeScore=" + homeScore +
                ", investorScore=" + investorScore +
                ", addressId=" + addressId +
                ", estimatedValue=" + estimatedValue +
                ", estimatedLow=" + estimatedLow +
                ", estimatedHigh=" + estimatedHigh +
                ", estimatedConfidence=" + estimatedConfidence +
                ", estimatedValueDate=" + estimatedValueDate +
                ", rent=" + rent +
                ", originalLoanAmount=" + originalLoanAmount +
                ", amount=" + amount +
                ", transferValue=" + transferValue +
                ", estimatedRent=" + estimatedRent +
                ", foundation='" + foundation + '\'' +
                ", roomList='" + roomList + '\'' +
                ", appliances='" + appliances + '\'' +
                ", floorCovering='" + floorCovering + '\'' +
                ", roofType='" + roofType + '\'' +
                ", viewType='" + viewType + '\'' +
                ", tracking='" + tracking + '\'' +
                ", paidListing=" + paidListing +
                ", firstLook=" + firstLook +
                ", hundredDownPayment=" + hundredDownPayment +
                ", hmpthMortgage=" + hmpthMortgage +
                ", hmpthRenovationMortgage=" + hmpthRenovationMortgage +
                ", priceReduction=" + priceReduction +
                ", gnndExpDate=" + gnndExpDate +
                ", nspHome=" + nspHome +
                ", specialtyHome=" + specialtyHome +
                ", firstMergeDate=" + firstMergeDate +
                ", altFinance=" + altFinance +
                ", estimatedMonthlyPayment=" + estimatedMonthlyPayment +
                ", monthlyAmountRTOHenrysAlgo=" + monthlyAmountRTOHenrysAlgo +
                ", monthlyAmount30yearFixedMortgage=" + monthlyAmount30yearFixedMortgage +
                ", dataSources='" + dataSources + '\'' +
                ", exclusive=" + exclusive +
                ", specialFinancing=" + specialFinancing +
                ", bargainPrice=" + bargainPrice +
                ", fixerUpper=" + fixerUpper +
                ", rtoPotential=" + rtoPotential +
                ", rtoFinancing=" + rtoFinancing +
                '}';
    }
}
