package com.nic.properties.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity

@Table(name = "tbl_state")

public class StateDetail {

    @Column(name = "state_name")
    private String state_name;

    @Id
    @Column(name = "state_code", columnDefinition = "char(2)")
    private String state_code;

    public StateDetail() {

    }

    public String getState_code() {

        return state_code;

    }

    public String getState_name() {

        return state_name;

    }

}
