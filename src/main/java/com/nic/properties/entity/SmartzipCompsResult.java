package com.nic.properties.entity;

import java.io.Serializable;
import java.util.List;

public class SmartzipCompsResult implements Serializable {

    public List<SmartzipComparable> comparable_properties;

    public List<SmartzipComparable> getComparables() {
        return comparable_properties;
    }

    public void setComparables(List<SmartzipComparable> comparable_properties) {
        this.comparable_properties = comparable_properties;
    }

}
