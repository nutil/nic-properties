package com.nic.properties.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Embeddable
@JsonInclude(Include.NON_NULL)

public class ForeclosureInfo implements Serializable {

	@Column(name="reo_document_number")
	@TableGenerator(name = "tbl_prop")
	private String reoDocumentNumber;

	

	@Column(name = "reo_recording_date",columnDefinition = "date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date reoRecordingDate;
	
	@Column(name="nots_trustee_sale_number")
	private String trusteeSaleNumber;
	
	@Column(name = "auction_date",columnDefinition = "date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date auctionDate;
	

	@Column(name = "auction_time", columnDefinition = "time",nullable = true)
	
	private Time auctionTime;
	
	@Column(name="opening_bid")
	@TableGenerator(name = "tbl_prop")
	private Integer openingBid;
	
	@Column(name="nots_auction_address")
	@TableGenerator(name = "tbl_prop")
    private String auctionAddress;	
	
	
	@Column(name = "nots_auction_recording_date",columnDefinition = "date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date auctionRecordingDate;
	
	@Column(name="nots_loan_default_amount", columnDefinition = "DECIMAL")
	@TableGenerator(name = "tbl_prop")
	private Integer loanDefaultAmount;
	
	@Column(name = "nots_loan_date",columnDefinition = "date", nullable = true)
	@Temporal(TemporalType.DATE)
	private Date laonDate;
	
	@Column(name="nots_loan_no")
	@TableGenerator(name = "tbl_prop")
	private String loanNumber;

	public String getReoDocumentNumber() {
		return reoDocumentNumber;
	}

	public void setReoDocumentNumber(String reoDocumentNumber) {
		this.reoDocumentNumber = reoDocumentNumber;
	}

	public Date getReoRecordingDate() {
		return reoRecordingDate;
	}

	public void setReoRecordingDate(Date reoRecordingDate) {
		this.reoRecordingDate = reoRecordingDate;
	}

	public String getTrusteeSaleNumber() {
		return trusteeSaleNumber;
	}

	public void setTrusteeSaleNumber(String trusteeSaleNumber) {
		this.trusteeSaleNumber = trusteeSaleNumber;
	}

	public Date getAuctionDate() {
		return auctionDate;
	}

	public void setAuctionDate(Date auctionDate) {
		this.auctionDate = auctionDate;
	}

	public Time getAuctionTime() {
		return auctionTime;
	}

	public void setAuctionTime(Time auctionTime) {
		this.auctionTime = auctionTime;
	}

	public Integer getOpeningBid() {
		return openingBid;
	}

	public void setOpeningBid(Integer openingBid) {
		this.openingBid = openingBid;
	}

	public String getAuctionAddress() {
		return auctionAddress;
	}

	public void setAuctionAddress(String auctionAddress) {
		this.auctionAddress = auctionAddress;
	}

	public Date getAuctionRecordingDate() {
		return auctionRecordingDate;
	}

	public void setAuctionRecordingDate(Date auctionRecordingDate) {
		this.auctionRecordingDate = auctionRecordingDate;
	}

	public Integer getLoanDefaultAmount() {
		return loanDefaultAmount;
	}

	public void setLoanDefaultAmount(Integer loanDefaultAmount) {
		this.loanDefaultAmount = loanDefaultAmount;
	}

	public Date getLaonDate() {
		return laonDate;
	}

	public void setLaonDate(Date laonDate) {
		this.laonDate = laonDate;
	}

	public String getLoanNumber() {
		return loanNumber;
	}

	public void setLoanNumber(String loanNumber) {
		this.loanNumber = loanNumber;
	}

	@Override
	public String toString() {
        return "ForeclosureInfo [reoDocumentNumber=" + reoDocumentNumber + ", reoRecordingDate=" + reoRecordingDate
                + ", trusteeSaleNumber=" + trusteeSaleNumber + ", auctionDate=" + auctionDate + ", auctionTime="
                + auctionTime + ", openingBid=" + openingBid + ", auctionAddress=" + auctionAddress
                + ", auctionRecordingDate=" + auctionRecordingDate + ", loanDefaultAmount=" + loanDefaultAmount
                + ", laonDate=" + laonDate + ", loanNumber=" + loanNumber + "]";
    }

}
