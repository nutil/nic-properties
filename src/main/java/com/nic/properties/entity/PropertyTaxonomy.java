package com.nic.properties.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "property_taxonomy")
public class PropertyTaxonomy implements Serializable {

    @Column(name = "sale_group_type")
    private String saleGroupType;
    @Id
    @Column(name = "sale_type")
    private String saleType;

    public String getSaleGroupType() {
        return saleGroupType;
    }

    public void setSaleGroupType(String saleGroupType) {
        this.saleGroupType = saleGroupType;
    }

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }

    @Override
    public String toString() {
        return "PropertyTaxonomy [saleGroupType=" + saleGroupType + ", saleType=" + saleType + "]";
    }


}
