package com.nic.properties.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tbl_prop", schema = "nic_pdb")
@JsonInclude(Include.NON_NULL)
public class FsboOrMlsProperty implements Serializable {
    @Embedded
    Features features;
    @Embedded
    Facts facts;
    @Embedded
    PriceInfo priceInfo;
    @Id
    @Column(name = "property_id", nullable = false, columnDefinition = "INT", length = 11)

    private Integer propertyId;
    @Column(name = "sale_type")
    private String saleType;
    @Column(name = "street_address")
    private String StreetAddress;
    @Column(name = "full_street_name")
    private String streetName;
    @Column(name = "city")
    private String city;
    @Column(name = "state_or_province", nullable = true, columnDefinition = "CHAR")
    private String State;
    @Column(name = "longitude", nullable = true)
    private Double longitude;
    @Column(name = "description", columnDefinition = "text")
    private String description;
    @Column(name = "latitude", nullable = true)
    private Double latitude;
    @Column(name = "construction_type")
    private String streetNumber;

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }

    public String getStreetAddress() {
        return StreetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        StreetAddress = streetAddress;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public Features getFeatures() {
        return features;
    }

    public void setFeatures(Features features) {
        this.features = features;
    }

    public Facts getFacts() {
        return facts;
    }

    public void setFacts(Facts facts) {
        this.facts = facts;
    }

    public PriceInfo getPriceInfo() {
        return priceInfo;
    }

    public void setPriceInfo(PriceInfo priceInfo) {
        this.priceInfo = priceInfo;
    }

    @Override
    public String toString() {
        return "FsboOrMlsProperty [propertyId=" + propertyId + ", saleType=" + saleType + ", StreetAddress="
                + StreetAddress + ", streetName=" + streetName + ", city=" + city + ", State=" + State + ", longitude="
                + longitude + ", description=" + description + ", latitude=" + latitude + ", streetNumber="
                + streetNumber + ", features=" + features + ", facts=" + facts +", priceInfo= "+priceInfo +"]";
    }

}
