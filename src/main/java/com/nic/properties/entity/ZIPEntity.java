package com.nic.properties.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;


@Entity(name = "ZIP")
@Table(name = "ZIP", schema = "nic_pdb")
public class ZIPEntity implements Serializable {
    @Id
    @Column(name = "ZIP", columnDefinition = "mediumint(5) unsigned zerofill", nullable = false, precision = 5)
    private BigInteger cityZipCode;

    @Column(name = "STATE", columnDefinition = "char(2)", nullable = false, length = 2)
    private String stateName;

    @Column(name = "CITY", columnDefinition = "varchar(28)", length = 28, nullable = false)
    private String cityName;

    @ManyToOne
    @JoinColumn(name = "fips")
    private CountyEntity county;

    public ZIPEntity() {
    }

    public ZIPEntity(BigInteger cityZipCode, String stateName, String cityName, CountyEntity county) {
        this.cityZipCode = cityZipCode;
        this.stateName = stateName;
        this.cityName = cityName;
        this.county = county;
    }

    public BigInteger getCityZipCode() {
        return cityZipCode;
    }

    public void setCityZipCode(BigInteger cityZipCode) {
        this.cityZipCode = cityZipCode;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public CountyEntity getCounty() {
        return county;
    }

    public void setCounty(CountyEntity county) {
        this.county = county;
    }

    @Override
    public String toString() {
        return "ZIPEntity{" +
                "cityZipCode=" + cityZipCode +
                ", stateName='" + stateName + '\'' +
                ", cityName='" + cityName + '\'' +
                ", county=" + county +
                '}';
    }
}
