package com.nic.properties.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "CNTY")
public class County implements Serializable {
    @Id
    @Column(name = "FIPS", columnDefinition = "MEDIUMINT")
    private Integer fips;

    public Integer getFips() {
        return fips;
    }

    public void setFips(Integer fips) {
        this.fips = fips;
    }

}
