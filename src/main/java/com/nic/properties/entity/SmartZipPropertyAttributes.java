package com.nic.properties.entity;

import java.io.Serializable;

public class SmartZipPropertyAttributes implements Serializable {

    public int beds;

    public float baths;

    public int partial_baths;

    public int rooms;

    public int stories;

    public int units;

    public int sqft;

    public int sqft_lot;

    public int year_built;

    public int getBeds() {
        return beds;
    }

    public void setBeds(int beds) {
        this.beds = beds;
    }

    public float getBaths() {
        return baths;
    }

    public void setBaths(float baths) {
        this.baths = baths;
    }

    public int getPartial_baths() {
        return partial_baths;
    }

    public void setPartial_baths(int partial_baths) {
        this.partial_baths = partial_baths;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public int getStories() {
        return stories;
    }

    public void setStories(int stories) {
        this.stories = stories;
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(int units) {
        this.units = units;
    }

    public int getSqft() {
        return sqft;
    }

    public void setSqft(int sqft) {
        this.sqft = sqft;
    }

    public int getSqft_lot() {
        return sqft_lot;
    }

    public void setSqft_lot(int sqft_lot) {
        this.sqft_lot = sqft_lot;
    }

    public int getYear_built() {
        return year_built;
    }

    public void setYear_built(int year_built) {
        this.year_built = year_built;
    }

}
